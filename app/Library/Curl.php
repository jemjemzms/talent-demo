<?php
//Author:Jed Diaz
//Date: Dec, 3, 2015
//Description: This class will post to afojp
//Requirements: API Address(STRING),  Parameters(ASSOCARRAY)  
//Application: FLAMINGO (SKIN:BIGBANG)

namespace App\Library {

	class Curl {
		
		function post_curl($url,$xml){ 
			 //'https://mapi.inservix.com/api-web/member-api'
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/xml',
				'Content-Length: ' . strlen($xml))
			);  
			$result = curl_exec($ch);
			$info = curl_getinfo($ch);
			 return $result;
			//header("Content-Type: text/xml");
			//var_dump( $result); 
		}
		
		function call_api($url,$xml){ 
			//This function post the API call to xpro web services
			//AccessPassword gets appended to the parameters 
			 
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
			curl_setopt($ch, CURLOPT_FAILONERROR, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
			curl_setopt($ch, CURLOPT_TIMEOUT, 0); // times out after Ns
			curl_setopt($ch, CURLOPT_POST, 1); // set POST method
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml); // add POST fields
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
			curl_setopt($ch, CURLOPT_FAILONERROR, 0);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_COOKIEFILE, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/xml',
				'Content-Length: ' . strlen($xml))
			);

			
			$result = curl_exec($ch); // run the whole process
			curl_close($ch);
			// echo  $result;
			//header("Content-Type: text/xml");
		    return $result;
			 
			
			// print_r($xml);
			// return $xml[1];   
			//CONVERT THE XML TO ARRAY
			//$xmlObj  = new XmlToArray($xml[1]);
			//return $xml[1];   
			 
		}
		
		function URLIFY($fields){
			//url-ify the data for the POST
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }	
			return rtrim($fields_string,'&');
			}
			
			function SET_HEADER($header_type){
			if($header_type=="html"){
				header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past 
			}else if($header_type=="xml"){  
				header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
				header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
				header("Cache-Control: no-cache, must-revalidate" );
				header("Pragma: no-cache" );
				header("Content-type: text/xml");
				echo"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			}
		}
	}//class

}
?>