<?php
namespace App\Http\Controllers\Frontend;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;

//for pagination
use Illuminate\Pagination\LengthAwarePaginator;
//use App\Http\Models\Backend\Setting;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\model_list; 
use App\Http\Models\Backoffice\model_gallery;
use App\Http\Models\Backoffice\experience;
use App\Http\Models\Backoffice\categories;

use App\Http\Models\Backoffice\shortlists;
use App\Http\Models\Backoffice\customers;

class ModelController extends Controller{

    public function index($talent){

      $result = model_list::where('slug', $talent)->get();

        $data = [];
        foreach ($result as $key){

            $exp = explode(',', $key->experiences);
            $experience = experience::where('id', $exp[0])->first();

            $data[] = [ 'id' => $key->id, 
                        'model_name' => $key->model_name, 
                        'slug' => $key->slug, 
                        'country' => $key->country, 
                        'height' => $key->height, 
                        'bust_chest' => $key->bust_chest,
                        'waist' => $key->waist,
                        'hips' => $key->hips,
                        'shoe_size' => $key->shoe_size,
                        'dress_size' => $key->dress_size,
                        'notes' => $key->notes,
                        'primary_photo' => $key->primary_photo,
                        'categories' => $key->categories,
                        'featured_model' => $key->featured_model,
                        'experience' => $experience->name,
                        'selected' => false ];
        }

        $result = model_gallery::where('user_id', $data[0]['id'])->get();
        $gallery = [];
        $mainImage = '';
        foreach ($result as $key){
            if($key->featured == 'yes'){
              $mainImage = $key->image_name;
            } else{
            $gallery[] = [ 'id' => $key->id, 
                        'image_name' => $key->image_name, 
                        'user_id' => $key->user_id, 
                        'featured' => $key->featured
                      ];
            }
        }
        $mainImage = ($mainImage) ? asset('/img/uploads/medium/'.$mainImage ) : asset('/img/profiles/medium/'.$data[0]['primary_photo'] );

    	return view('Frontend.models')
              ->with('data', $data)
              ->with('mainImage', $mainImage)
              ->with('gallery', $gallery);
    }

    public function checkShortlist(){

            $result = model_list::where('slug', Request::input('slug'))->first();

            $selected = 'false';
            $customer = customers::where('username', Request::input('username'))->first();
            $customerId = (int)$customer->id;
            $talentId = $result->id;
            $exist = shortlists::where('customer_id', $customerId)->where('talent_id', $talentId)->count();
            if($exist > 0){
              $selected = 'true';
            }
            

      return $selected;
    }

}
?>