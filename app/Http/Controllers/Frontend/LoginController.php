<?php
namespace App\Http\Controllers\Frontend;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;

//for pagination
use Illuminate\Pagination\LengthAwarePaginator;
//use App\Http\Models\Backend\Setting;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\customers;

class LoginController extends Controller{

    public function check(){
    	
    	return 'false';
    }

    public function index(){

      return view('Frontend.login');
    }

    public function signup(){

      return view('Frontend.signup'); 
    }

    public function signupProcess(){

      $result = customers::where('username', Request::input('username'))->count();

      if($result == 0){
      
      $customers = new customers;
      $customers->username = Request::input('username');
      $customers->first_name = Request::input('firstname');
      $customers->last_name = Request::input('lastname');
      $customers->nickname = Request::input('nickname');
      $customers->email = Request::input('email');
      $customers->website = Request::input('website');
      $customers->bio = Request::input('bio');
      $customers->password = Request::input('password');
      $customers->save();

        return ['status' => 'true', 'message' => 'You\'ve been successfully been registered.' ];  

      } else{

        return ['status' => 'false', 'message' => 'The username already exists.' ];  
      
      }

    }

    public function loginProcess(){
      $customerQuery = customers::where('username', Request::input('username'))->where('password', Request::input('password'));

      if($customerQuery->count() > 0){
        date_default_timezone_set('Asia/Manila');

        $customer = $customerQuery->first();
        $customerLastLogin = customers::find($customer->id);
        $customerLastLogin->last_login = date('Y-m-d H:i:s');
        $customerLastLogin->save();

        return ['status' => 'true', 'message' => 'You\'ve successfully been signed in.' ];  
      } else{
        return ['status' => 'false', 'message' => 'You\'re username and password are incorrect.' ];  
      }
    }
}
?>