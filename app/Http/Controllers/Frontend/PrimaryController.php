<?php
namespace App\Http\Controllers\Frontend;

use DB;
use Redirect;
use Input;
use Session;
use Illuminate\Http\Request; //save post
use App\Http\Controllers\Controller;

//for pagination
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Models\Backend\Setting;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\model_list; 
use App\Http\Models\Backoffice\model_gallery;
use App\Http\Models\Backoffice\categories;
use App\Http\Models\Backoffice\blog;
use App\Http\Models\Backoffice\pages;
use App\Http\Models\Backoffice\media;

class PrimaryController extends Controller{

    public function index(){
    	
    	$config = [
                  'url' => asset('/'),
                  ];
                  

    	return view('Frontend.index')
    			->with('config', $config)
                ->with('googleAnalyticsApi', self::_getSetting('google_analytics_api'));
    }

    public function home(){

        $result = model_list::leftJoin('model_gallery', function($join) {
          $join->on('model_list.id', '=', 'model_gallery.user_id');
        })->where('model_gallery.featured', 'yes')
        ->where('model_list.featured_model', 'yes')
        ->orderBy('model_list.order', 'ASC')
        ->get([
            'model_list.id',
            'model_list.model_name',
            'model_list.notes',
            'model_list.slug',
            'model_gallery.image_name',
        ]);

        $featured = [];
        foreach ($result as $key){

            $featured[] = [ 'id'         => $key->id, 
                            'model_name' => $key->model_name, 
                            'notes'      => $key->notes, 
                            'slug'       => $key->slug, 
                            'image_name' => $key->image_name,
                          ];
        }

        return view('Frontend.home')
                 ->with('featured', $featured);
    }

    public function albums(){
        $result = model_list::orderBy('order', 'ASC')->get();

        $albums = [];
        foreach ($result as $key){

            $albums[] = [ 'id'         => $key->id, 
                            'model_name' => $key->model_name, 
                            'notes'      => $key->notes, 
                            'slug'       => $key->slug, 
                            'primary_photo' => $key->primary_photo,
                            'categories'    => $key->categories,
                          ];

        }

        $result = categories::get();
        $categories = [];
        foreach ($result as $key){

            $categories[] = [ 'id'       => $key->id, 
                            'name'       => $key->name, 
                            'slug'       => $key->slug, 
                          ];

        }

        return view('Frontend.m_albums')
                ->with('albums', $albums)
                ->with('categories', $categories);
    }

    public function albumsEntry($id, $title){

        $albumDetail = model_list::where('id', $id)->first();

        $categoriesArray = explode(',', $albumDetail['categories']);
        $categories = [];
        foreach ($categoriesArray as $catKey){
          $category = categories::where('id', $catKey)->first(['name']);

          array_push($categories, $category['name']);
        }

        $result = model_gallery::where('user_id', $albumDetail['id'])->orderBy('order', 'ASC')->get();
        $galleries = [];
        foreach ($result as $key){
            $galleries[] = ['id' => $key->id, 'image_name' => $key->image_name, 'caption' => $key->caption];
        } 

        return view('Frontend.m_album_entry')
                    ->with('albumId', $id)
                    ->with('url', asset('/'.$id.'/'.$title))
                    ->with('albumDetail',$albumDetail)
                    ->with('categories', $categories[0])
                    ->with('galleries', $galleries);
    }

    public function albumsSlideshowEntry($albumId, $galleryId){

        $albumDetail = model_list::where('id', $albumId)->first();

        $categoriesArray = explode(',', $albumDetail['categories']);
        $categories = [];
        foreach ($categoriesArray as $catKey){
          $category = categories::where('id', $catKey)->first(['name']);

          array_push($categories, $category['name']);
        }

        $result = model_gallery::where('user_id', $albumDetail['id'])->orderBy('order', 'ASC')->get();
        $galleries = [];
        $firstGallery = [];
        foreach ($result as $key){
            if($galleryId == $key->id){
                $firstGallery[] = ['id' => $key->id, 'image_name' => $key->image_name, 'caption' => $key->caption];
            } else{
                $galleries[] = ['id' => $key->id, 'image_name' => $key->image_name, 'caption' => $key->caption];
            }
        } 

        $gallery = array_merge($firstGallery, $galleries);

        return view('Frontend.m_album_slideshow_entry')
                    ->with('albumId', $albumId)
                    ->with('albumDetail',$albumDetail)
                    ->with('categories', $categories[0])
                    ->with('galleries', $gallery);
    }

    public function blogs(){
       $news = blog::leftJoin('users', function($join) {
          $join->on('blog.author', '=', 'users.id');
        })->orderBy('blog.created_at', 'DESC')
        ->get();

        $blogs = [];
        foreach ($news as $key){
            $blogs[] = [ 'blog_id' => $key->blog_id, 
                        'title' => $key->title,
                        'summary' => $key->summary,
                        'description' => $key->description,
                        'featured_image' => $key->featured_image,
                        'status' => $key->status,
                        'author' => $key->author,
                        'slug' => str_replace(' ','-', strtolower($key->title)),
                        'name' => $key->first_name . ' '.$key->last_name,
                        'date' => date('F j, Y', strtotime($key->created_at)),
                        'selected' => false ];
        }

        return view('Frontend.m_blogs')
                ->with('data', $blogs);
    }

    public function blogsEntry($id, $title){

      $news = blog::leftJoin('users', function($join) {
        $join->on('blog.author', '=', 'users.id');
      })
      ->where('blog.blog_id', $id)
      ->first();

        $data = [];
        $data = [   
                    'blog_id' => $news->blog_id, 
                    'title' => $news->title,
                    'summary' => $news->summary,
                    'description' => $news->description,
                    'featured_image' => $news->featured_image,
                    'status' => $news->status,
                    'author' => $news->author,
                    'slug' => str_replace(' ','-', strtolower($news->title)),
                    'name' => $news->first_name . ' '.$news->last_name,
                    'date' => date('F j, Y', strtotime($news->created_at)),
                     ];


        return view('Frontend.m_blog_entry')
                ->with('data', (object)$data);
    }

    public function about(){

        $aboutheaderPhoto = media::where('media_id', 35)->first();
        $aboutBgPhoto = media::where('media_id', 34)->first();

        $data = pages::leftJoin('users', function($join) {
          $join->on('pages.author', '=', 'users.id');
        })
        ->where('pages.pages_id',1)
        ->first();

      return view('Frontend.m_about')
              ->with('data', (object)$data)
              ->with('aboutheaderPhoto', $aboutheaderPhoto)
              ->with('aboutBgPhoto', $aboutBgPhoto);
    }

    public function contact(){
        $contactHeaderPhoto = media::where('media_id', 37)->first();
        $contactBgPhoto = media::where('media_id', 36)->first();

        return view('Frontend.m_contact')
                ->with('contactHeaderPhoto', $contactHeaderPhoto)
                ->with('contactBgPhoto', $contactBgPhoto);
    }

    public function page($id, $title){

        $pageHeaderPhoto = media::where('media_id', 38)->first();
        $page = DB::table('pages')
        ->select('title','description')
        ->where('pages_id', $id)
        ->first();

        return view('Frontend.m_pages')
                ->with('pageHeaderPhoto', $pageHeaderPhoto)
                ->with('data', $page);
    }

    public static function _getSetting( $config ){
        $setting = DB::table('settings')
        ->select('setting_value')
        ->where('setting_name', $config)
        ->get();

        return $setting[0]->setting_value;  
    }

    public static function _getPage( $config ){
         $page = DB::table('pages')
        ->select('title')
        ->where('pages_id', $config)
        ->first();

        return $page->title;     
    }

    public function mail(Request $request){
        
          
            $post = $request->all();
            $v = \Validator::make($request->all(),
                [
                    'name'       => 'required',
                    'email'       => 'required',
                    'message'       => 'required',
                ]);

            if($v->fails()){
                \Session::flash('error', 'Please complete the form.');
                 return Redirect::back();
            } else{
                $name = $post['name'];
                $email = $post['email'];
                $message = $post['message'];

                $to = self::_getSetting('email');
                $subject = 'Contact Form Inquiry'; 

                $headers = 'From: no-reply@marlonerubiophotography.com' 
                            . "\r\n" . 'Reply-To: no-reply@marlonerubiophotography.com'; 

                $message = 'Name: '.$name 
                            . "\r\n" . 'Email: '.$email
                            . "\r\n" . 'Message:'
                            . "\r\n" . $message;

                mail($to, $subject, $message, $headers, 'no-reply@marlonerubiophotography.com');

                \Session::flash('message', 'Message has successfully been sent. I will ge back at you shortly.');
                 return Redirect::back();
              }
    }

}
?>