<?php
namespace App\Http\Controllers\Frontend;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;

//for pagination
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Models\Backend\Setting;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\pages;
use App\Http\Models\Backoffice\users; 
use App\Http\Models\Backoffice\customers;
use App\Http\Models\Backoffice\media;

class PagesController extends Controller{

    public function contactIndex(){

      $pageHeaderPhoto = media::where('media_id', 38)->first();

    	return view('Frontend.contact')
              ->with('pageHeaderPhoto', $pageHeaderPhoto)
              ->with('address', self::_getSetting('address'))
              ->with('globePhone', self::_getSetting('globe_phone'))
              ->with('phone', self::_getSetting('phone'))
              ->with('email', self::_getSetting('email'));
    }

    public function aboutIndex(){

      $data = pages::leftJoin('users', function($join) {
          $join->on('pages.author', '=', 'users.id');
        })
        ->where('pages.pages_id',1)
        ->first();

      return view('Frontend.about')
              ->with('data', (object)$data);
    }

    public function book(){
      return view('Frontend.book');
    }

    public function mail(){

      // Insert new mail service
      
    } 

    public function getUserInfo(){
      $username = Request::input('username');

      $result = customers::where('username', $username)->first();
      
      return ['name' => $result['first_name'] . ' '.$result['last_name'], 'email' => $result['email'] ];
    }

    public static function _getSetting( $config ){
        $setting = DB::table('settings')
        ->select('setting_value')
        ->where('setting_name', $config)
        ->get();

        return $setting[0]->setting_value;  
    }
}
?>