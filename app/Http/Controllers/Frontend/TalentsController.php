<?php
namespace App\Http\Controllers\Frontend;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;

//for pagination
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Models\Backend\Setting;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\model_list; 
use App\Http\Models\Backoffice\model_gallery;
use App\Http\Models\Backoffice\experience;
use App\Http\Models\Backoffice\categories;

use App\Http\Models\Backoffice\shortlists;
use App\Http\Models\Backoffice\customers;

use PDF;

class TalentsController extends Controller{

    public function index(){
    	return view('Frontend.talents');
    }

    public function getTalents($username){

        // Talents
        $result = model_list::take(1000)->orderBy('created_at','desc')->get();

        $talents = [];
        $categories = [];
        foreach ($result as $key){

            $exp = explode(',', $key->experiences);
            $experience = experience::where('id', $exp[0])->first();

            $categoriesArray = explode(',', $key->categories);
            foreach ($categoriesArray as $catKey){
              $category = categories::where('id', $catKey)->first(['name']);

              array_push($categories, $category->name);
            }

            $selected = false;
            if($username !== 'none'){
              $customer = customers::where('username', $username)->first();
              $customerId = (int)$customer->id;
              $talentId = (int)$key->id;
              $exist = shortlists::where('customer_id', $customerId)->where('talent_id', $talentId)->count();
              if($exist > 0){
                $selected = true;
              }
            }

            $talents[] = [ 'id'           => $key->id, 
                          'model_name'    => $key->model_name, 
                          'categories'    => $categories,
                          'country'       => $key->country,
                          'height'        => $key->height,
                          'bust_chest'    => $key->bust_chest,
                          'waist'         => $key->waist,
                          'hips'          => $key->hips,
                          'shoe_size'     => $key->shoe_size,
                          'dress_size'    => $key->dress_size,
                          'slug'          => $key->slug, 
                          'experience'    => ($key->experiences == 'NaN') ? 'N/A' : $experience->name,
                          'primary_photo' => $key->primary_photo,
                          'selected'      => $selected 
                        ];

            unset($categories);
            $categories = [];
        }

        return $talents;
    }

    public function shortlist(){

      $customer = customers::where('username', Request::input('username'))->first();

      $customerId = (int)$customer->id;
      $talentId = (int)Request::input('id');

      $exist = shortlists::where('customer_id', $customerId)->where('talent_id', $talentId)->count();

      if($exist == 0){

        $shortlist = new shortlists;
        $shortlist->customer_id = $customerId;
        $shortlist->talent_id = $talentId;
        $shortlist->save();
      
        return ['status' => 'true', 'message' => 'The talent was successfully added to your list' ];  
      } else{
        return ['status' => 'false', 'message' => 'The talent is already in your list.' ];  
      }
    }

    public function removeShortlist(){
      $customer = customers::where('username', Request::input('username'))->first();

      $customerId = (int)$customer->id;
      $talentId = (int)Request::input('id');

      $exist = shortlists::where('customer_id', $customerId)->where('talent_id', $talentId)->count();

      if($exist > 0){

        shortlists::where('customer_id', $customerId)->where('talent_id', $talentId)->delete();
      
        return ['status' => 'true', 'message' => 'The talent was successfully removed from your list' ];  
      } else{
        return ['status' => 'false', 'message' => 'The talent does not exist in your list.' ];  
      }
    }

    // CUSTOMER PAGE
    public function customerIndex(){
      return view('Frontend.customerTalents');
    }

    public function customerShortlist($username){

      // Talents
       //$result = model_list::take(10)->orderBy('created_at','desc')->get();
        $customer = customers::where('username', $username)->first();
        $customerId = (int)$customer->id;

       $result = shortlists::leftJoin('model_list', function($join) {
              $join->on('shortlists.talent_id', '=', 'model_list.id');
            })
            ->where('shortlists.customer_id', '=', $customerId)
            ->get();

        $talents = [];
        $categories = [];
        foreach ($result as $key){

            $exp = explode(',', $key->experiences);
            $experience = experience::where('id', $exp[0])->first();

            $categoriesArray = explode(',', $key->categories);
            foreach ($categoriesArray as $catKey){
              $category = categories::where('id', $catKey)->first(['name']);

              array_push($categories, $category->name);
            }

            $selected = false;

            $talents[] = [ 'id'           => $key->id, 
                          'model_name'    => $key->model_name, 
                          'categories'    => $categories,
                          'country'       => $key->country,
                          'height'        => $key->height,
                          'bust_chest'    => $key->bust_chest,
                          'waist'         => $key->waist,
                          'hips'          => $key->hips,
                          'shoe_size'     => $key->shoe_size,
                          'dress_size'    => $key->dress_size,
                          'slug'          => $key->slug, 
                          'experience'    => $experience->name,
                          'primary_photo' => $key->primary_photo,
                          'selected'      => $selected 
                        ];

            unset($categories);
            $categories = [];
        }

        return $talents;
    }

    public function genertePdf($id){

        $data = model_list::where('id', $id)->first();

        $exp = explode(',', $data['experiences']);
        $experience = experience::where('id', $exp[0])->first(['name']);

        PDF::SetTitle('Compcard');
        PDF::AddPage();
        $html  = '';
        $html .='<table cellpadding="2" cellspacing="2" width="570" height="300px"><tr><td><img width="150" src="'.asset('/img/profiles/small/'.$data['primary_photo'] ).'" /></td>';

        $galleryQuery = model_gallery::where('user_id', $data['id']);
        $gallery = [];
         if($galleryQuery->count() > 0) {
            $gallery = $galleryQuery->get();
            $count = 0;
             foreach ($gallery as $key){  
              if ( $count < 2 ) {
                $html .= '<td valign="bottom"><img width="150" src="'.asset('/img/uploads/small/'.$key['image_name'] ).'" /></td>';
              }
              $count++;
             }  
         }

        $html .='</tr></table>';
        
        $html .='<table cellpadding="8" cellspacing="2" ><tr>';
        $html .= '<td bgcolor="#e51760" style="font-size:9px;color:#ffffff">';
        $html .= '<h3 style="color:#0f0f10;font-size:18px;">'.$data['model_name'].'</span><br/><span style="color:#fff; font-size:10px;">'.$experience->name.'</h3>';
        
        $html .= $data['country'] ? '<label style="color:#ffdde9">Country: </label><b>'.$data['country'].'</b> • ' : '';
        $html .= $data['height'] ? '<label style="color:#ffdde9">Height: </label><b>'.$data['height'].'</b> • ' : '';
        $html .= $data['bust_chest'] ? '<label style="color:#ffdde9">Bust/Chest: </label><b>'.$data['bust_chest'].'</b> • ' : '';
        $html .= $data['waist'] ? '<label style="color:#ffdde9">Waist: </label><b>'.$data['waist'].'</b> • ' : '';
        $html .= $data['hips'] ? '<label style="color:#ffdde9">Hips: </label><b>'.$data['hips'].'</b> • ' : '';
        $html .= $data['shoe_size'] ? '<label style="color:#ffdde9">Shoes :</label><b>'.$data['shoe_size'].'</b> • ' : '';
        $html .= $data['dress_size'] ? '<label style="color:#ffdde9">Dress: </label><b>'.$data['dress_size'].'</b>' : '';
        $html .= '</td>';
        $html .= '<td bgcolor="#0f0f10" align="left">';
        $html .= '<h3><img width="100" src="'.asset('/img/main/logo2.png').'"/></h3>';
        $html .= '<span style="color:#fff;font-size:8px;">Choice Model Management <br/> Address: '.self::_getSetting('address').' <br/> Globe: '.self::_getSetting('globe_phone').' <br/> Phone: '.self::_getSetting('phone').'  <br/> Email: <span style="color:#e51760">'.self::_getSetting('email').'</span></span>';
        $html .= '</td>';
        $html .='</tr></table>';    

        PDF::setPageOrientation('P');     
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('compcard.pdf');
    }

    public static function _getSetting( $config ){
        $setting = DB::table('settings')
        ->select('setting_value')
        ->where('setting_name', $config)
        ->get();

        return $setting[0]->setting_value;  
    }

}
?>