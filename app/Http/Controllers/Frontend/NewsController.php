<?php
namespace App\Http\Controllers\Frontend;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;

//for pagination
use Illuminate\Pagination\LengthAwarePaginator;
//use App\Http\Models\Backend\Setting;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\news;
use App\Http\Models\Backoffice\users;

class NewsController extends Controller{

    public function index(){
    	return view('Frontend.newslist');
    }

    public function getNews(){

        $news = news::leftJoin('users', function($join) {
          $join->on('news.author', '=', 'users.id');
        })
        ->get();

        $data = [];
        $count = 0;
        foreach ($news as $key){
            $data[] = [ 'news_id' => $key->news_id, 
                        'title' => $key->title,
                        'summary' => $key->summary,
                        'description' => $key->description,
                        'featured_image' => $key->featured_image,
                        'status' => $key->status,
                        'author' => $key->author,
                        'slug' => str_replace(' ','-', strtolower($key->title)),
                        'name' => $key->first_name . ' '.$key->last_name,
                        'date' => date('F j, Y', strtotime($key->created_at)),
                        'selected' => false ];
        $count++;
        }

       return $data;
    }

    public function indexNewsItem($id){

      $news = news::leftJoin('users', function($join) {
        $join->on('news.author', '=', 'users.id');
      })
      ->where('news.news_id', $id)
      ->first();

        $data = [];
        $data = [ 'news_id' => $news->news_id, 
                    'title' => $news->title,
                    'summary' => $news->summary,
                    'description' => $news->description,
                    'featured_image' => $news->featured_image,
                    'status' => $news->status,
                    'author' => $news->author,
                    'slug' => str_replace(' ','-', strtolower($news->title)),
                    'name' => $news->first_name . ' '.$news->last_name,
                    'date' => date('F j, Y', strtotime($news->created_at)),
                     ];

      return view('Frontend.newsitem')
             ->with('data', (object)$data);
    }

}
?>