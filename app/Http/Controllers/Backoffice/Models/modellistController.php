<?php
namespace App\Http\Controllers\Backoffice\Models;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use Image;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\model_list; 
use App\Http\Models\Backoffice\model_gallery;

class modelListController extends Controller{

    public function view(){
        return view('Backoffice.models.modellist');
    }

    public function index(){

        $result = model_list::get();

        $data = [];
        $count = 0;
        foreach ($result as $key){

            $data[] = [ 'id' => $key->id, 
                        'model_name' => $key->model_name, 
                        'slug' => $key->slug, 
                        'country' => $key->country, 
                        'height' => $key->height, 
                        'bust_chest' => $key->bust_chest,
                        'waist' => $key->waist,
                        'hips' => $key->hips,
                        'shoe_size' => $key->shoe_size,
                        'dress_size' => $key->dress_size,
                        'notes' => $key->notes,
                        'primary_photo' => $key->primary_photo,
                        'experiences' => $key->experiences,
                        'categories' => $key->categories,
                        'featured_model' => $key->featured_model,
                        'order' => $key->order,
                        'created_at' => $key->created_at,
                        'selected' => false ];
        $count++;
        }

        return $data;
    }

    public function store() {
        $result = new model_list;
        $result->model_name = Request::input('model_name');
        $result->slug = Request::input('slug');
        $result->country = Request::input('country');
        $result->height = Request::input('height');
        $result->bust_chest = Request::input('bust_chest');
        $result->waist = Request::input('waist');
        $result->hips = Request::input('hips');
        $result->shoe_size = Request::input('shoe_size');
        $result->dress_size = Request::input('dress_size');
        $result->notes = Request::input('notes');
        $result->primary_photo = Request::input('primary_photo');
        $result->experiences = Request::input('experiences');
        $result->categories = Request::input('categories');
        $result->featured_model = Request::input('featured_model');
        $result->order = Request::input('order');
        $result->save();

        return [ 'id' => $result->id, 
                 'model_name' => $result->model_name, 
                 'slug' => $result->slug, 
                 'country' => $result->country, 
                 'height' => $result->height, 
                 'bust_chest' => $result->bust_chest,
                 'waist' => $result->waist,
                 'hips' => $result->hips,
                 'shoe_size' => $result->shoe_size,
                 'dress_size' => $result->dress_size,
                 'notes' => $result->notes,
                 'primary_photo' => $result->primary_photo,
                 'experiences' => $result->experiences,
                 'categories' => $result->categories,
                 'featured_model' => $result->featured_model,
                 'order' => $result->order,
                 'selected' => false ];
    }

    public function update($id){
        $result = model_list::find($id);
        $result->model_name = Request::input('model_name');
        $result->slug = Request::input('slug');
        $result->country = Request::input('country');
        $result->height = Request::input('height');
        $result->bust_chest = Request::input('bust_chest');
        $result->waist = Request::input('waist');
        $result->hips = Request::input('hips');
        $result->shoe_size = Request::input('shoe_size');
        $result->dress_size = Request::input('dress_size');
        $result->notes = Request::input('notes');
        $result->primary_photo = Request::input('primary_photo');
        $result->experiences = Request::input('experiences');
        $result->categories = Request::input('categories');
        $result->featured_model = Request::input('featured_model');
        $result->order = Request::input('order');
        $result->save();
 
        return $result;
    }

    public function destroy($id){
        model_list::destroy($id);
    }

    public function upload(){

        $fileName = '';
        if (Request::hasFile('file')) { 
                $path = public_path() .'/img/profiles/large'; 
                $fileName = rand(11111,99999).'_'.Request::file('file')->getClientOriginalName();
                Request::file('file')->move( $path, $fileName);

                //Small
                $smallPath = public_path() .'/img/profiles/small/'.$fileName;
                Image::make($path.'/'.$fileName)->resize(200, null, function ($constraint) { $constraint->aspectRatio(); })->save($smallPath);

                //Medium
                $mediumPath = public_path() .'/img/profiles/medium/'.$fileName;
                Image::make($path.'/'.$fileName)->resize(400, null, function ($constraint) { $constraint->aspectRatio(); })->save($mediumPath);
        } 

        return $fileName;
    }

    public function uploadGallery(){

        $fileName = '';
        if (Request::hasFile('file')) { 
            $path = public_path() .'/img/uploads/large'; 
            $image = Request::file('file');
            $fileName = rand(11111,99999).'_'.Request::file('file')->getClientOriginalName();
            $image->move( $path, $fileName);

            //Small
            $smallPath = public_path() .'/img/uploads/small/'.$fileName;
            Image::make($path.'/'.$fileName)->resize(200, null, function ($constraint) { $constraint->aspectRatio(); })->save($smallPath);

            //Medium
            $mediumPath = public_path() .'/img/uploads/medium/'.$fileName;
            Image::make($path.'/'.$fileName)->resize(400, null, function ($constraint) { $constraint->aspectRatio(); })->save($mediumPath);
        
            $result = new model_gallery;
            $result->image_name = $fileName;
            $result->user_id = Request::input('id');
            $result->featured = 'no';
            $result->save();
        }

        return $fileName;

    }

    public function getGallery($id){

        $result = model_gallery::where('user_id', $id)->get();

        $data = [];
        foreach ($result as $key){
            $data[] = [ 'id' => $key->id, 
                        'image_name' => $key->image_name, 
                        'caption' => $key->caption, 
                        'order' => $key->order, 
                        'user_id' => $key->user_id, 
                        'featured' => $key->featured
                      ];
        }

        return $data;
    }

    public function setfeaturedSettings($userid, $id){
        // Set all to No
        $result = model_gallery::where('user_id', $userid)->update(['featured' => 'no']);

        if($result > 0){
            model_gallery::where('id', $id)->update(['featured' => 'yes']);
        }
    }

    public function removePhoto($photoId){
        model_gallery::destroy($photoId);
    }

    public function getCaption($id){

        $result = model_gallery::where('id', $id)->first();

        return ['caption' => $result->caption, 'order' => $result->order];
    }

    public function saveCaption(){

        $result = model_gallery::where('id', Request::input('galleryId'))
                    ->update(['caption' => Request::input('captionMessage'),
                              'order' => Request::input('order')]);

        return 'true';
    }
}
?>