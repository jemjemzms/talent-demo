<?php
namespace App\Http\Controllers\Backoffice\Models;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\experience;

class experienceController extends Controller{

    public function view(){

        return view('Backoffice.models.experience');
    }

    public function index(){

    	$result = experience::get();

        $data = [];
        $count = 0;
        foreach ($result as $key){
            $data[] = [ 'id' => $key->id, 
                        'name' => $key->name, 
                        'slug' => $key->slug, 
                        'description' => $key->description,
                        'selected' => false ];
        $count++;
        }

        return $data;
    }

    public function store() {
        $result = new experience;
        $result->name = Request::input('name');
        $result->slug = Request::input('slug');
        $result->description = Request::input('description');
        $result->save();

        return [ 'id' => $result->id, 
                 'name' => $result->name, 
                 'slug' => $result->slug, 
                 'description' => $result->description,
                 'selected' => false ];
    }

    public function update($id){

        $result = experience::find($id);
        $result->name = Request::input('name');
        $result->slug = Request::input('slug');
        $result->description = Request::input('description');
        $result->save();
 
        return $result;
    }

    public function destroy($id){
        experience::destroy($id);
    }
}
?>