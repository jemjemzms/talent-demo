<?php
namespace App\Http\Controllers\Backoffice\Models;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\categories;

class categoriesController extends Controller{

    public function view(){

        return view('Backoffice.models.categories');
    }

    public function index(){

    	$result = categories::get();

        $data = [];
        $count = 0;
        foreach ($result as $key){
            $data[] = [ 'id' => $key->id, 
                        'name' => $key->name, 
                        'slug' => $key->slug, 
                        'description' => $key->description,
                        'selected' => false ];
        $count++;
        }

        return $data;
    }

    public function store() {
        $result = new categories;
        $result->name = Request::input('name');
        $result->slug = Request::input('slug');
        $result->description = Request::input('description');
        $result->save();

        return [ 'id' => $result->id, 
                 'name' => $result->name, 
                 'slug' => $result->slug, 
                 'description' => $result->description,
                 'selected' => false ];
    }

    public function update($id){

        $result = categories::find($id);
        $result->name = Request::input('name');
        $result->slug = Request::input('slug');
        $result->description = Request::input('description');
        $result->save();
 
        return $result;
    }

    public function destroy($id){
        categories::destroy($id);
    }
}
?>