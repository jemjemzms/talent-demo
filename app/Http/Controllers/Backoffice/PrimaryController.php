<?php
namespace App\Http\Controllers\Backoffice;

use DB;
use Redirect;
use Input;
use Session;
use Illuminate\Http\Request; //save post
use App\Http\Controllers\Controller;

//for pagination
use Illuminate\Pagination\LengthAwarePaginator;
//use App\Http\Models\Backend\Setting;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\model_list; 

class PrimaryController extends Controller{

    public function dashboard(){
    	return view('Backoffice.primary');
    }

    public function getDashData(){
        $data = [
                'totalAlbum' => model_list::count(),
                ]; 

        return $data; 
    }

    public function index(){
    	
        if(Sentinel::check() == null){
          \Session::flash('message', 'You are not authorized to access the page.');
          return redirect('/backoffice/login');
        }
        
    	$config = [
                  'url' => asset('/'),
                  ];
                  

    	return view('Backoffice.index')
    			->with('config', $config);
    }

}
?>