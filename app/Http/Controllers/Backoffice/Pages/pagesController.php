<?php
namespace App\Http\Controllers\Backoffice\pages;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\pages;
use App\Http\Models\Backoffice\users;

class pagesController extends Controller{

    public function view(){

        return view('Backoffice.pages.pages');
    }

    public function index(){

        $pages = pages::leftJoin('users', function($join) {
          $join->on('pages.author', '=', 'users.id');
        })
        ->get();


        $data = [];
        $count = 0;
        foreach ($pages as $key){
            $data[] = [ 'page_id' => $key->pages_id, 
                        'title' => $key->title,
                        'description' => $key->description,
                        'author' => $key->author,
                        'name' => $key->first_name.' '.$key->last_name,
                        'selected' => false ];
        $count++;
        }

       return $data;
    }

    public function store() {
        $pages = new pages;
        $pages->title = Request::input('title');
        $pages->description = Request::input('description');
        $pages->author = Session::get('id');
        $pages->save();

        $users = Users::where('id', $pages->author)->first();

        return [ 'page_id' => $pages->pages_id, 
                 'title' => $pages->title,
                 'description' => $pages->description,
                 'author' => $pages->author,
                 'name' => $users->first_name.' '.$users->last_name,
                 'selected' => false ];
    }

    public function update($id){

        $pages = pages::find($id);
        $pages->title = Request::input('title');
        $pages->description = Request::input('description');
        $pages->author = Session::get('id');
        $pages->save();
 
        return $pages;
    }

    public function destroy($id){
        pages::destroy($id);
    }
}
?>