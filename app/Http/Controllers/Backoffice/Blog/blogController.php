<?php
namespace App\Http\Controllers\Backoffice\blog;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use Image;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\blog;
use App\Http\Models\Backoffice\users;

class blogController extends Controller{

    public function view(){

        return view('Backoffice.blog.blog');
    }

    public function index(){

        $blog = blog::leftJoin('users', function($join) {
          $join->on('blog.author', '=', 'users.id');
        })
        ->get();


        $data = [];
        $count = 0;
        foreach ($blog as $key){
            $data[] = [ 'blog_id' => $key->blog_id, 
                        'title' => $key->title,
                        'summary' => $key->summary,
                        'description' => $key->description,
                        'featured_image' => $key->featured_image,
                        'status' => $key->status,
                        'author' => $key->author,
                        'name' => $key->first_name . ' '.$key->last_name,
                        'selected' => false ];
        $count++;
        }

       return $data;
    }

    public function store() {
        $blog = new blog;
        $blog->title = Request::input('title');
        $blog->summary = Request::input('summary');
        $blog->description = Request::input('description');
        $blog->status = Request::input('status');
        $blog->author = Session::get('id');
        $blog->featured_image = Request::input('featured_image');
        $blog->save();

        $users = Users::where('id', $blog->author)->first();

        return [ 'blog_id' => $blog->blog_id, 
                 'title' => $blog->title,
                 'summary' => $blog->summary,
                 'description' => $blog->description,
                 'featured_image' => $blog->featured_image,
                 'status' => $blog->status,
                 'author' => $blog->author,
                 'name' => $users->first_name. ' '.$users->first_name,
                 'selected' => false ];
    }

    public function update($id){

        $blog = blog::find($id);
        $blog->title = Request::input('title');
        $blog->summary = Request::input('summary');
        $blog->description = Request::input('description');
        $blog->status = Request::input('status');
        $blog->author = Session::get('id');
        $blog->featured_image = Request::input('featured_image');
        $blog->save();
 
        return $blog;
    }

    public function destroy($id){
        blog::destroy($id);
    }

    public function upload(){

        $fileName = '';
        if (Request::hasFile('file')) { 
                $path = public_path() .'/img/news/large'; 
                $fileName = rand(11111,99999).'_'.Request::file('file')->getClientOriginalName();
                Request::file('file')->move( $path, $fileName);

                //Small
                $smallPath = public_path() .'/img/news/small/'.$fileName;
                Image::make($path.'/'.$fileName)->resize(200, null, function ($constraint) { $constraint->aspectRatio(); })->save($smallPath);

                //Medium
                $mediumPath = public_path() .'/img/news/medium/'.$fileName;
                Image::make($path.'/'.$fileName)->resize(400, null, function ($constraint) { $constraint->aspectRatio(); })->save($mediumPath);
        } 

        return $fileName;
    }
}
?>