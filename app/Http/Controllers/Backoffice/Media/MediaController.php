<?php
namespace App\Http\Controllers\Backoffice\Media;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use Image;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\Http\Models\Backoffice\media;
use App\Http\Models\Backoffice\users;

class MediaController extends Controller{

    public function view(){

        return view('Backoffice.media.media');
    }

    public function index(){

        $media = media::leftJoin('users', function($join) {
          $join->on('media.author', '=', 'users.id');
        })
        ->get();


        $data = [];
        foreach ($media as $key){
            $data[] = [ 'media_id' => $key->media_id, 
                        'title' => $key->title,
                        'summary' => $key->summary,
                        'description' => $key->description,
                        'featured_image' => $key->featured_image,
                        'status' => $key->status,
                        'author' => $key->author,
                        'name' => $key->first_name . ' '.$key->last_name,
                        'selected' => false ];
        }

       return $data;
    }

    public function store() {
        $media = new media;
        $media->title = Request::input('title');
        $media->summary = Request::input('summary');
        $media->description = Request::input('description');
        $media->status = Request::input('status');
        $media->author = Session::get('id');
        $media->featured_image = Request::input('featured_image');
        $media->save();

        $users = Users::where('id', $media->author)->first();

        return [ 'media_id' => $media->media_id, 
                 'title' => $media->title,
                 'summary' => $media->summary,
                 'description' => $media->description,
                 'featured_image' => $media->featured_image,
                 'status' => $media->status,
                 'author' => $media->author,
                 'name' => $users->first_name. ' '.$users->first_name,
                 'selected' => false ];
    }

    public function update($id){

        $media = media::find($id);
        $media->title = Request::input('title');
        $media->summary = Request::input('summary');
        $media->description = Request::input('description');
        $media->status = Request::input('status');
        $media->author = Session::get('id');
        $media->featured_image = Request::input('featured_image');
        $media->save();
 
        return $media;
    }

    public function destroy($id){
        media::destroy($id);
    }

    public function upload(){

        $fileName = '';
        if (Request::hasFile('file')) { 
                $path = public_path() .'/img/media/large'; 
                $fileName = rand(11111,99999).'_'.Request::file('file')->getClientOriginalName();
                Request::file('file')->move( $path, $fileName);

                //Small
                $smallPath = public_path() .'/img/media/small/'.$fileName;
                Image::make($path.'/'.$fileName)->resize(200, null, function ($constraint) { $constraint->aspectRatio(); })->save($smallPath);

                //Medium
                $mediumPath = public_path() .'/img/media/medium/'.$fileName;
                Image::make($path.'/'.$fileName)->resize(400, null, function ($constraint) { $constraint->aspectRatio(); })->save($mediumPath);
        } 

        return $fileName;
    }
}
?>