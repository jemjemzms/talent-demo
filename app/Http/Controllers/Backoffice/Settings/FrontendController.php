<?php
namespace App\Http\Controllers\Backoffice\Settings;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;
use App\Http\Models\BackEnd\Setting;

class FrontendController extends Controller{

	public function view(){

        return view('Backoffice.settings.frontend');
    }

    public function getFrontendSettings(){

    	date_default_timezone_set(self::_getSetting('timezone'));

    	$data = [
    			'talent_display_count'   => self::_getSetting('talent_display_count'),
                'address'                => self::_getSetting('address'),
                'globe_phone'            => self::_getSetting('globe_phone'),
                'phone'                  => self::_getSetting('phone'),
                'email'                  => self::_getSetting('email'), 
                'music_app'              => self::_getSetting('music_app'),
                'fb_app'                 => self::_getSetting('fb_app'),
                'google_analytics_api'   => self::_getSetting('google_analytics_api'),
                'slide_box_height'               => self::_getSetting('slide_box_height'),
                'slide_box_width'                => self::_getSetting('slide_box_width'),
                'slide_box_line_height'          => self::_getSetting('slide_box_line_height'),
                'menu_font_size'                 => self::_getSetting('menu_font_size'), 
                'menu_font_weight'               => self::_getSetting('menu_font_weight'),
                'menu_text_transform'            => self::_getSetting('menu_text_transform'),
                'slide_font_size'                => self::_getSetting('slide_font_size'),
                'slide_line_height'              => self::_getSetting('slide_line_height'),
                'slide_text_shadow'              => self::_getSetting('slide_text_shadow'),
                'slide_description_font_size'    => self::_getSetting('slide_description_font_size'),
                'slide_description_margin_top'   => self::_getSetting('slide_description_margin_top'),
                'slide_description_text_shadow'  => self::_getSetting('slide_description_text_shadow'),
    			];

    	return $data;
    }

    public function saveFrontendSettings(){

    	$settings = array(
                            'talent_display_count'  =>  Request::input('talent_display_count'),
                            'address'               =>  Request::input('address'),
                            'globe_phone'           =>  Request::input('globe_phone'),
                            'phone'                 =>  Request::input('phone'),
                            'email'                 =>  Request::input('email'),
                            'music_app'             =>  Request::input('music_app'),
                            'fb_app'                =>  Request::input('fb_app'),
                            'google_analytics_api'  =>  Request::input('google_analytics_api'),
                            'slide_box_height'              =>  Request::input('slide_box_height'),
                            'slide_box_width'               =>  Request::input('slide_box_width'),
                            'slide_box_line_height'         =>  Request::input('slide_box_line_height'),
                            'menu_font_size'                =>  Request::input('menu_font_size'),
                            'menu_font_weight'              =>  Request::input('menu_font_weight'),
                            'menu_text_transform'           =>  Request::input('menu_text_transform'),
                            'slide_font_size'               =>  Request::input('slide_font_size'),
                            'slide_line_height'             =>  Request::input('slide_line_height'),
                            'slide_text_shadow'             =>  Request::input('slide_text_shadow'),
                            'slide_description_font_size'   =>  Request::input('slide_description_font_size'),
                            'slide_description_margin_top'  =>  Request::input('slide_description_margin_top'),
                            'slide_description_text_shadow' =>  Request::input('slide_description_text_shadow'),
                        );


        foreach($settings as $key => $value){
        	DB::table('settings')->where('setting_name', $key)->update(['setting_value' => $value]);
        }
    }

    public static function _getSetting( $config ){
    	$setting = DB::table('settings')
    	->select('setting_value')
    	->where('setting_name', $config)
    	->get();

    	return $setting[0]->setting_value;  
    }

}