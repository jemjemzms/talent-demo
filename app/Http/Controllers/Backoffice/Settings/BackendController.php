<?php
namespace App\Http\Controllers\Backoffice\Settings;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;
use App\Http\Models\BackEnd\Setting;

class BackendController extends Controller{

	public function view(){

        return view('Backoffice.settings.backend');
    }

    public function getBackendSettings(){

    	date_default_timezone_set(self::_getSetting('timezone'));

    	$data = [
    			'modellist_display_count'   => self::_getSetting('modellist_display_count'),
    			'experience_display_count'  => self::_getSetting('experience_display_count'),
    			'categories_display_count'  => self::_getSetting('categories_display_count'),
                'customers_display_count'   => self::_getSetting('customers_display_count'),
    			'pages_display_count'       => self::_getSetting('pages_display_count'),
    			'news_display_count'        => self::_getSetting('news_display_count') 
    			];

    	return $data;
    }

    public function saveBackendSettings(){

    	$settings = array(
                            'modellist_display_count'  =>  Request::input('modellist_display_count'),
                            'experience_display_count' =>  Request::input('experience_display_count'),
                            'categories_display_count' =>  Request::input('categories_display_count'),
                            'customers_display_count'  =>  Request::input('customers_display_count'),
                            'pages_display_count'      =>  Request::input('pages_display_count'),
                            'news_display_count'       =>  Request::input('news_display_count')
                        );


        foreach($settings as $key => $value){
        	DB::table('settings')->where('setting_name', $key)->update(['setting_value' => $value]);
        }
    }

    public static function getAppName(){

        return self::_getSetting('applicationname');
    }

    public function getPageCount($setting){

    	return self::_getSetting($setting);
    }

    public static function _getSetting( $config ){
    	$setting = DB::table('settings')
    	->select('setting_value')
    	->where('setting_name', $config)
    	->get();

    	return $setting[0]->setting_value;  
    }

}