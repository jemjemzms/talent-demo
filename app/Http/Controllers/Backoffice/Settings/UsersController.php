<?php
namespace App\Http\Controllers\Backoffice\Settings;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Capsule\Manager as Capsule;

use App\Http\Models\Backoffice\users;

class UsersController extends Controller{

	public function view(){

        return view('Backoffice.settings.users');
    }

    public function index(){

        $users = Users::get();

        $data = [];
        foreach ($users as $user){
            $data[] = [ 'id' => $user->id, 
                        'email' => $user->email,
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'last_login' => $user->last_login,
                        'selected' => false ];
        }

        return $data;
    }

    public function store() {
    
        if(Request::input('type') == 'adduser'){
            $credentials = [
                        'email'        => Request::input('email'),
                        'password'     => Request::input('password'),
                        'first_name'   => Request::input('first_name'),
                        'last_name'    => Request::input('last_name'),
                    ];

            $users = Sentinel::registerAndActivate($credentials);

            if (isset($users['id'])){
                return [ 'id' => $users['id'], 
                         'email' => $users['email'],
                         'first_name' => $users['first_name'],
                         'last_name' => $users['last_name'],
                         'password' => $users['password'],
                         'selected' => false ];
            }

        }

        if(Request::input('type') == 'changepass'){

            $user = Sentinel::findById(Request::input('id'));

            $credentials = [
                'password' => Request::input('password'),
            ];

            Sentinel::update($user, $credentials);

        }
    }

    public function update($id){

        $users = Users::find($id);
        $users->email = Request::input('email');
        $users->save();
 
        return $users;
    }

    public function destroy($id){
        Users::destroy($id);
    }

}