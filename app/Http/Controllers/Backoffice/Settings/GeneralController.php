<?php
namespace App\Http\Controllers\Backoffice\Settings;

use DB;
use Redirect;
use Input;
use Session;
use Request;
use App\Http\Controllers\Controller;
use App\Http\Models\BackEnd\Setting;

class GeneralController extends Controller{

	public function view(){

        return view('Backoffice.settings.general');
    }

    public function getGeneralSettings(){

    	date_default_timezone_set(self::_getSetting('timezone'));

    	$data = [
                'applicationname' => self::_getSetting('applicationname'),
    	        'timezone' => [['title' => 'Manila',
    	                        'format' => 'Asia/Manila'], 
    	                        ['title' => 'Singapore',
    	                        'format' => 'Asia/Singapore'],
    	                        ['title' => 'Hong Kong',
    	                        'format' => 'Asia/Hong_Kong'],
    			                ['title' => 'Taipei',
    	                        'format' => 'Asia/Taipei'],
    			                ['title' => 'Kuala Lumpur',
    	                        'format' => 'Asia/Kuala_Lumpur'],
    	                        ['title' => 'Macau',
    	                        'format' => 'Asia/Macau']
    	                       ],
    			'dateformat' => [
    							 ['date' => date('F j, Y'), 'format' => 'F j, Y'],
    							 ['date' => date('Y-m-d'), 'format' => 'Y-m-d'],
    							 ['date' => date('m-d-Y'), 'format' => 'm-d-Y'],
    							 ['date' => date('d-m-Y'), 'format' => 'd-m-Y']
    							],
    			'timeformat' => [
    							 ['time' => date('g:i a'), 'format' => 'g:i a'],
    							 ['time' => date('g:i A'), 'format' => 'g:i A'],
    							 ['time' => date('H:i'), 'format' => 'H:i']
    							],
    			'defaultappname' => self::_getSetting('applicationname'),
    			'defaulttimezone' => self::_getSetting('timezone'),
    			'defaultdateformat' => self::_getSetting('dateformat'),
    			'defaulttimeformat' => self::_getSetting('timeformat')
    			];

    	return $data;
    }

    public function saveGeneralSettings(){

    	$settings = array(
                            'applicationname'  =>  Request::input('appName'),
                            'timezone'         =>  Request::input('timeZone'),
                            'dateformat'       =>  Request::input('dateFormat'),
                            'timeformat'       =>  Request::input('timeFormat')
                        );


        foreach($settings as $key => $value){
        	DB::table('settings')->where('setting_name', $key)->update(['setting_value' => $value]);
        }
    }

    public static function _getSetting( $config ){
    	$setting = DB::table('settings')
    	->select('setting_value')
    	->where('setting_name', $config)
    	->get();

    	return $setting[0]->setting_value;  
    }

}