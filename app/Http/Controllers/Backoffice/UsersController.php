<?php
namespace App\Http\Controllers\Backoffice;

use DB;
use Redirect;
use Input;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Capsule\Manager as Capsule;

use Illuminate\Pagination\LengthAwarePaginator;

class UsersController extends Controller{

		public function index(){

        if(Sentinel::check() == null){
            \Session::flash('message', 'You are not authorized to access the page.');
            return redirect('backoffice/login');
        }


        $result = DB::table('users')
                ->paginate(20);

        	return view('Backoffice.user.index')->with('data',$result);
        }

        public function register(){
        if(Sentinel::check() == null){
            \Session::flash('message', 'You are not authorized to access the page.');
            return redirect('backoffice/login');
        }

        	return view('Backoffice.user.register');
        }

        public function edit($id){

            if(Sentinel::check() == null){
            \Session::flash('message', 'You are not authorized to access the page.');
            return redirect('backoffice/login');
            }
        
            $users = DB::table('users')
                      ->where('id', $id)->first();

            $roles = DB::table('role_users')
                      ->select('roles.slug', 'roles.name')
                      ->leftJoin('roles', 'roles.id', '=', 'role_users.role_id')
                      ->where('role_users.user_id', $users->id)->first();

            $rolesName = isset($roles->name) ? $roles->name : '';
            $rolesSlug = isset($roles->slug) ? $roles->slug : '';

            return view('Backoffice.user.edit')
                    ->with('name', $rolesName)
                    ->with('slug', $rolesSlug)
                    ->with('row', $users);
        }

        public function save(Request $request){

        	$post = $request->all();
        	$v = \Validator::make($request->all(),
    		[
                'email'          => 'required',
                'password'       => 'required',
                'repeatpassword' => 'required',
            ]);

        	if($v->fails()){
				return redirect()->back()->withErrors($v->errors());
			} else{

                if($post['password'] == $post['repeatpassword']){

    	        	$credentials = [
    				    'email'        => $post['email'],
    				    'password'     => $post['password'],
    				    'first_name'   => $post['firstname'],
    				    'last_name'    => $post['lastname'],
    				];

    				$user = Sentinel::registerAndActivate($credentials);

                    if (isset($user['id'])){
    				    
                        $user = Sentinel::findById($user->id);
                        $role = Sentinel::findRoleByName($post['role']);
                        $role->users()->attach($user);

                        \Session::flash('message', 'The user has been added.');
                    	return redirect('backoffice/users');
    				} else{
    		       		\Session::flash('message', 'Invalid username and password.');
    	                return Redirect::back();
    	            }
                } else{
                    \Session::flash('message', 'The password did not match.');
                    return Redirect::back();
                }
			} 
        }

        public function update(Request $request){

            $post = $request->all();
            $v = \Validator::make($request->all(),
            [
                'email'          => 'required',
            ]);

            if($v->fails()){
                return redirect()->back()->withErrors($v->errors());
            } else{

                if(!empty($post['password'])){
                    
                    if($post['password'] == $post['repeatpassword']){

                        $credentials = [
                        'email'        => $post['email'],
                        'password'     => $post['password'],
                        'first_name'   => $post['firstname'],
                        'last_name'    => $post['lastname'],
                    ];

                    } else{
                       \Session::flash('message', 'Invalid username and password.');
                            return Redirect::back();
                    }
                } else{

                    $credentials = [
                        'email'        => $post['email'],
                        'first_name'   => $post['firstname'],
                        'last_name'    => $post['lastname'],
                    ];
                }

                    $user = Sentinel::update($post['id'], $credentials);

                    if (isset($user['id'])){
                        
                        $user1 = Sentinel::findById($post['id']);
                        $role1 = Sentinel::findRoleByName($post['roleName']);
                        $role1->users()->detach($user1);

                        $user = Sentinel::findById($post['id']);
                        $role = Sentinel::findRoleBySlug($post['role']);
                        $role->users()->attach($user);

                        \Session::flash('message', 'The user has been updated.');
                        return redirect('backoffice/users');
                } else{
                    \Session::flash('message', 'The password did not match.');
                    return Redirect::back();
                }
            } 
        }

         public function delete($id){
         	
         	$user = Sentinel::findById($id);
			$user->delete();

            \Session::flash('message', 'The user has been removed.');
        	return Redirect::back();
        }

        public function login(){
            if ($user = Sentinel::check()){
               return redirect('backoffice');
            }
            
        	return view('auth.login');
        }

        public function authenticate(Request $request){
        	$post = $request->all();

        	$v = \Validator::make($request->all(),
    		[
                'email'        => 'required',
                'password'       => 'required',
            ]);

        	if($v->fails()){
				return redirect()->back()->withErrors($v->errors());
			} else{
	        	$credentials = [
				    'email'    => $post['email'],
				    'password' => $post['password'],
				];

				Sentinel::authenticate($credentials);

				if ($user = Sentinel::check()){
                    $user = Sentinel::getUser();

                    $setting = DB::table('settings')->where('setting_name', 'access')->first();
                    if($setting->setting_value == 'true'){
                        if($user->inRole('editor') || $user->inRole('guest')){
                           // exit;
                            \Session::flash('message', 'The system is locked. For inquiries, Please contact an administrator.');
                            Session::forget('name');
                            Sentinel::logout();
                            return redirect('backoffice/login');
                        }
                    }

                    Session::put('id', $user->id);
                    Session::put('name', $user->first_name);

                    if ($user->inRole('superadmin')){
                       Session::put('role', 'superadmin');
                    }

                    if ($user->inRole('admin')){
                       Session::put('role', 'admin');
                    }

                    if ($user->inRole('editor')){
                      Session::put('role', 'editor');
                    }

                    if ($user->inRole('guest')){
                       Session::put('role', 'guest');
                    }

					return redirect('backoffice');
				} else{
		       		\Session::flash('message', 'Invalid username and password.');
	                return redirect('backoffice/login');
	            }
			} 
        }

        public function logout(){
        	Session::forget('name');

        	Sentinel::logout();

        	return redirect('backoffice/login');
        }

}