<?php

use Cartalyst\Sentinel\Native\Facades\Sentinel;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
	
	Route::match(['get', 'post'], '/wp-admin', function () {
		return redirect('backoffice/login');
	});

	/* 
	FRONTEND 
	*/
	// Home
	Route::get('/', 'Frontend\PrimaryController@home');
	Route::get('/albums', 'Frontend\PrimaryController@albums');
	Route::get('/albums/{id}/{title}', 'Frontend\PrimaryController@albumsEntry');
	Route::get('/albums-slideshow/{albumId}/{galleryId}', 'Frontend\PrimaryController@albumsSlideshowEntry');
	Route::get('/blogs', 'Frontend\PrimaryController@blogs');
	Route::get('/blogs/{id}/{title}', 'Frontend\PrimaryController@blogsEntry');
	Route::get('/about', 'Frontend\PrimaryController@about');
	Route::get('/contact', 'Frontend\PrimaryController@contact');

	Route::get('/page/{id}/{title}', 'Frontend\PrimaryController@page');

	Route::get('template/home', 'Frontend\PrimaryController@home');

	Route::post('mail', 'Frontend\PrimaryController@mail');
	
	Route::get('template/talents', 'Frontend\TalentsController@index');
	Route::get('api/getTalents/{username}', 'Frontend\TalentsController@getTalents');
	Route::post('api/shortlist', 'Frontend\TalentsController@shortlist');
	Route::post('api/remove-shortlist', 'Frontend\TalentsController@removeShortlist');
	Route::get('pdf/model/{id}', 'Frontend\TalentsController@genertePdf');
	Route::get('template/my-talents', 'Frontend\TalentsController@customerIndex');
	Route::get('api/getCustomerTalents/{username}', 'Frontend\TalentsController@customerShortlist');
	Route::get('template/model/{talent}', 'Frontend\ModelController@index');
	Route::post('api/checkShortlist', 'Frontend\ModelController@checkShortlist');

	// News
	Route::get('template/newslist', 'Frontend\NewsController@index');
	Route::get('api/getNews', 'Frontend\NewsController@getNews'); 
	Route::get('template/newsitem/{id}', 'Frontend\NewsController@indexNewsItem');

	// Pages
	Route::get('template/contact', 'Frontend\PagesController@contactIndex');
	Route::get('template/about', 'Frontend\PagesController@aboutIndex');
	Route::get('template/book', 'Frontend\PagesController@book');

	Route::post('api/mail', 'Frontend\PagesController@mail');
	Route::post('api/book', 'Frontend\PagesController@bookMail');

	Route::post('api/getUserInfo', 'Frontend\PagesController@getUserInfo');

	// Login
	Route::get('api/checkLogin', 'Frontend\LoginController@check');
	Route::get('template/login', 'Frontend\LoginController@index');
	Route::get('template/signup', 'Frontend\LoginController@signup'); 
	Route::post('api/signup', 'Frontend\LoginController@signupProcess'); 

	Route::post('api/login', 'Frontend\LoginController@loginProcess'); 

	/* 
	BACKEND 
	*/
	Route::get('backoffice', 'Backoffice\PrimaryController@index');
	Route::get('backoffice/dashboard', 'Backoffice\PrimaryController@dashboard');
	Route::get('api/getDashData', 'Backoffice\PrimaryController@getDashData');

	// Albums
	Route::get('template/albums', 'Backoffice\Models\modelListController@view');
	Route::resource('api/model-list','Backoffice\Models\modelListController');
	Route::post('upload/profile','Backoffice\Models\modelListController@upload');

	Route::post('upload/gallery','Backoffice\Models\modelListController@uploadGallery');
	Route::get('api/get-gallery/{id}','Backoffice\Models\modelListController@getGallery');	
	Route::get('api/set-featured-gallery/{userid}/{id}', 'Backoffice\Models\modelListController@setfeaturedSettings');
	Route::delete('api/remove-photo/{id}', 'Backoffice\Models\modelListController@removePhoto');

	Route::get('api/get-caption/{id}', 'Backoffice\Models\modelListController@getCaption');
	Route::post('api/save-caption', 'Backoffice\Models\modelListController@saveCaption');

	Route::get('template/experience', 'Backoffice\Models\experienceController@view');
	Route::resource('api/experience','Backoffice\Models\experienceController');

	Route::get('template/categories', 'Backoffice\Models\categoriesController@view');
	Route::resource('api/categories','Backoffice\Models\categoriesController'); 

	// Pages
	Route::get('template/pages', 'Backoffice\Pages\pagesController@view');
	Route::resource('api/pages','Backoffice\Pages\pagesController'); 

	// Blog
	Route::get('template/blog', 'Backoffice\Blog\blogController@view');
	Route::resource('api/blog','Backoffice\Blog\blogController'); 
	Route::post('upload/blog','Backoffice\Blog\blogController@upload');

	// MEDIA
	Route::get('template/media', 'Backoffice\Media\MediaController@view');
	Route::resource('api/media','Backoffice\Media\MediaController'); 
	Route::post('upload/media','Backoffice\Media\MediaController@upload');

	// Settings
	Route::get('template/users', 'Backoffice\Settings\UsersController@view');
	Route::resource('api/users','Backoffice\Settings\UsersController');

	Route::get('template/settings', 'Backoffice\Settings\GeneralController@view');
	Route::get('api/get-general-settings', 'Backoffice\Settings\GeneralController@getGeneralSettings');
	Route::post('api/save-general-settings', 'Backoffice\Settings\GeneralController@saveGeneralSettings');

	Route::get('template/backend-settings', 'Backoffice\Settings\BackendController@view');
	Route::get('api/get-backend-settings', 'Backoffice\Settings\BackendController@getBackendSettings');
	Route::post('api/save-backend-settings', 'Backoffice\Settings\BackendController@saveBackendSettings');

	Route::get('template/frontend-settings', 'Backoffice\Settings\FrontendController@view');
	Route::get('api/get-frontend-settings', 'Backoffice\Settings\FrontendController@getFrontendSettings');
	Route::post('api/save-frontend-settings', 'Backoffice\Settings\FrontendController@saveFrontendSettings');

	Route::get('api/get-page-count/{setting}', 'Backoffice\Settings\BackendController@getPageCount');
	
	// Users
	Route::get('backoffice/users', 'Backoffice\UsersController@index');
	Route::get('backoffice/users/permission', 'Backoffice\UsersController@permissions');
	Route::get('backoffice/users/edit-permission/{id}', 'Backoffice\UsersController@editPermissions');
	Route::post('backoffice/users/update-permission', 'Backoffice\UsersController@updatePermission');
	Route::get('backoffice/users/register', 'Backoffice\UsersController@register');
	Route::post('backoffice/users/save', 'Backoffice\UsersController@save');

	//User Authentication
	Route::get('backoffice/register', 'Backoffice\UsersController@register');
	Route::get('backoffice/logout', 'Backoffice\UsersController@logout');

	Route::get('backoffice/users/delete/{id}', 'Backoffice\UsersController@delete');
	Route::get('backoffice/users/edit/{id}', 'Backoffice\UsersController@edit');
	Route::post('backoffice/users/update', 'Backoffice\UsersController@update');

	Route::get('backoffice/login', 'Backoffice\UsersController@login');

	Route::get('backoffice/login', 'Backoffice\UsersController@login');
	Route::post('backoffice/authenticate', 'Backoffice\UsersController@authenticate');

	Route::match(['get', 'post'], 'Backoffice/{id}', function () {
		return redirect('backoffice/login');
	});