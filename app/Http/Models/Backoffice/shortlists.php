<?php
namespace App\Http\Models\Backoffice;

use Illuminate\Database\Eloquent\Model;

class shortlists extends Model
{
    protected $table = 'shortlists';
    protected $primaryKey = 'shortlist_id';
}
