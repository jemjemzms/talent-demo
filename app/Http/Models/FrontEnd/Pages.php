<?php
//Author:Jed Diaz
//Date: Dec 16, 2015
//Description: This will call selected settings configurations

namespace App\Http\Models\FrontEnd {
use DB;
	class Pages {
		
		function get($pageName){ 
			$pages = DB::table('pages')
						->select('page_value')
						->where('page_name', $pageName)
						->get();

			return $pages[0]->page_value;	
		}
	}

}
?>