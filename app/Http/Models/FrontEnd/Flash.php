<?php
//Author:Jed Diaz
//Date: Dec 16, 2015
//Description: Casino games flash records

namespace App\Http\Models\FrontEnd {
use DB;
	class Flash {
		
		function getAll(){ 
			return DB::table('casino_games')
			        ->orderBy('order', 'asc')
					->get();
		}

		function getByCount( $count ){ 
			return DB::table('casino_games')
					->take($count)
					->get();
		}

		function getGames($provider){ 
			return DB::table('casino_games')
					->where('provider', $provider)
			        ->orderBy('order', 'asc')
					->get();
		}
	}

}
?>