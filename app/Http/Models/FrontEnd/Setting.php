<?php
//Author:Jed Diaz
//Date: Dec 16, 2015
//Description: This will call selected settings configurations

namespace App\Http\Models\FrontEnd {
use DB;
	class Setting {
		
		function get($setingName){ 
			$setting = DB::table('settings')
						->select('setting_value')
						->where('setting_name', $setingName)
						->get();

			return $setting[0]->setting_value;	
		}
	}

}
?>