<?php
//Author:Jed Diaz
//Date: Dec 16, 2015
//Description: Fetch banners

namespace App\Http\Models\FrontEnd {
use DB;
	class Banner {
		
		function get($id, $language){

			return DB::table('banners')
						->where('location_type', $id)
						->where('language', $language)->first();
		}
	}

}
?>