<?php 
namespace App\Http\Middleware;
 
use Closure;
use App\Http\Models\Backoffice\operators;
 
class SimpleAuthMiddleware
{
 
   /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request $request
    * @param  \Closure $next
    * @return mixed
    */
   public function handle($request, Closure $next)
   {
      $opName = $_SERVER['HTTP_X_USER'];
      $opPass = $_SERVER['HTTP_X_PASS'];

      if(!isset($opName) || !isset($opPass)){  
            return response('Please set custom header.', 401);
      }  
  
      $operators = operators::where('operatorName', '=', $opName)->where('operatorPassword', '=', $opPass)->first();

      if(! $operators ){
        return response('Your credentials are invalid.', 401);
      }

      return $next($request); 
   }
 
}
?>