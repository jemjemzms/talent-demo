app.controller('backendSettingsController', ['$scope','$location', '$http', function($scope, $location, $http) {

  $scope.url = $location.$$protocol+'://'+$location.$$host+'/';
  $scope.modelListDisplayCount = '';
  $scope.experienceDisplayCount = '';
  $scope.categoryDisplayCount = '';
  $scope.customersDisplayCount = '';
  $scope.pagesDisplayCount = '';
  $scope.newsDisplayCount = '';


  $scope.init = function(){
    $http.get($scope.url + 'api/get-backend-settings').
    success(function(data, status, headers, config) {
      $scope.modelListDisplayCount = parseInt(data.modellist_display_count);
      $scope.experienceDisplayCount = parseInt(data.experience_display_count);
      $scope.categoryDisplayCount = parseInt(data.categories_display_count);
      $scope.customersDisplayCount = parseInt(data.customers_display_count);
      $scope.pagesDisplayCount = parseInt(data.pages_display_count);
      $scope.newsDisplayCount = parseInt(data.news_display_count);
    });
  };

  $scope.submitForm = function(){

      if ($scope.mpgForm.$valid) {

        $scope.loading = true;
        $http.post($scope.url + 'api/save-backend-settings', {
          modellist_display_count: $scope.modelListDisplayCount,
          experience_display_count: $scope.experienceDisplayCount,
          categories_display_count: $scope.categoryDisplayCount,
          customers_display_count: $scope.customersDisplayCount,
          pages_display_count: $scope.pagesDisplayCount,
          news_display_count: $scope.newsDisplayCount,
        }).success(function(data, status, headers, config) {
          $scope.loading = false;
          alertify.success('The settings has been saved.');
        });
      }

  }

  $scope.init();

}]);