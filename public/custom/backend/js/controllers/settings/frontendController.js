app.controller('frontendSettingsController', ['$scope','$location', '$http', function($scope, $location, $http) {

  $scope.url = $location.$$protocol+'://'+$location.$$host+'/';
  $scope.talentDisplayCount = '';
  $scope.address = '';
  $scope.globePhone = '';
  $scope.phone = '';
  $scope.email = '';
  $scope.musicApp = '';
  $scope.fbApp = '';
  $scope.googleAnalytics = '';

  $scope.slide_box_height = '';
  $scope.slide_box_width = '';
  $scope.slide_box_line_height = '';
  $scope.menu_font_size = '';
  $scope.menu_font_weight = '';
  $scope.menu_text_transform = '';
  $scope.slide_font_size = '';
  $scope.slide_line_height = '';
  $scope.slide_text_shadow = '';
  $scope.slide_description_font_size = '';
  $scope.slide_description_margin_top = '';
  $scope.slide_description_text_shadow = '';

  $scope.init = function(){
    $http.get($scope.url + 'api/get-frontend-settings').
    success(function(data, status, headers, config) {
      $scope.talentDisplayCount = parseInt(data.talent_display_count);
      $scope.address = data.address;
      $scope.globePhone = data.globe_phone;
      $scope.phone = data.phone;
      $scope.email = data.email;
      $scope.musicApp = data.music_app;
      $scope.fbApp = data.fb_app;
      $scope.googleAnalytics = data.google_analytics_api;

      $scope.slide_box_height = data.slide_box_height;
      $scope.slide_box_width = data.slide_box_width; 
      $scope.slide_box_line_height = data.slide_box_line_height; 
      $scope.menu_font_size = data.menu_font_size;
      $scope.menu_font_weight = data.menu_font_weight;
      $scope.menu_text_transform = data.menu_text_transform;
      $scope.slide_font_size = data.slide_font_size;
      $scope.slide_line_height = data.slide_line_height;
      $scope.slide_text_shadow = data.slide_text_shadow;
      $scope.slide_description_font_size = data.slide_description_font_size;
      $scope.slide_description_margin_top = data.slide_description_margin_top;
      $scope.slide_description_text_shadow = data.slide_description_text_shadow;
    });
  };

  $scope.submitForm = function(){

      if ($scope.mpgForm.$valid) {

        $scope.loading = true;
        $http.post($scope.url + 'api/save-frontend-settings', {
          talent_display_count: $scope.talentDisplayCount,
          address: $scope.address,
          globe_phone: $scope.globePhone,
          phone: $scope.phone,
          email: $scope.email,
          music_app: $scope.musicApp,
          fb_app: $scope.fbApp,
          google_analytics_api: $scope.googleAnalytics,

          slide_box_height: $scope.slide_box_height,
          slide_box_width: $scope.slide_box_width, 
          slide_box_line_height: $scope.slide_box_line_height,
          menu_font_size: $scope.menu_font_size,
          menu_font_weight: $scope.menu_font_weight,
          menu_text_transform: $scope.menu_text_transform,
          slide_font_size: $scope.slide_font_size,
          slide_line_height: $scope.slide_line_height,
          slide_text_shadow: $scope.slide_text_shadow,
          slide_description_font_size: $scope.slide_description_font_size,
          slide_description_margin_top: $scope.slide_description_margin_top,
          slide_description_text_shadow: $scope.slide_description_text_shadow,

        }).success(function(data, status, headers, config) {
          $scope.loading = false;
          alertify.success('The settings has been saved.');
        });
      }

  }

  $scope.init();

}]);