var app = angular.module('contentApp', ['ngRoute', 'angularUtils.directives.dirPagination', 'checklist-model', 'ckeditor', 'blockUI'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%'); 
  $interpolateProvider.endSymbol('%>');
});
 
app.config(function($routeProvider){
  $routeProvider.
    when('/dashboard', {
      templateUrl: config.url + 'backoffice/dashboard',
      controller: 'dashboardController'
    }).
    when('/albums',{
      templateUrl:  config.url + 'template/albums',
      controller: 'modellistController'
    }). 
    when('/experience',{
      templateUrl:  config.url + 'template/experience',
      controller: 'experienceController'
    }).
    when('/categories',{
      templateUrl:  config.url + 'template/categories',
      controller: 'categoryController'
    }). 
    when('/pages',{
      templateUrl:  config.url + 'template/pages',
      controller: 'pagesController'
    }). 
    when('/blog',{
      templateUrl:  config.url + 'template/blog',
      controller: 'blogController'
    }). 
    when('/users',{
      templateUrl:  config.url + 'template/users',
      controller: 'usersController'
    }).
    when('/media',{
      templateUrl:  config.url + 'template/media',
      controller: 'mediaController'
    }).
    when('/settings',{
      templateUrl:  config.url + 'template/settings',
      controller: 'generalSettingsController'
    }).
    when('/frontend-settings',{
      templateUrl:  config.url + 'template/frontend-settings',
      controller: 'frontendSettingsController'
    }).
    when('/backend-settings',{
      templateUrl:  config.url + 'template/backend-settings',
      controller: 'backendSettingsController'
    }).
    otherwise({
      redirectTo: '/dashboard'
    });
});

app.controller('dashboardController', function($scope, $http) {
 $scope.loading = false;
 $scope.totals = [];

 $scope.init = function(){

    // Get Platforms
    $http.get(config.url + 'api/getDashData').
    success(function(data, status, headers, config) {
        $scope.totals = data;
    });
  };

 $scope.init();
});


app.controller('menuController', ['$scope','$location', function($scope, $location) {
  
  $scope.getClass = function (path) {
    return ($location.path().substr(0, path.length) === path) ? 'active' : '';
  };

  $scope.getParentClass = function(type) {

    if(type == 'models'){
     switch( $location.path() ) {
      case '/albums':
      case '/experience':
      case '/categories':
        return 'active';
      break;
     } 
    }

    if(type == 'set'){
     switch( $location.path() ) {
      case '/settings':
      case '/backend-settings':
      case '/frontend-settings':
        return 'active';
      break;
     } 
    }

    return '';
  };

}]);

function OtherController($scope) {
  $scope.pageChangeHandler = function(num) {
    //console.log('going to page ' + num);
  };
}

app.controller('OtherController', OtherController);

app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

$( ".triggerClose" ).on( "click", function() {
  //Close Event
});
