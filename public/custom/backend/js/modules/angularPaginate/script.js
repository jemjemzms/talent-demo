// Code goes here

var myApp = angular.module('myApp', ['angularUtils.directives.dirPagination'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});
/*
function MyController($scope) {

  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.meals = [];

  $http.get('/api/getPromotions').
    success(function(data, status, headers, config) {
      $scope.meals = data; 
  });

  $scope.pageChangeHandler = function(num) {
      console.log('meals page changed to ' + num);
  };
}*/

myApp.controller('MyController', function($scope, $http) {
 
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.meals = [];

  $scope.init = function() {
    $http.get('/api/getPromotions').
    success(function(data, status, headers, config) {
      $scope.meals = data; 
    });
  }
 
  $scope.init();
 
});