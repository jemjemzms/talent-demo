var app = angular.module('contentApp', ['ngRoute', 'angularUtils.directives.dirPagination', 'ipCookie', 'ui.bootstrap', 'blockUI'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%'); 
  $interpolateProvider.endSymbol('%>');
});

app.config(function($routeProvider){
  $routeProvider.
    when('/home', {
      templateUrl: config.url + 'template/home',
      controller: 'homeController'
    }).
    when('/talents/:message',{
      templateUrl:  config.url + 'template/talents',
      controller: 'talentController'
    }).
    when('/model/:talent', {
       templateUrl: function(params) {
        return config.url + 'template/model/' + params.talent ;
       },
       controller: 'modelController'
    }).
    when('/my-talents',{
      templateUrl:  config.url + 'template/my-talents',
      controller: 'mytalentController'
    }).
    when('/news',{
      templateUrl:  config.url + 'template/newslist',
      controller: 'newsController'
    }).
    when('/news/:id/:title', {
       templateUrl: function(params) {
        return config.url + 'template/newsitem/' + params.id ;
       },
       controller: 'newsitemController'
    }).
    when('/login',{
      templateUrl:  config.url + 'template/login',
      controller: 'loginController'
    }).
    when('/signup',{
      templateUrl:  config.url + 'template/signup',
      controller: 'signupController'
    }).
    when('/contact-us',{
      templateUrl:  config.url + 'template/contact',
      controller: 'contactController'
    }).
    when('/about-us',{
      templateUrl:  config.url + 'template/about',
      controller: 'pagesController'
    }).
    otherwise({
      redirectTo: '/home'
    });
});

app.controller('homeController', ['$scope','$location', 'ipCookie', function($scope, $location, ipCookie) {
    
    if( ipCookie('username') === undefined){
      //console.log('logged out');
    } else{
      //console.log('logged in');
    }

}]);


app.controller('pagesController', ['$scope','$location', 'ipCookie', function($scope, $location, ipCookie) {
    
}]);



function OtherController($scope) {
  $scope.pageChangeHandler = function(num) {
    //console.log('going to page ' + num);
  };
}

app.controller('OtherController', OtherController);

app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});