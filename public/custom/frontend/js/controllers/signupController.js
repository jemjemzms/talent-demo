app.controller('signupController', ['$scope', '$http', '$routeParams', '$location', 'ipCookie', function($scope, $http, $routeParams, $location, ipCookie){

 $scope.loading = false;
 $scope.username = "";
 $scope.firstname = "";  
 $scope.lastname = "";  
 $scope.nickname = "";  
 $scope.email = "";
 $scope.website = "";
 $scope.bio = "";
 $scope.password = "";
 $scope.repeatpassword = "";
 $scope.minlength = 5;

 $scope.init = function(){
    if(ipCookie('username') === undefined){
    } else{
      $location.path("/home");
      alertify.warning('Cannot access te page. You\'re already logged in.');
    }
 };

 $scope.submitForm = function(){
      if ($scope.signupForm.$valid) {

        var error = false;
        if($scope.password !== $scope.repeatpassword){
           alertify.warning('Your password did not match.');
           error = true;
        }

        if($scope.validateEmail($scope.email) == false){
           alertify.warning('Your email is not valid.');
           error = true;
        }
      
        if( error == false ){
          $scope.loading = true;
          $http.post(config.url + 'api/signup', {
            username: $scope.username,
            firstname: $scope.firstname,
            lastname: $scope.lastname,
            nickname: $scope.nickname,
            email: $scope.email,
            website: $scope.website,
            bio: $scope.bio,
            password: $scope.password,
          }).success(function(data, status, headers, config) {

            if(data.status === 'true'){
              alertify.success(data.message);
              $location.path("/login");
            } else {
              alertify.warning(data.message);
            }

            $scope.loading = false;
          });
          
        }

      }
  };

  $scope.validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    return re.test(email);
  };

 $scope.init();

} ] );