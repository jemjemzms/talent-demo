app.controller('mytalentController', ['$scope', '$http', '$routeParams', '$location', 'ipCookie', '$uibModal', function($scope, $http, $routeParams, $location, ipCookie, $uibModal){

 $scope.loading = false;
 $scope.category = $routeParams.message;
 $scope.talents = [];
 $scope.order = '-id';
 $scope.pageSize = 6;
 $scope.currentPage = 1;
 $scope.selectedGender = [];
 $scope.selectedExperience = [];
 $scope.selectedCategories = [];
 $scope.selectedCountries = [];
 $scope.genderDropdown = true;
 $scope.categoriesDropdown = true;

 $scope.getDefaults = function( defaultName ){
      var categories = {
      'gender': function() {
        return ['Male', 'Female'];
      },
      'experience': function() {
        return ['New Face', 'Professional Model'];
      },
      'categories': function() {
        return ['Athlete', 'Dancer', 'Flyin', 'In Town'];
      },
      'countries' : function(){
        return ['Philippines', 'Spain', 'U.S.A', 'Lebanon', 'Australia', 'Singapore', 'Shanghai, China', 'Germany'];
      }
    };

    if (categories[defaultName]){
      return categories[defaultName]();
    }
  };

 $scope.init = function(){

    $scope.selectedGender = $scope.getDefaults('gender');
    $scope.selectedExperience = $scope.getDefaults('experience');
    $scope.selectedCategories = $scope.getDefaults('categories');
    $scope.selectedCountries = $scope.getDefaults('countries');

    $scope.identifyDefaults( $routeParams.message );

    // Get Talents
    $http.get(config.url + 'api/getCustomerTalents/' + ipCookie('username')).
    success(function(data, status, headers, config) {
        $scope.talents = data;
    });
  };

  $scope.identifyDefaults = function(message){

  var categories = {
    'all': function() {
    },
    'female': function() {
       $scope.selectedGender = ['Female'];
       $scope.gender = 'Female';
       $scope.genderDropdown = false;
       $scope.categoriesDropdown = true;
    },
    'male': function() {
       $scope.selectedGender = ['Male'];
       $scope.gender = 'Male';
       $scope.genderDropdown = false;
       $scope.categoriesDropdown = true;
    },
    'flyin' : function(){
      $scope.selectedCategories = ['Athlete', 'Dancer', 'Flyin'];
      $scope.categories = 'Flyin';
      $scope.genderDropdown = true;
      $scope.categoriesDropdown = false;
    },
    'in-town': function() {
      $scope.selectedCategories = ['Athlete', 'Dancer', 'In Town'];
      $scope.categories = 'In Town';
      $scope.genderDropdown = true;
      $scope.categoriesDropdown = false;
    }
  };

  if (categories[message]){
    categories[message]();
  }

 };

 $scope.printToView = function(id){
   window.open(config.url + 'pdf/model/'+id);
 };
 
 $scope.process = function(action, id){
    var process = {
    'all': function() {
    },
    'removeFromShortlist': function(id) { 

        if(ipCookie('username') === undefined){
          $location.path("/login");
        } else{
    
          $http.post(config.url + 'api/remove-shortlist', {
            username: ipCookie('username'),
            id: id,
          }).success(function(data, status, headers, config) {

             if(data.status === 'true'){
              $scope.init();
              alertify.success(data.message);
             } else{
              alertify.warning(data.message);
             }

          }); 
        }
    },
  };

  if (process[action]){
    process[action](id);
  }
 };

 // Book Model
 $scope.item = "";
 $scope.itemName = "";
 $scope.userName = "";
 $scope.bookModel = function (size, talent) {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: config.url + 'template/book',
      controller: 'BookController',
      size: size,
      resolve: {
        item: function () {
          return talent.id;
        },
        itemName: function () {
          return talent.model_name;
        },
        userName: function () {
          if( ipCookie('username') === undefined){
          } else{
            return ipCookie('username');
          }
        }
      }
    });

  };
 // EOF Book Model
 
 $scope.$watch('gender',function(val,old){
    $scope.selectedGender = $scope.getDefaults('gender');
    var gender = $scope.selectedGender;
    if (val) {
      

      gender.length = 0;
      $scope.selectedGender = [val];
    } 
  }); 

  $scope.$watch('experience',function(val,old){
    $scope.selectedExperience = $scope.getDefaults('experience');
    var exp = $scope.selectedExperience;
    if (val) {
      

      exp.length = 0;
      $scope.selectedExperience = [val];
    } 
  }); 

  $scope.$watch('categories',function(val,old){
    $scope.selectedCategories = $scope.getDefaults('categories');
    var exp = $scope.selectedCategories;
    if (val) {
      

      exp.length = 0;
      $scope.selectedCategories = [val];
    } 
  }); 

  $scope.$watch('countries',function(val,old){
    $scope.selectedCountries = $scope.getDefaults('countries');
    var exp = $scope.selectedCountries;
    if (val) {
      

      exp.length = 0;
      $scope.selectedCountries = [val];
    } 
  }); 

  $scope.init();

} ] );

app.filter('filterGender', function() {
    return function(talents, selectedGender) {
        return talents.filter(function(talent) {

            for (var i in talent.categories) {
                if (selectedGender.indexOf(talent.categories[i]) != -1) {
                    return true;
                }
            }

            return false;

        });
    };
});

app.filter('filterExperience', function() {
    return function(talents, selectedExperience) {
        return talents.filter(function(talent) {

            var exp = talent.experience;
            for (var i in selectedExperience) {
                if (exp.indexOf(selectedExperience[i]) != -1) {
                    return true;
                }
            }

            return false;

        });
    };
});

app.filter('filterCategories', function() {
    return function(talents, selectedCategories) {
        return talents.filter(function(talent) {

            for (var i in talent.categories) {
                if (selectedCategories.indexOf(talent.categories[i]) != -1) {
                    return true;
                }
            }

            return false;

        });
    };
});

app.filter('filterCountries', function() {
    return function(talents, selectedCountries) {
        return talents.filter(function(talent) {

            var country = talent.country;
            for (var i in selectedCountries) {
                if (country.indexOf(selectedCountries[i]) != -1) {
                    return true;
                }
            }

            return false;

        });
    };
});