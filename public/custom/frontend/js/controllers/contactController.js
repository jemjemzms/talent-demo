app.controller('contactController', ['$scope', '$http', '$routeParams', '$location', 'ipCookie', '$rootScope', function($scope, $http, $routeParams, $location, ipCookie, $rootScope){

 $scope.loading = false;
 $scope.name = "";
 $scope.email = "";
 $scope.message = "";
 $scope.prove = "";
 $scope.num1 = "";
 $scope.num2 = "";
 $scope.messageSent = false;
 $scope.messageInfo = "";

 $scope.init = function(){
  $scope.num1 = Math.floor(Math.random() * 10) + 1;
  $scope.num2 = Math.floor(Math.random() * 10) + 1;

 };

 $scope.submitForm = function(){
      if ($scope.contactForm.$valid) {

        var error = false;

        var result = $scope.num1 + $scope.num2;
        if(parseInt($scope.prove) !== result){
          alertify.warning('Your answer is incorrect. Please try again.');
          error = true;
        }

        if($scope.validateEmail($scope.email) == false){
           alertify.warning('Your email is not valid.');
           error = true;
        }
      
        if( error == false ){
          
          $scope.loading = true;
          $http.post(config.url + 'api/mail', {
            name: $scope.name,
            email: $scope.email,
            message: $scope.message,
          }).success(function(data, status, headers, config) {

            if(data.status == 'true'){
              $scope.messageInfo = data.message;
              $scope.messageSent = true;
            } else{
              alertify.warning(data.message);
            }

            $scope.loading = false;
          }); 

        }

      }
  };

  $scope.validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    return re.test(email);
  };

  $scope.init();

}]);