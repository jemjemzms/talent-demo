app.controller('newsController', ['$scope', '$http', '$routeParams', '$location', 'ipCookie', function($scope, $http, $routeParams, $location, ipCookie){

 $scope.loading = false;
 $scope.model = [];
 $scope.order = '-news_id';
 $scope.pageSize = 6;
 $scope.currentPage = 1;

 $scope.init = function(){

    // Get Talents
    $http.get(config.url + 'api/getNews').
    success(function(data, status, headers, config) {
        $scope.model = data;
    });
  };

  $scope.init();

} ] );