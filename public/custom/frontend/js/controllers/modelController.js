app.controller('modelController', ['$http', '$scope','$location', 'ipCookie', '$routeParams', '$uibModal', function($http, $scope, $location, ipCookie, $routeParams, $uibModal) {

  $scope.removeShortListButton = false;

  $scope.init = function(){

    if(ipCookie('username') === undefined){
    } else{
     $http.post(config.url + 'api/checkShortlist', {
            username: ipCookie('username'),
            slug: $routeParams.talent,
          }).success(function(data, status, headers, config) {
          	  if(data == 'true'){
              	$scope.removeShortListButton = true;
          	  }
          }); 	
    }

  };

  $scope.init();

  $scope.process = function(action, id){

    var process = {
    'all': function() {
    },
    'addToShortlist': function(id) { 

        if(ipCookie('username') === undefined){
          $location.path("/login");
        } else{

          $http.post(config.url + 'api/shortlist', {
            username: ipCookie('username'),
            id: id,
          }).success(function(data, status, headers, config) {

             if(data.status === 'true'){
              alertify.success(data.message);
              $scope.removeShortListButton = true;
             } else{
              alertify.warning(data.message);
             }

          }); 
        } 
    },
    'removeFromShortlist': function(id) { 

        if(ipCookie('username') === undefined){
          $location.path("/login");
        } else{
    
          $http.post(config.url + 'api/remove-shortlist', {
            username: ipCookie('username'),
            id: id,
          }).success(function(data, status, headers, config) {

             if(data.status === 'true'){
              alertify.success(data.message);
              $scope.removeShortListButton = false;
             } else{
              alertify.warning(data.message);
             }

          }); 
        }
    },
  };

  if (process[action]){
    process[action](id);
  }
 };

  $scope.openPhoto = function (size, path) {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'viewModel.html',
      controller: 'ViewModelController',
      size: size,
      resolve: {
        path: function () {
          return path;
        }
      }
    });
 };

 // Book Model
 $scope.item = "";
 $scope.itemName = "";
 $scope.userName = "";
 $scope.bookModel = function (size, talentName, talentId) {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: config.url + 'template/book',
      controller: 'BookController',
      size: size,
      resolve: {
        item: function () {
          return talentId;
        },
        itemName: function () {
          return talentName;
        },
        userName: function () {
          if( ipCookie('username') === undefined){
          } else{
            return ipCookie('username');
          }
        }
      }
    });

  };
 // EOF Book Model

 $scope.printToView = function(id){
   window.open(config.url + 'pdf/model/'+id);
 };

}]);

app.controller('ViewModelController', function ($scope, $uibModalInstance, path) {
  $scope.path = path;
  $scope.ok = function () {
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});