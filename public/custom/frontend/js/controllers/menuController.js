app.controller('menuController', ['$scope','$location', 'ipCookie', '$rootScope', function($scope, $location, ipCookie, $rootScope) {
  
  $scope.menuControlOut = true;
  $scope.menuUsername = '';

  $scope.init = function(){
     if( ipCookie('username') === undefined){
      $scope.menuControlLoggedOut = true;
     } else{
      $scope.menuUsername = 'You are logged in as ' + ipCookie('username')+'!';
      $scope.menuControlLoggedOut = false;
     }
  };

  $scope.getClass = function (path) {
    
    if(path == '/news'){
    var str = $location.path();
    str = str.search('news');
      if(str === 1){
        return 'current-menu-item current_page_item';
      }
    }
    
    return ($location.path() === path) ? 'current-menu-item current_page_item' : '';
  };

  $scope.getParentClass = function(type) {

    if(type == 'talents'){
    var str = $location.path();
    str = str.search('model');
      if(str === 1){
        return 'current-menu-item current_page_item';
      }
    }

    if(type == 'talents'){
     switch( $location.path() ) {
      case '/talents/all':
      case '/talents/female':
      case '/talents/male':
      case '/talents/flyin':
      case '/talents/in-town':
        return 'current-menu-item current_page_item';
      break;
     } 
    }

    /*
    if(type == 'set'){
     switch( $location.path() ) {
      case '/users':
      case '/settings':
      case '/mpg-settings':
        return 'active';
      break;
     } 
    } */

    return '';
  };

  $scope.logout = function() {
    ipCookie.remove('username');
    $location.path("/home");
    alertify.success('You\'ve been successfully logged out.');
    $scope.menuControlLoggedOut = true;
    $scope.menuUsername = '';
  };

  $rootScope.$on("logged-in", function(){
    $scope.menuControlLoggedOut = false;
    $scope.menuUsername = 'You are logged in as ' + ipCookie('username')+'!';
  });


  $scope.init();
}]);