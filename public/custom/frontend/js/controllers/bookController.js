app.controller('BookController', function ($http, $scope, $uibModalInstance, item, itemName, userName) {

  $scope.title = itemName;
  $scope.subject = 'Book Model #'+item;
  $scope.name = '';
  $scope.email = '';
  
  $scope.init = function(){
    if(userName === undefined){
    } else{
      $http.post(config.url + 'api/getUserInfo', {
        username: userName,
      }).success(function(data, status, headers, config) {
        $scope.name = data.name;
        $scope.email = data.email;
      });   
    }
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

   $scope.submitForm = function(){
      if ($scope.bookForm.$valid) {

          $scope.loading = true;
          $http.post(config.url + 'api/book', {
            subject: $scope.subject,
            name: $scope.name,
            email: $scope.email,
            message: $scope.message,
          }).success(function(data, status, headers, config) {

            if(data.status == 'true'){
              alertify.success(data.message);
              $uibModalInstance.close();
            } else{
              alertify.warning(data.message);
            }

            $scope.loading = false;
          }); 
      }
  };

  $scope.init();

});