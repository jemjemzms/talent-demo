app.controller('loginController', ['$scope', '$http', '$routeParams', '$location', 'ipCookie', '$rootScope', function($scope, $http, $routeParams, $location, ipCookie, $rootScope){

 $scope.loading = false;
 $scope.username = "";
 $scope.password = "";
 $scope.expires = 7;
 $scope.expirationUnit = 'days';

 $scope.init = function(){

    if(ipCookie('username') === undefined){
    } else{
      $location.path("/home");
      alertify.warning('Cannot access te page. You\'re already logged in.');
    }

 };

 $scope.submitForm = function(){
      if ($scope.loginForm.$valid) {

        $scope.loading = true;
        $http.post(config.url + 'api/login', {
          username: $scope.username,
          password: $scope.password,
        }).success(function(data, status, headers, config) {

          if(data.status == 'true'){

            ipCookie('username', $scope.username, { expires: $scope.expires, expirationUnit: $scope.expirationUnit });
            $location.path("/my-talents");
            
            $rootScope.$emit("logged-in", {});

            alertify.success(data.message);
          } else{
            alertify.warning(data.message);
          }

          $scope.loading = false;
        }); 

      }
  };

 $scope.init();

} ] );