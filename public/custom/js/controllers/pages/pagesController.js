app.controller('pagesController', function($scope, $http) {

  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.loading = false;
  $scope.model = [];
  $scope.editingData = {};
  $scope.textid = '';
  $scope.texttitle = '';
  $scope.textdescription = '';

  $scope.texteditid = '';
  $scope.textedittitle = '';
  $scope.texteditdescription = '';  

  $scope.order = '-title';
  $scope.content = true;

  // CKeditor options.
  $scope.options = {
    language: 'en',
    allowedContent: true,
    entities: false
  };

  $scope.onReady = function () {
    //CKEditor
  };
  // EOF CKeditor

  $scope.init = function() {
    $scope.loading = true;
    $scope.content = true;
    
    $http.get(config.url + 'api/pages').
    success(function(data, status, headers, config) {

      $scope.model = data;
      for (var i = 0, length = data.length; i < length; i++) {
        $scope.editingData[data[i].page_id] = false;
      }

      $scope.content = false;
      $scope.loading = false;
    });

    // Get Page Count
    $http.get(config.url + 'api/get-page-count/' + 'pages_display_count').
    success(function(data, status, headers, config) {
      $scope.pageSize = parseInt(data);
    });
  }; 

  /*
  $scope.modify = function(row){
    $scope.editingData[row.page_id] = true;
  }; 

  $scope.update = function(row){
      $scope.editingData[row.page_id] = false;

      $scope.loading = true;
 
      $http.put(config.url + 'api/pages/' + row.page_id, {
        title: row.title,
        description: row.description,
      }).success(function(data, status, headers, config) {
        $scope.loading = false;
        alertify.success('Record has been updated.');
      });
  };

  $scope.cancel = function(row){
      $scope.editingData[row.page_id] = false;
  };*/

  $scope.editPage = function(page){
    $scope.texteditid = page.page_id;
    $scope.textedittitle = page.title;
    $scope.texteditdescription = page.description;  
  }

  $scope.submitUpdateForm = function(){
     if ($scope.editPagesForm.$valid) {

        $scope.loading = true;
        $http.put(config.url + 'api/pages/' + $scope.texteditid, {
          title: $scope.textedittitle,
          description: $scope.texteditdescription,
        }).success(function(data, status, headers, config) {

          $( ".triggerEditClose" ).trigger( "click" );
          $scope.init();

          alertify.success('Page has been added.');
          $scope.loading = false;
        }); 
      }
  };

  $scope.remove = function(rowId){

     bootbox.dialog({
      message: "Are you sure that you want to delete this row?",
      title: "ARE YOU SURE?",
      buttons: {
        danger: {
          label: "Confirm",
          className: "btn-danger",
          callback: function() {

            $scope.delete( rowId );
            alertify.success('Record has been deleted.');
         }
       },
       main: {
        label: "Cancel",
        className: "btn-primary",
        callback: function() {}
      }
    }
   });
  };

  $scope.getTotal = function(){
    if($scope.model.length == 0){
      $('#recordTable').hide(); 
    } else{ 
      $('#recordTable').show(); 
    }

    return $scope.model.length;
  };

  $scope.delete = function( rowId ){
    $scope.loading = true;
    $http.delete(config.url + 'api/pages/' + rowId)
    .success(function() {

      $scope.init();

      $scope.loading = false;
    });
  };

  $scope.toggleAll = function() {
     var toggleStatus = $scope.isAllSelected;
     angular.forEach($scope.model, function(itm){ itm.selected = toggleStatus; });  
  };
  
  $scope.optionToggled = function(){
    $scope.isAllSelected = $scope.model.every(function(itm){ return itm.selected; })
  };

  $scope.deleteAllSelected = function(){
    bootbox.dialog({
        message: "Are you sure that you want to delete the selected records?",
        title: "ARE YOU SURE?",
        buttons: {
          danger: {
            label: "Confirm",
            className: "btn-danger",
            callback: function() {
              var obj = $scope.model;
              var selected = [];
              for(x in obj){
                if (obj.hasOwnProperty(x)) {
                  if(obj[x].selected == true ){
                    $scope.delete( obj[x].page_id );
                    selected.push(obj[x].page_id);
                  }
                }
              }

              if(selected.length > 0){
                alertify.success('Selected records has been deleted.');
              } else{
                alertify.warning('Warning: No record was selected.');
              }
           }
         },
         main: {
          label: "Cancel",
          className: "btn-primary",
          callback: function() {}
        }
      }
     });
  };

  $scope.submitForm = function(){
      if ($scope.pagesForm.$valid) {

        $scope.loading = true;
        $http.post(config.url + 'api/pages', {
          title: $scope.texttitle,
          description: $scope.textdescription,
        }).success(function(data, status, headers, config) {

          $scope.textTitle = '';

          $( ".triggerClose" ).trigger( "click" );
          
          $scope.model.splice(0, 0, data);

          alertify.success('Record has been added.');
          $scope.loading = false;
        }); 
      }
  };

  $scope.sortRowByName = function(){
      if($scope.order == '-title'){
        $scope.order = 'title';
      } else{
        $scope.order = '-title';
      } 
  };

  $scope.init();
});