app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.service('fileUpload', ['$http','$location', '$rootScope', function ($http, $location, $rootScope) {
    this.uploadFileToUrl = function(file, uploadUrl, type, id){
        
        var self = this;
        var fd = new FormData();
        fd.append('file', file);
        fd.append('id', id);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function( data ){
            var path = 'profiles/small/';
            var imageUrl = $location.$$protocol + '://' + $location.$$host + '/img/' + path + data;
            switch(type){
              case 'new':
                $('#profile-image').attr('src', imageUrl);
                $('#profilePicName').val(data);
              break;
              case 'edit':
                $('#edit-profile-image').attr('src', imageUrl);
                $('#editProfilePicName').val(data);
              break;
              case 'gallery':
                path = 'uploads/small/';

                $rootScope.$emit("ResetGallery", {});
              break;
            }
        })
        .error(function(){
        });
    }
}]);

app.controller('modellistController', ['$scope', '$http', 'fileUpload', '$rootScope','$location', function($scope, $http, fileUpload, $rootScope, $location){

  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.loading = false;
  $scope.model = [];
  $scope.editingData = {};

  $scope.content = false;
  $scope.order = '-created_at.date';

  $scope.mpgApiURL = '';

  $scope.experiences = [];
  $scope.categories = [];

  $scope.expandImage = '';

  $scope.galleries = [];
  $scope.galleryId = '';
  $scope.gallerySelect = { index: 0 };
  
  //NEW FIELDS
  $scope.textModelName = '';
  $scope.textSlug = '';
  $scope.textCountry = '';
  $scope.textHeight = '';
  $scope.textBustChest = '';
  $scope.textWaist = '';
  $scope.textHips = '';
  $scope.textShoeSize = '';
  $scope.textDressSize = '';
  $scope.textNotes = '';
  $scope.textPrimaryPhoto = '';
  $scope.textExperiences = {experience: []};
  $scope.textCategories = {category: []};
  $scope.textfeatured = 'no';

  //EDIT FIELDS
  $scope.editModelId = '';
  $scope.editModelName = '';
  $scope.editSlug = '';
  $scope.editCountry = '';
  $scope.editHeight = '';
  $scope.editBustChest = '';
  $scope.editWaist = '';
  $scope.editHips = '';
  $scope.editShoeSize = '';
  $scope.editDressSize = '';
  $scope.editNotes = '';
  $scope.editPrimaryPhoto = '';
  $scope.editExperiences = {experience: []};
  $scope.editCategories = {category: [3]};
  $scope.editFeatured = '';

  $scope.init = function() {
    $scope.loading = true;
    $scope.content = true;

    // Get Experience
    $http.get(config.url + 'api/experience').
    success(function(data, status, headers, config) {
      $scope.experiences = data;
    });

    // Get Game Category
    $http.get(config.url + 'api/categories').
    success(function(data, status, headers, config) {
      $scope.categories = data;
    });

    // Get Models
    $http.get(config.url + 'api/model-list').
    success(function(data, status, headers, config) {
      $scope.model = data;
      for (var i = 0, length = data.length; i < length; i++) {
        $scope.editingData[data[i].id] = false;
      }

      $('#profile-image').attr('src', $('#profileDefaultImage').val());
      $('#edit-profile-image').attr('src', $('#profileDefaultImage').val());
      $scope.content = false;
      $scope.loading = false;
    });

    // Get Page Count
    $http.get(config.url + 'api/get-page-count/' + 'modellist_display_count').
    success(function(data, status, headers, config) {
      $scope.pageSize = parseInt(data);
    });
  }; 

  $scope.modify = function(row){
    
   $scope.editModelId = row.id;
   $scope.editModelName = row.model_name;
   $scope.editSlug = row.slug;
   $scope.editCountry = row.country;
   $scope.editHeight = row.height;
   $scope.editBustChest = row.bust_chest;
   $scope.editWaist = row.waist;
   $scope.editHips = row.hips;
   $scope.editShoeSize = row.shoe_size;
   $scope.editDressSize = row.dress_size;
   $scope.editNotes = row.notes;
   $scope.editPrimaryPhoto = row.primary_photo;
   $scope.editFeatured = row.featured_model;

   // Experience
   var arrayExperience = row.experiences.split(', ');
   for(var i=0; i<arrayExperience.length;i++) arrayExperience[i] = parseInt(arrayExperience[i], 10);
   $scope.editExperiences.experience = arrayExperience;

   // Category
   var arrayCategory = row.categories.split(', ');
   for(var i=0; i<arrayCategory.length;i++) arrayCategory[i] = parseInt(arrayCategory[i], 10);
   $scope.editCategories.category = arrayCategory;

   // Profile Picture
   $('#edit-profile-image').attr('src', config.url + 'img/profiles/small/' + row.primary_photo);
   $("#editProfilePicName").val(row.primary_photo);

  };

  $scope.submitEditForm = function(){
    if($scope.editForm.$valid) {
         var experience = $scope.editExperiences.experience;
         experience = experience.join(", ");
         var category = $scope.editCategories.category;
         category = category.join(", ");

          $scope.loading = true;
          $http.put(config.url + 'api/model-list/' + $scope.editModelId, {
            model_name: $scope.editModelName,
            slug: $scope.editSlug,
            country: $scope.editCountry,
            height: $scope.editHeight,
            bust_chest: $scope.editBustChest,
            waist: $scope.editWaist,
            hips: $scope.editHips,
            shoe_size: $scope.editShoeSize,
            dress_size: $scope.editDressSize,
            notes: $scope.editNotes,
            primary_photo: $("#editProfilePicName").val(),
            experiences: experience,
            categories: category,
            featured_model: $scope.editFeatured,
          }).success(function(data, status, headers, config) {
            $scope.editModelId = "";
            $scope.editModelName = "";
            $scope.editSlug = "";
            $scope.editCountry = "";
            $scope.editHeight = "";
            $scope.editBustChest = "";
            $scope.editWaist = "";
            $scope.editHips = "";
            $scope.editShoeSize = "";
            $scope.editDressSize = "";
            $scope.editNotes = "";
            $scope.editPrimaryPhoto = "";
            $scope.editExperiences.experience = [];
            $scope.editCategories.category = [];
            $scope.editFeatured = '';
            $('#edit-profile-image').attr('src', $('#profileDefaultImage').val());
            $('#file-editprofilePhoto').val('');
            $('#editProfilePicName').val('');
            $( ".triggerEditClose" ).trigger( "click" );
            $scope.init();
            $scope.loading = false;

            alertify.success('The model has been updated.');
          });
      } 
  };

  $scope.cancel = function(row){
      $scope.editingData[row.id] = false;
  };

  $scope.remove = function(rowId){

     bootbox.dialog({
      message: "Are you sure that you want to delete this row?",
      title: "ARE YOU SURE?",
      buttons: {
        danger: {
          label: "Confirm",
          className: "btn-danger",
          callback: function() {
              $scope.delete( rowId );

              alertify.success('Model has been deleted.');
          }
       },
       main: {
        label: "Cancel",
        className: "btn-primary",
        callback: function() {}
      }
    }
   });
  };

  $scope.delete = function( rowId ){
    $scope.loading = true;
    $http.delete(config.url + 'api/model-list/' + rowId)
    .success(function() {

      $scope.init();

      $scope.loading = false;
    });
  };

  $scope.getTotal = function(){
    if($scope.model.length == 0){
      $('#recordTable').hide(); 
      $('.total-records').hide(); 
      $('.no-records').show(); 
    } else{ 
      $('#recordTable').show(); 
      $('.total-records').show(); 
      $('.no-records').hide();
      return $scope.model.length;
    }
  };

  $scope.submitForm = function(){

      if($scope.newForm.$valid) {
         var experience = $scope.textExperiences.experience;
         experience = experience.join(", ");
         var category = $scope.textCategories.category;
         category = category.join(", ");

         $scope.loading = true;
          $http.post(config.url + 'api/model-list', {
            model_name: $scope.textModelName,
            slug: $scope.textSlug,
            country: $scope.textCountry,
            height: $scope.textHeight,
            bust_chest: $scope.textBustChest,
            waist: $scope.textWaist,
            hips: $scope.textHips,
            shoe_size: $scope.textShoeSize,
            dress_size: $scope.textDressSize,
            notes: $scope.textNotes,
            primary_photo: $("#profilePicName").val(),
            experiences: experience,
            categories: category,
            featured_model: $scope.textfeatured,
          }).success(function(data, status, headers, config) {

            $scope.textModelName = "";
            $scope.textSlug = "";
            $scope.textCountry = "";
            $scope.textHeight = "";
            $scope.textBustChest = "";
            $scope.textWaist = "";
            $scope.textHips = "";
            $scope.textShoeSize = "";
            $scope.textDressSize = "";
            $scope.textNotes = "";
            $scope.textPrimaryPhoto = "";
            $scope.textExperiences.experience = [];
            $scope.textCategories.category = [];
            $scope.textfeatured = "";
            $('#profile-image').attr('src', $('#profileDefaultImage').val());
            $('#file-profilePhoto').val('');
            $('#profilePicName').val('');
            $( ".triggerAddClose" ).trigger( "click" );
            $scope.model.splice(0, 0, data);
            $scope.loading = false;

            alertify.success('The model has been created.');
          });
      } 
  };

  $scope.toggleAll = function() {
     var toggleStatus = $scope.isAllSelected;
     angular.forEach($scope.model, function(itm){ itm.selected = toggleStatus; });  
  };
  
  $scope.optionToggled = function(){
    $scope.isAllSelected = $scope.model.every(function(itm){ return itm.selected; })
  };

  $scope.deleteAllSelected = function(){
    bootbox.dialog({
        message: "Are you sure that you want to delete the selected records?",
        title: "ARE YOU SURE?",
        buttons: {
          danger: {
            label: "Confirm",
            className: "btn-danger",
            callback: function() {
              var obj = $scope.model;
              var selected = [];
              for(x in obj){
                if (obj.hasOwnProperty(x)) {
                  if(obj[x].selected == true ){
                    $scope.delete( obj[x].id );
                    selected.push(obj[x].id);
                  }
                }
              }

              if(selected.length > 0){
                alertify.success('Selected records has been deleted.');
              } else{
                alertify.warning('Warning: No record was selected.');
              }
           }
         },
         main: {
          label: "Cancel",
          className: "btn-primary",
          callback: function() {}
        }
      }
     });
  };

  $scope.sortRowByID = function(){
      if($scope.order == '-id'){
        $scope.order = 'id';
      } else{
        $scope.order = '-id';
      } 
  };

  $scope.sortRowByName = function(){
      if($scope.order == '-model_name'){
        $scope.order = 'model_name';
      } else{
        $scope.order = '-model_name';
      } 
  };

  $scope.getExperience = function( id ){

    var x;
    var obj = $scope.experience;
    for(x in obj){
      if (obj.hasOwnProperty(x)) {
        if(obj[x].id == id){
          return obj[x].name;
        }
      }
    }

    return 'No Experience';
  };

  $scope.getCategory = function( id ){

    var x;
    var obj = $scope.categories;
    for(x in obj){
      if (obj.hasOwnProperty(x)) {
        if(obj[x].id == id){
          return obj[x].name;
        }
      }
    }

    return 'No Category';
  };

  $scope.getExperience = function( id ){

    var x;
    var obj = $scope.experiences;
    for(x in obj){
      if (obj.hasOwnProperty(x)) {
        if(obj[x].id == id){
          return obj[x].name;
        }
      }
    }

    return 'No Experience';
  };

  $scope.uploadFile = function(){
        var file = $scope.myFile;
        var uploadUrl = config.url + "upload/profile";
        fileUpload.uploadFileToUrl(file, uploadUrl, 'new', '');
  };

  $scope.uploadEditFile = function(){
        var file = $scope.myFile;
        var uploadUrl = config.url + "upload/profile";
        fileUpload.uploadFileToUrl(file, uploadUrl, 'edit', '');
  };

  $scope.uploadGalleryFile = function(){
        var file = $scope.myFile;
        var uploadUrl = config.url + "upload/gallery";
        fileUpload.uploadFileToUrl(file, uploadUrl, 'gallery', $scope.galleryId);
  };

  $rootScope.$on("ResetGallery", function(){
   $scope.loadGallery();
  });

  $scope.setGallery = function(row){
    $scope.galleryId = row.id;

    $scope.loadGallery();
  };

  $scope.selectFeatured = function(gallery){
    $('.gallery-label').html('');
    $('#gallery-' + gallery.id).html('Featured Image');

    // Get Gallery
    $http.get(config.url + 'api/set-featured-gallery/'+ gallery.user_id + '/' + gallery.id).
    success(function(data, status, headers, config) {
     // $scope.galleries = data;
       alertify.success('A Featured Photo has been selected.');
    });

  };

  $scope.loadGallery = function(){
    // Get Gallery
    $('#file-galleryPhoto').val("");
    $http.get(config.url + 'api/get-gallery/' + $scope.galleryId).
    success(function(data, status, headers, config) {
      $scope.galleries = data;

      for (var i = 0, length = data.length; i < length; i++) {
         if(data[i].featured == 'yes'){
            $scope.gallerySelect.index = data[i].id;
         }
      }

    });
  };

  $scope.postLabelFeatured = function( featured ){
    if(featured == 'yes'){
      return 'Featured Image';
    }
  };

  $scope.expandImage = '';
  $scope.selectExpandPhoto = function( row ){
    $scope.expandImage = $location.$$protocol + '://' + $location.$$host + '/img/profiles/small/' + row.primary_photo;
  };

  $scope.removePhoto = function(photoId){
    bootbox.dialog({
      message: "Are you sure that you want to delete this photo?",
      title: "ARE YOU SURE?",
      buttons: {
        danger: {
          label: "Confirm",
          className: "btn-danger",
          callback: function() {
              
              $http.delete(config.url + 'api/remove-photo/' + photoId)
              .success(function() {
                $scope.loadGallery();
              });

              alertify.success('Photo has been deleted.');
          }
       },
       main: {
        label: "Cancel",
        className: "btn-primary",
        callback: function() {}
      }
    }
   });
  };

  $scope.$watch('textModelName',function(val,old){
   
    if (val) {

      val = val.replace(/\s+/g, '-');    
      $scope.textSlug = val.toLowerCase();

    }

  }); 

  $scope.init();
} ] );
