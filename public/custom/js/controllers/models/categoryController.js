app.controller('categoryController', function($scope, $http) {

  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.loading = false;
  $scope.model = [];
  $scope.editingData = {};
  $scope.textName = '';
  $scope.textSlug = '';
  $scope.textDescription = '';
  $scope.order = '-name';
  $scope.content = true;

  $scope.init = function() {
    $scope.loading = true;
    $scope.content = true;
    
    $http.get(config.url + 'api/categories').
    success(function(data, status, headers, config) {

      $scope.model = data;
      for (var i = 0, length = data.length; i < length; i++) {
        $scope.editingData[data[i].id] = false;
      }

      $scope.content = false;
      $scope.loading = false;
    });

    // Get Page Count
    $http.get(config.url + 'api/get-page-count/' + 'categories_display_count').
    success(function(data, status, headers, config) {
      $scope.pageSize = parseInt(data);
    });
  }; 

  $scope.modify = function(row){
    $scope.editingData[row.id] = true;
  };

  $scope.update = function(row){
      $scope.editingData[row.id] = false;

      $scope.loading = true;
 
      $http.put(config.url + 'api/categories/' + row.id, {
        name: row.name,
        slug: row.slug,
        description: row.description
      }).success(function(data, status, headers, config) {
        $scope.loading = false;
        alertify.success('Record has been updated.');
      });
  };

  $scope.cancel = function(row){
      $scope.editingData[row.id] = false;
  };

  $scope.remove = function(rowId){

     bootbox.dialog({
      message: "Are you sure that you want to delete this row?",
      title: "ARE YOU SURE?",
      buttons: {
        danger: {
          label: "Confirm",
          className: "btn-danger",
          callback: function() {

            $scope.delete( rowId );
            alertify.success('Record has been deleted.');
         }
       },
       main: {
        label: "Cancel",
        className: "btn-primary",
        callback: function() {}
      }
    }
   });
  };

  $scope.getTotal = function(){
    if($scope.model.length == 0){
      $('#recordTable').hide(); 
      $('.total-records').hide(); 
      $('.no-records').show(); 
    } else{ 
      $('#recordTable').show(); 
      $('.total-records').show(); 
      $('.no-records').hide();
      return $scope.model.length;
    }
  };

  $scope.delete = function( rowId ){
    $scope.loading = true;
    $http.delete(config.url + 'api/categories/' + rowId)
    .success(function() {

      $scope.init();

      $scope.loading = false;
    });
  };

  $scope.toggleAll = function() {
     var toggleStatus = $scope.isAllSelected;
     angular.forEach($scope.model, function(itm){ itm.selected = toggleStatus; });  
  };
  
  $scope.optionToggled = function(){
    $scope.isAllSelected = $scope.model.every(function(itm){ return itm.selected; })
  };

  $scope.deleteAllSelected = function(){
    bootbox.dialog({
        message: "Are you sure that you want to delete the selected records?",
        title: "ARE YOU SURE?",
        buttons: {
          danger: {
            label: "Confirm",
            className: "btn-danger",
            callback: function() {
              var obj = $scope.model;
              var selected = [];
              for(x in obj){
                if (obj.hasOwnProperty(x)) {
                  if(obj[x].selected == true ){
                    $scope.delete( obj[x].id );
                    selected.push(obj[x].id);
                  }
                }
              }

              if(selected.length > 0){
                alertify.success('Selected records has been deleted.');
              } else{
                alertify.warning('Warning: No record was selected.');
              }
           }
         },
         main: {
          label: "Cancel",
          className: "btn-primary",
          callback: function() {}
        }
      }
     });
  };

  $scope.submitForm = function(){
      if ($scope.tableForm.$valid) {

        $scope.loading = true;
        $http.post(config.url + 'api/categories', {
          name: $scope.textName,
          slug: $scope.textSlug,
          description: $scope.textDescription
        }).success(function(data, status, headers, config) {

          $scope.textName = '';
          $scope.textSlug = '';
          $scope.textDescription = '';

          $( ".triggerClose" ).trigger( "click" );
          
          $scope.model.splice(0, 0, data);

          alertify.success('Record has been added.');
          $scope.loading = false;
        }); 
      }
  };

  $scope.sortRowByName = function(){
      if($scope.order == '-name'){
        $scope.order = 'name';
      } else{
        $scope.order = '-name';
      } 
  };

  $scope.sortRowBySlug = function(){
      if($scope.order == '-slug'){
        $scope.order = 'slug';
      } else{
        $scope.order = '-slug';
      } 
  };

  $scope.$watch('textName',function(val,old){
    $scope.textSlug = val; 

    if (val) {

      val = val.replace(/\s+/g, '-');    
      $scope.textSlug = val.toLowerCase();

    }

  });

  $scope.init();
});