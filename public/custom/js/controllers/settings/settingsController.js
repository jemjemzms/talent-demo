app.controller('generalSettingsController', ['$scope','$location', '$http', function($scope, $location, $http) {

  $scope.url = $location.$$protocol+'://'+$location.$$host+'/';
  $scope.general = [];
  $scope.generalAppName = '';
  $scope.generalTimezone = '';
  $scope.generalDateformat = '';
  $scope.generalTimeformat = '';

  $scope.generalDateformat = { index: 0 };
  $scope.generalTimeformat = { index: 0 };

  $scope.init = function(){
    $http.get($scope.url + 'api/get-general-settings').
    success(function(data, status, headers, config) {
      $scope.general = data;
      $scope.generalAppName = data.defaultappname;
      $scope.generalTimezone = data.defaulttimezone;
      $scope.generalDateformat.index = data.defaultdateformat;
      $scope.generalTimeformat.index = data.defaulttimeformat;
    });
  };

  $scope.submitForm = function(){

      if ($scope.generalForm.$valid) {

        $scope.loading = true;
        $http.post($scope.url + 'api/save-general-settings', {
          appName: $scope.generalAppName,
          timeZone: $scope.generalTimezone,
          dateFormat: $scope.generalDateformat.index,
          timeFormat: $scope.generalTimeformat.index,
        }).success(function(data, status, headers, config) {
          $scope.loading = false;
          alertify.success('The general settings has been saved.');
        });
      }

  }

  $scope.init();

}]);