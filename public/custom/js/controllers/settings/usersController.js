app.controller('usersController', function($scope, $http) {

  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.loading = false;
  $scope.model = [];
  $scope.editingData = {};
  $scope.textEmail = '';
  $scope.textFirst = '';
  $scope.textLast = '';
  $scope.textPass = '';
  $scope.textConfPass = '';

  $scope.selecteduser = [];

  $scope.order = '-id';
  $scope.content = false;

  $scope.init = function() {
    $scope.loading = true;
    $scope.content = true;

    $http.get(config.url + 'api/users').
    success(function(data, status, headers, config) {

      $scope.model = data;
      for (var i = 0, length = data.length; i < length; i++) {
        $scope.editingData[data[i].id] = false;
      }

      $scope.content = false;
      $scope.loading = false;
    });
  }; 

  $scope.modify = function(users){
    $scope.editingData[users.id] = true;
  };

  $scope.update = function(users){
      $scope.editingData[users.id] = false;

      $scope.loading = true;
 
      $http.put(config.url + 'api/users/' + users.id, {
        email: users.email,
        first_name: users.first_name,
        last_name: users.last_name,
      }).success(function(data, status, headers, config) {
        $scope.loading = false;
        alertify.success('Record has been updated.');
      });
  };

  $scope.cancel = function(users){
      $scope.editingData[users.id] = false;
  };

  $scope.remove = function(id){

     bootbox.dialog({
      message: "Are you sure that you want to delete this row?",
      title: "ARE YOU SURE?",
      buttons: {
        danger: {
          label: "Confirm",
          className: "btn-danger",
          callback: function() {
            $scope.delete(id);

            alertify.success('Record has been deleted.');
         }
       },
       main: {
        label: "Cancel",
        className: "btn-primary",
        callback: function() {}
      }
    }
   });
  };

  $scope.delete = function( rowId ){
    $scope.loading = true;
    $http.delete(config.url + 'api/users/' + rowId)
    .success(function() {

      $scope.init();

      $scope.loading = false;
    });
  };

  $scope.getTotal = function(){
    if($scope.model.length == 0){
      $('#recordTable').hide(); 
      $('.total-records').hide(); 
      $('.no-records').show(); 
    } else{ 
      $('#recordTable').show(); 
      $('.total-records').show(); 
      $('.no-records').hide();
      return $scope.model.length;
    }
  };
/*
  $scope.changePassword = function(user){
    console.log(user);
  }; */

  $scope.submitPassForm = function(){
      if ($scope.changePassForm.$valid) {

        if($scope.textPass == $scope.textConfPass){
            
            $scope.loading = true;
            $http.post(config.url + 'api/users', {
              id : $scope.selecteduser.id,
              password : $scope.textPass,
              type: 'changepass',
            }).success(function(data, status, headers, config) {

              $scope.textPass = '';
              $scope.textConfPass = '';

              $( ".triggerClose" ).trigger( "click" );
              alertify.success('Password has been changed.');
              $scope.loading = false;
            }); 

        } else{
            alertify.warning('The passwords did not match.');
        }
      }
  };

  $scope.submitForm = function(){
      if ($scope.userForm.$valid) {

        if($scope.textPass == $scope.textConfPass){
            
            $scope.loading = true;
            $http.post(config.url + 'api/users', {
              email: $scope.textEmail,
              first_name : $scope.textFirst,
              last_name : $scope.textLast,
              password : $scope.textPass,
              type: 'adduser',
            }).success(function(data, status, headers, config) {

              $scope.textEmail = '';
              $scope.textFirst = '';
              $scope.textLast = '';
              $scope.textPass = '';
              $scope.textConfPass = '';

              $scope.model.splice(0, 0, data);

              $( ".triggerClose" ).trigger( "click" );
              alertify.success('User has been added.');
              $scope.loading = false;
            }); 

        } else{
            alertify.warning('The passwords did not match.');
        }
      }
  };

  $scope.setUser = function( user ){

    $scope.selecteduser = user;

  };

  $scope.toggleAll = function() {
     var toggleStatus = $scope.isAllSelected;
     angular.forEach($scope.model, function(itm){ itm.selected = toggleStatus; });  
  };
  
  $scope.optionToggled = function(){
    $scope.isAllSelected = $scope.model.every(function(itm){ return itm.selected; })
  };

  $scope.deleteAllSelected = function(){
    bootbox.dialog({
        message: "Are you sure that you want to delete this user?",
        title: "ARE YOU SURE?",
        buttons: {
          danger: {
            label: "Confirm",
            className: "btn-danger",
            callback: function() {
              var obj = $scope.model;
              var selected = [];
              for(x in obj){
                if (obj.hasOwnProperty(x)) {
                  if(obj[x].selected == true ){
                    $scope.delete( obj[x].id );
                    selected.push(obj[x].id);
                  }
                }
              }

              if(selected.length > 0){
                alertify.success('Selected records has been deleted.');
              } else{
                alertify.warning('Warning: No record was selected.');
              }
           }
         },
         main: {
          label: "Cancel",
          className: "btn-primary",
          callback: function() {}
        }
      }
     });  
  };

  $scope.sortCategoryByID = function(){
      if($scope.order == '-id'){
        $scope.order = 'id';
      } else{
        $scope.order = '-id';
      } 
  };

  $scope.sortCategoryByName = function(){
      if($scope.order == '-email'){
        $scope.order = 'email';
      } else{
        $scope.order = '-email';
      } 
  };

  $scope.init();
});