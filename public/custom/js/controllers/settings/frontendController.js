app.controller('frontendSettingsController', ['$scope','$location', '$http', function($scope, $location, $http) {

  $scope.url = $location.$$protocol+'://'+$location.$$host+'/';
  $scope.talentDisplayCount = '';
  $scope.address = '';
  $scope.globePhone = '';
  $scope.phone = '';
  $scope.email = '';
  $scope.musicApp = '';
  $scope.fbApp = '';

  $scope.init = function(){
    $http.get($scope.url + 'api/get-frontend-settings').
    success(function(data, status, headers, config) {
      $scope.talentDisplayCount = parseInt(data.talent_display_count);
      $scope.address = data.address;
      $scope.globePhone = data.globe_phone;
      $scope.phone = data.phone;
      $scope.email = data.email;
      $scope.musicApp = data.music_app;
      $scope.fbApp = data.fb_app;
    });
  };

  $scope.submitForm = function(){

      if ($scope.mpgForm.$valid) {

        $scope.loading = true;
        $http.post($scope.url + 'api/save-frontend-settings', {
          talent_display_count: $scope.talentDisplayCount,
          address: $scope.address,
          globe_phone: $scope.globePhone,
          phone: $scope.phone,
          email: $scope.email,
          music_app: $scope.musicApp,
          fb_app: $scope.fbApp,
        }).success(function(data, status, headers, config) {
          $scope.loading = false;
          alertify.success('The settings has been saved.');
        });
      }

  }

  $scope.init();

}]);