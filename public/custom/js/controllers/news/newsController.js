app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                   $('.featured-photo').html(element[0].files[0].name);
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.service('newsPhotoUpload', ['$http','$location', function ($http, $location) {
    this.uploadFileToUrl = function(file, uploadUrl){
        
        var self = this;
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function( data ){
            $('.featured-photo-image').val(data);
            $('.featured-photo-image-url').attr('src', config.url + 'img/news/small/' + data);
            
        })
        .error(function(){
        });
    }
}]);

app.controller('newsController', ['$scope', '$http', 'newsPhotoUpload', function($scope, $http, newsPhotoUpload){

  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.loading = false;
  $scope.publishStatus = false;
  $scope.featuredPhoto = '';
  $scope.model = [];
  $scope.editingData = {};
  $scope.textid = '';
  $scope.texttitle = '';
  $scope.textsummary = '';
  $scope.textdescription = '';
  $scope.textstatus = 'draft';

  $scope.texteditid = '';
  $scope.textedittitle = '';
  $scope.texteditsummary = '';
  $scope.texteditstatus = '';
  $scope.texteditdescription = '';  
  $scope.texteditfeaturedImage = '';
  
  $scope.order = '-title';
  $scope.content = true;

  // CKeditor options.
  $scope.options = {
    language: 'en',
    allowedContent: true,
    entities: false
  };

  $scope.onReady = function () {
    //CKEditor
  };
  // EOF CKeditor

  $scope.init = function() {
    $scope.loading = true;
    $scope.content = true;
    
    $http.get(config.url + 'api/news').
    success(function(data, status, headers, config) {

      $scope.model = data;
      for (var i = 0, length = data.length; i < length; i++) {
        $scope.editingData[data[i].news_id] = false;
      }

      $scope.content = false;
      $scope.loading = false;
    });

    // Get Page Count
    $http.get(config.url + 'api/get-page-count/' + 'news_display_count').
    success(function(data, status, headers, config) {
      $scope.pageSize = parseInt(data);
    });
  }; 

  $scope.changeStatus = function(){
    if($scope.publishStatus == true){
      $scope.publishStatus = false;
    } else{
      $scope.publishStatus = true;
    }
  };

  $scope.AddNewTrash = function(news){
    $scope.textid = '';
    $scope.texttitle = '';
    $scope.textsummary = '';
    $scope.textdescription = '';
    $scope.textstatus = 'draft';
    $('.featured-photo-image').val('');
    $('.featured-photo').html('choose photo');
    $('.featured-photo-image-url').attr('src', config.url + 'img/news/news.png');

    alertify.success('Record has been removed.');
    $( ".triggerClose" ).trigger( "click" );
  };

   $scope.uploadNewsFile = function(){
        var file = $scope.myFile;
        var uploadUrl = config.url + "upload/news";
        newsPhotoUpload.uploadFileToUrl(file, uploadUrl);
  };

  $scope.addNewNews = function(){
    $scope.textid = '';
    $scope.texttitle = '';
    $scope.textsummary = '';
    $scope.textdescription = '';
    $scope.textstatus = 'draft';
    $scope.texteditfeaturedImage = '';
    $('.featured-photo-image').val('');
    $('.featured-photo-image-url').attr('src', config.url + 'img/news/news.png');
    $('.featured-photo').html('choose photo');
  };

  $scope.submitForm = function(){
      if ($scope.newsForm.$valid) {

        $scope.loading = true;
        $http.post(config.url + 'api/news', {
          title: $scope.texttitle,
          summary: $scope.textsummary,
          status: $scope.textstatus,
          description: $scope.textdescription,
          featured_image: $('.featured-photo-image').val(),
        }).success(function(data, status, headers, config) {

          $scope.textid = '';
          $scope.texttitle = '';
          $scope.textsummary = '';
          $scope.textdescription = '';
          $scope.textstatus = 'draft';
          $('.featured-photo-image').val('');
          $('.featured-photo').html('choose photo');
          $('.featured-photo-image-url').attr('src', config.url + 'img/news/news.png');

          $( ".triggerClose" ).trigger( "click" );
          
          $scope.model.splice(0, 0, data);

          alertify.success('Record has been added.');
          $scope.loading = false;
        }); 
      }
  };

  $scope.editNews = function(news){
    $scope.texteditid = news.news_id;
    $scope.textedittitle = news.title;
    $scope.texteditsummary = news.summary;
    $scope.texteditstatus = news.status;
    $scope.texteditdescription = news.description;  
    $scope.texteditfeaturedImage = news.featured_image;
    $('.featured-photo-image').val(news.featured_image);
    var newsImage = 'news.png';
    if(news.featured_image){
      newsImage = news.featured_image;
    }
    $('.featured-photo-image-url').attr('src', config.url + 'img/news/small/'+newsImage);
    $('.featured-photo').html(news.featured_image);
  }

  $scope.submitUpdateForm = function(){
     if ($scope.editNewsForm.$valid) {

        $scope.loading = true;
        $http.put(config.url + 'api/news/' + $scope.texteditid, {
          title: $scope.textedittitle,
          summary: $scope.texteditsummary,
          description: $scope.texteditdescription,
          status: $scope.texteditstatus,
          featured_image: $('.featured-photo-image').val(),
        }).success(function(data, status, headers, config) {

          $scope.texteditid = '';
          $scope.textedittitle = '';
          $scope.texteditsummary = '';
          $scope.texteditdescription = '';
          $scope.texteditstatus = 'draft';
          $('.featured-photo-image').val('');
          $('.featured-photo').html('choose photo');
          $('.featured-photo-image-url').attr('src', config.url + 'img/news/news.png');

          $( ".triggerEditClose" ).trigger( "click" );
          $scope.init();

          alertify.success('News has been updated.');
          $scope.loading = false;
        }); 
      }
  };

  $scope.remove = function(rowId){

     bootbox.dialog({
      message: "Are you sure that you want to delete this row?",
      title: "ARE YOU SURE?",
      buttons: {
        danger: {
          label: "Confirm",
          className: "btn-danger",
          callback: function() {

            $scope.delete( rowId );
            alertify.success('Record has been deleted.');
         }
       },
       main: {
        label: "Cancel",
        className: "btn-primary",
        callback: function() {}
      }
    }
   });
  };

  $scope.getTotal = function(){
    if($scope.model.length == 0){
      $('#recordTable').hide(); 
    } else{ 
      $('#recordTable').show(); 
    }

    return $scope.model.length;
  };

  $scope.delete = function( rowId ){
    $scope.loading = true;
    $http.delete(config.url + 'api/news/' + rowId)
    .success(function() {

      $scope.init();

      $scope.loading = false;
    });
  };

  $scope.toggleAll = function() {
     var toggleStatus = $scope.isAllSelected;
     angular.forEach($scope.model, function(itm){ itm.selected = toggleStatus; });  
  };
  
  $scope.optionToggled = function(){
    $scope.isAllSelected = $scope.model.every(function(itm){ return itm.selected; })
  };

  $scope.deleteAllSelected = function(){
    bootbox.dialog({
        message: "Are you sure that you want to delete the selected records?",
        title: "ARE YOU SURE?",
        buttons: {
          danger: {
            label: "Confirm",
            className: "btn-danger",
            callback: function() {
              var obj = $scope.model;
              var selected = [];
              for(x in obj){
                if (obj.hasOwnProperty(x)) {
                  if(obj[x].selected == true ){
                    $scope.delete( obj[x].news_id );
                    selected.push(obj[x].news_id);
                  }
                }
              }

              if(selected.length > 0){
                alertify.success('Selected records has been deleted.');
              } else{
                alertify.warning('Warning: No record was selected.');
              }
           }
         },
         main: {
          label: "Cancel",
          className: "btn-primary",
          callback: function() {}
        }
      }
     });
  };

  $scope.sortRowByName = function(){
      if($scope.order == '-title'){
        $scope.order = 'title';
      } else{
        $scope.order = '-title';
      } 
  };

  $scope.init();
}]);