-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2017 at 07:36 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `talentdemodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `account_id` int(11) NOT NULL,
  `account_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(3, 4, '396tor5tbzuuSeZOQuFOa8yXkR4iX0Ft', 1, '2015-12-17 21:09:30', '2015-12-17 21:09:30', '2015-12-17 21:09:30'),
(13, 14, 'hJ7sX0yL5zHY1EStHU931X35sQ4jkyUh', 1, '2016-02-23 16:21:48', '2016-02-23 16:21:48', '2016-02-23 16:21:48'),
(14, 15, 'WQ9l9OCeVZrUNhW6iSReiM2jzc4otKFB', 1, '2016-02-23 16:23:26', '2016-02-23 16:23:26', '2016-02-23 16:23:26'),
(15, 16, 'glF02pjFsi1ChGBITQN2yOQuyTPNWvpL', 1, '2016-02-24 14:45:37', '2016-02-24 14:45:37', '2016-02-24 14:45:37'),
(16, 17, 'u7K27OgtIYw2ujCZruOLEQMscqGBYmkG', 1, '2016-02-24 17:14:15', '2016-02-24 17:14:15', '2016-02-24 17:14:15'),
(17, 18, '1bQTkUO7CB1eO5BfOSTDj22zGI9jJDkB', 1, '2016-02-25 12:01:04', '2016-02-25 12:01:04', '2016-02-25 12:01:04'),
(18, 19, 'EmmyH7h9YVzYIOcBwUqLRIrRUniDzd3R', 1, '2016-02-26 17:18:19', '2016-02-26 17:18:19', '2016-02-26 17:18:19'),
(19, 20, 'Vt5bANn9t3s16PjpJZeAB7FeilU7tyrh', 1, '2016-02-26 17:23:05', '2016-02-26 17:23:05', '2016-02-26 17:23:05'),
(20, 21, 'U3dBviPSTBTOi1Gh8rwqJiE5aCjYHcNg', 1, '2016-03-03 02:12:34', '2016-03-03 02:12:34', '2016-03-03 02:12:34'),
(21, 22, 'vNkntz9PJdsmE4yO5JouA6wXLwdgj6jo', 1, '2016-03-08 18:30:42', '2016-03-08 18:30:42', '2016-03-08 18:30:42'),
(22, 23, 'yuI7EHtMdc35SbaQSDGrQb4SQhTS6tbw', 1, '2016-03-31 01:48:29', '2016-03-31 01:48:29', '2016-03-31 01:48:29'),
(23, 26, 'MfgkNg5WpiV85p4GGJ5PRZ6gnbfixonc', 1, '2016-04-15 00:03:41', '2016-04-15 00:03:41', '2016-04-15 00:03:41'),
(24, 28, 'qzr7cvSUiXfbkNHBL6VBgmbmkuEB2UZz', 1, '2016-04-15 00:05:15', '2016-04-15 00:05:15', '2016-04-15 00:05:15'),
(25, 29, 't5rlS8uy0SsuUVwQho8KxBxJaoFYYHur', 1, '2016-04-15 00:07:18', '2016-04-15 00:07:18', '2016-04-15 00:07:18'),
(26, 30, 'SWqMMbFDUM86DfePj35JLVCZhxOUDPoZ', 1, '2016-04-15 00:08:17', '2016-04-15 00:08:17', '2016-04-15 00:08:17'),
(27, 33, '2Q4uYLb159WZ9EHpF6EsVeSs8N3fiRUE', 1, '2016-04-15 00:10:26', '2016-04-15 00:10:25', '2016-04-15 00:10:26'),
(28, 35, 'hSj9Vhd1OZ6YFEXuNgaODTKdNLaH1xag', 1, '2016-04-15 00:30:38', '2016-04-15 00:30:37', '2016-04-15 00:30:38'),
(29, 36, 'FrngM0Q4DuwGZtqru6RkoJlOwfuEMxwe', 1, '2016-04-30 03:29:20', '2016-04-30 03:29:20', '2016-04-30 03:29:20'),
(30, 37, 'ijhAeQjDaLyJaQ1awHHeDR0Aq4oLWbpD', 1, '2016-04-30 03:34:20', '2016-04-30 03:34:20', '2016-04-30 03:34:20'),
(31, 38, 'ribIJLBjNva9V925FY6shDrWRfiqjAEK', 1, '2016-05-02 16:03:23', '2016-05-02 16:03:23', '2016-05-02 16:03:23');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` enum('draft','published') CHARACTER SET utf8 NOT NULL DEFAULT 'draft',
  `summary` blob,
  `description` blob,
  `featured_image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `author` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nickname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` blob,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` enum('draft','published') CHARACTER SET utf8 NOT NULL DEFAULT 'draft',
  `summary` blob,
  `description` blob,
  `featured_image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `author` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `language` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_17_034207_create_todo_table', 2),
('2014_07_02_230147_migration_cartalyst_sentinel', 3),
('2016_04_11_023031_create_game_categories_table', 4),
('2016_04_12_022121_platforms', 5),
('2016_04_12_023047_platforms', 6),
('2016_04_12_024859_game_list', 7),
('2016_04_12_025227_operators', 8),
('2016_04_12_093822_mpg_list', 9);

-- --------------------------------------------------------

--
-- Table structure for table `model_gallery`
--

CREATE TABLE `model_gallery` (
  `id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `caption` text,
  `user_id` int(11) NOT NULL,
  `featured` enum('yes','no') NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `model_list`
--

CREATE TABLE `model_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8_unicode_ci,
  `model_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `height` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bust_chest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waist` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hips` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shoe_size` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dress_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `primary_photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `experiences` text COLLATE utf8_unicode_ci NOT NULL,
  `categories` text COLLATE utf8_unicode_ci NOT NULL,
  `featured_model` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `pages_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` blob,
  `author` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `permission_name` varchar(255) NOT NULL,
  `guest` enum('1','0') NOT NULL DEFAULT '0',
  `editor` enum('1','0') NOT NULL DEFAULT '0',
  `admin` enum('1','0') NOT NULL DEFAULT '0',
  `superadmin` enum('1','0') NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `permission_name`, `guest`, `editor`, `admin`, `superadmin`, `order`) VALUES
(1, 'add_new_customer', '0', '1', '1', '1', 1),
(2, 'edit_customer', '0', '1', '1', '1', 2),
(3, 'delete_customer', '0', '1', '1', '1', 3),
(4, 'add_blacklist', '0', '0', '1', '1', 4),
(5, 'edit_blacklist', '0', '0', '1', '1', 5),
(6, 'delete_blacklist', '0', '0', '1', '1', 6),
(7, 'add_transaction', '0', '1', '1', '1', 7),
(8, 'edit_transaction', '0', '1', '1', '1', 8),
(9, 'delete_transaction', '0', '1', '1', '1', 9);

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(83, 17, 'cS44XFl9UQ8somKlFm67E1iqnQwmGPEx', '2016-02-24 17:14:45', '2016-02-24 17:14:45'),
(84, 14, 'TS6KqObTrVVLVtiAGryXlfonMsCzNzub', '2016-02-24 18:52:49', '2016-02-24 18:52:49'),
(87, 14, 'z6cUWXBRStJvAmdbJqRBNj3fO1f0vfuN', '2016-02-25 10:11:02', '2016-02-25 10:11:02'),
(91, 14, 'e478hjJArIKFnBC1RVevz2sFVjFCI1oH', '2016-02-25 12:19:16', '2016-02-25 12:19:16'),
(94, 14, '72o993sedD3QbhWpE8YY4vttQiF0nOFs', '2016-02-26 15:51:13', '2016-02-26 15:51:13'),
(95, 14, 'lAVawOpKOvNNks76Z81FahzGB3aUxP5O', '2016-02-26 16:53:29', '2016-02-26 16:53:29'),
(97, 19, '4SjgCLV8DIEXVJ3CBlPyMTOpykweUv3t', '2016-02-26 17:19:00', '2016-02-26 17:19:00'),
(101, 16, '0vsdidUojKGI0PZPGtvtOJY65GoZpxdQ', '2016-02-26 21:05:50', '2016-02-26 21:05:50'),
(102, 14, 'yWt6EiBG8rqdMKUUlaNqj8Q6NdGnVWvk', '2016-02-27 14:02:40', '2016-02-27 14:02:40'),
(103, 14, 'MZ6PD7stNJd9wdm16p51Gr5bM57IFcXn', '2016-02-27 15:33:34', '2016-02-27 15:33:34'),
(104, 16, 'hMh5Q24CxTlhhH058V32s3GFBT8d7PKy', '2016-02-28 12:57:29', '2016-02-28 12:57:29'),
(105, 16, 'ed8tkTGFQPaIczlVaOdyJoqFbjXNHpa2', '2016-02-29 01:12:16', '2016-02-29 01:12:16'),
(106, 17, 'pWNWldfnYCS7zTLMrWXQP8oOUe4Ka0fk', '2016-02-29 13:36:48', '2016-02-29 13:36:48'),
(109, 17, 'EHZHOlElT5qcAIlja8ThgJ7duQOarmNk', '2016-02-29 16:44:01', '2016-02-29 16:44:01'),
(110, 14, '54OQuWX8a25eeRdra7EEcm8xVEeHwGaH', '2016-02-29 17:24:36', '2016-02-29 17:24:36'),
(113, 15, 'NDa0NEwqsGwJot58SSD9FTBfm9QNghLG', '2016-03-01 07:47:57', '2016-03-01 07:47:57'),
(114, 15, 'UmGvmU6TIV3M8YfFq6d79FVWSKCsFrG8', '2016-03-01 10:13:36', '2016-03-01 10:13:36'),
(116, 19, 'D0WfIX5zYqSB3B3Wgz3O2sbbAmbOab3U', '2016-03-01 13:50:53', '2016-03-01 13:50:53'),
(117, 15, 'hQUifw564hPvcCPA1NVKSVEp0LMDby5Q', '2016-03-01 15:18:10', '2016-03-01 15:18:10'),
(118, 19, 'ibbM8NHwucmG5iRW5mQchPLflDhNZy4x', '2016-03-01 15:20:08', '2016-03-01 15:20:08'),
(119, 14, 'Au2thAmMqSMuU7s0G5A2dxX8AGm5eYPG', '2016-03-01 15:46:12', '2016-03-01 15:46:12'),
(120, 16, 'gbxdsPidmhXLv5Fj7vJegMhdhasRmtmM', '2016-03-01 16:37:40', '2016-03-01 16:37:40'),
(121, 16, '7kPTmuAO6FN5vZkQcrLemJEFKCkB84Ce', '2016-03-01 19:09:10', '2016-03-01 19:09:10'),
(123, 16, 'q4zehjarrMuM7c3JMsTIhNS45RdTlpVs', '2016-03-01 22:19:18', '2016-03-01 22:19:18'),
(124, 17, 'Efbl7ybAtclUyFQ234ExJn2rxyfx02eh', '2016-03-02 10:31:47', '2016-03-02 10:31:47'),
(125, 19, 'Y9JV4ZH2DJY5PILAdDMHt0IcJc06Dtr5', '2016-03-02 11:05:01', '2016-03-02 11:05:01'),
(126, 17, 'Jp9kSOSGnKDahDF0TO17DViPzawtW7Yu', '2016-03-02 11:23:15', '2016-03-02 11:23:15'),
(127, 19, '5Dain6mwkLfwzsqFg9uGMagEOh7CSAV3', '2016-03-02 12:09:19', '2016-03-02 12:09:19'),
(128, 16, 'noWeGAvvJ5lmhmyg7iwHFNgypnBUgQCV', '2016-03-02 13:16:30', '2016-03-02 13:16:30'),
(130, 15, 'pUEbRLnStNBvQgcfNU74rJ9NG0C0fSZ8', '2016-03-02 16:01:55', '2016-03-02 16:01:55'),
(131, 15, 'xMRqUuc0JvLIhNQPlrIXQgUMAY7JII4T', '2016-03-02 16:51:45', '2016-03-02 16:51:45'),
(132, 19, 'tgSBU0IpT84OhDkjO4DqQPUw5ju9uJxj', '2016-03-02 16:59:15', '2016-03-02 16:59:15'),
(133, 14, 'oWqiur4tCEQSctIPeyJERbL6WPxkm6qk', '2016-03-02 17:04:10', '2016-03-02 17:04:10'),
(139, 21, 'wDJ5EupkPXR1XaN2JHCa4eTaowF3TNeX', '2016-03-03 02:13:52', '2016-03-03 02:13:52'),
(140, 17, '379z7Y9OdZa4NZPp90wBg2mpgkNBaEMO', '2016-03-03 12:00:53', '2016-03-03 12:00:53'),
(141, 15, 'PHRkBZOuv4AMudtJ3616baiLDLr2yr5p', '2016-03-03 12:24:48', '2016-03-03 12:24:48'),
(142, 15, '6j0ybDK2NnKdWNwRNM1qCwi3cb97sDu5', '2016-03-03 14:32:08', '2016-03-03 14:32:08'),
(143, 16, 'slADBm2dPVfr1eILoOzReBX64lbhRLnL', '2016-03-03 14:40:44', '2016-03-03 14:40:44'),
(144, 14, 'DJrsCyGqvdYTizEmOPBAD4dcm74KpLlF', '2016-03-03 15:01:23', '2016-03-03 15:01:23'),
(145, 15, 'bNRkfJqzx15o4Dj5K2QsMxlj0L9qUzeU', '2016-03-03 15:35:33', '2016-03-03 15:35:33'),
(148, 14, 'lKT2QCzIUapdn8ib3N9J09qhjtX46gaw', '2016-03-03 16:16:14', '2016-03-03 16:16:14'),
(149, 15, 'QOW2tDuVfYwseVMtLt5onnUkALQtoslA', '2016-03-03 16:40:35', '2016-03-03 16:40:35'),
(150, 14, 'C6w0faoC6tJ6tzXfjbUw4dsG4AURNf6A', '2016-03-04 14:31:03', '2016-03-04 14:31:03'),
(152, 15, 'ahmcSXysGOZG6JvuI1DZvuoANK3I8jrN', '2016-03-05 10:43:30', '2016-03-05 10:43:30'),
(153, 16, 'gTr2wOpPw6lZzy2ProYU5rP3CzPxeAQf', '2016-03-05 21:23:26', '2016-03-05 21:23:26'),
(154, 16, 'gw9e7igug8FzQmi2Y4yCEtKdErvGfn1c', '2016-03-06 13:52:46', '2016-03-06 13:52:46'),
(155, 16, 'ak4FK4B3cr7TPc1w7x61BLsNg3RhXGHa', '2016-03-06 14:44:05', '2016-03-06 14:44:05'),
(156, 16, 'Zgrjrp9UEmHzfxx6Mtzlp0g17oUqjQVu', '2016-03-06 17:02:49', '2016-03-06 17:02:49'),
(157, 16, 'C5pnrj7bQqMey1iYxRSezaoqe99FXLWz', '2016-03-06 19:56:47', '2016-03-06 19:56:47'),
(159, 16, 'WkAflkQB6AnWcR9YrXxZBQcKEvZeH6mw', '2016-03-06 21:55:50', '2016-03-06 21:55:50'),
(160, 14, 'hvTc4igU4B0mPeMAWqpkljRSLECFywdB', '2016-03-07 08:47:20', '2016-03-07 08:47:20'),
(161, 19, 'sP0tDqUj7RB3EFd31z8lvTvp1Xw6xmM5', '2016-03-07 09:39:51', '2016-03-07 09:39:51'),
(162, 16, 'HTOMe5qljTB9XNZfd1fVISwiQKCb4mIH', '2016-03-07 10:11:58', '2016-03-07 10:11:58'),
(163, 14, '15b0WtMuGuSx9TAmu9veaDNUW6egmxei', '2016-03-07 10:17:41', '2016-03-07 10:17:41'),
(165, 19, 'KRE0BfBXonnSjo4LLSXwg9OAQD6hZuqg', '2016-03-07 15:37:13', '2016-03-07 15:37:13'),
(166, 14, 'xlwzo6JPqGmvwKUjBjLCSPpKqZa37Xgn', '2016-03-07 15:44:41', '2016-03-07 15:44:41'),
(170, 14, 'rRfLZQRUBOFoGCRTmFyCDTcCZaARadlf', '2016-03-07 18:40:05', '2016-03-07 18:40:05'),
(171, 19, 'Ku0px94jVpezytzqaoWfmcpSHoQAs1MH', '2016-03-07 18:42:08', '2016-03-07 18:42:08'),
(172, 19, 'HONiPfjsJiwuo7yMMNo6Xkf1GL5ff2YF', '2016-03-07 19:21:49', '2016-03-07 19:21:49'),
(173, 14, 'sAjJtUuYs113osnZlrtQsqVXQiLRSVdj', '2016-03-08 17:18:45', '2016-03-08 17:18:45'),
(180, 16, 'WmxHvXDg1IcKO7wO55RKABW8ffIJgvPQ', '2016-03-09 00:48:04', '2016-03-09 00:48:04'),
(183, 21, 'OR8jEFI980B7YyeG5XJDqKOUpk1v0hyK', '2016-03-09 00:48:58', '2016-03-09 00:48:58'),
(184, 14, 'jFJU7ewQwkKRfnELPpcW8KGHW2GdKIk9', '2016-03-09 08:37:01', '2016-03-09 08:37:01'),
(185, 16, 'Une1C592dw3YAWYGlIlMQxunqoYBYLVK', '2016-03-09 15:02:41', '2016-03-09 15:02:41'),
(188, 19, 'YLCVqMmbpW0Wr61zu0GQMjBCH77yVGVE', '2016-03-09 15:54:30', '2016-03-09 15:54:30'),
(190, 14, 'AnUSCkpeSZ2KFd4MbnNdFWlJS5XbkcpX', '2016-03-09 17:22:12', '2016-03-09 17:22:12'),
(191, 15, 'D6ZwmBuCk1M052DuMFOiktZNl8lG1hvd', '2016-03-09 17:48:21', '2016-03-09 17:48:21'),
(192, 19, 'P5jhTHjhJlixxnqV2TTv55FGR7rxyoWe', '2016-03-09 17:49:01', '2016-03-09 17:49:01'),
(193, 14, 'pB81q4mCCfNyYkTSioLJ2eJHiPGW83re', '2016-03-09 18:32:28', '2016-03-09 18:32:28'),
(194, 19, 'tfs4eDf0ZNh11wv4Cl3jQMgOwZ6rttW3', '2016-03-09 19:24:31', '2016-03-09 19:24:31'),
(196, 16, 'zpoYmre0CjvZ0m0LpteVZ9qxQbMQkdM0', '2016-03-09 22:30:35', '2016-03-09 22:30:35'),
(201, 19, 'j7ALuIGAqrEmS6DI8z7Tm1XLfZDQjB4h', '2016-03-10 15:56:55', '2016-03-10 15:56:55'),
(207, 14, '0eJ718UnNKkgiAjPWPSQv12zZrm8kKvw', '2016-03-10 16:50:40', '2016-03-10 16:50:40'),
(213, 16, 'B14OWFU7LbreuxNwkCv5wBRqTIgfwC6Q', '2016-03-11 06:14:19', '2016-03-11 06:14:19'),
(217, 19, 'oH9bUiblpKH6TzbxLUIXaNbIadV5ejtB', '2016-03-11 14:20:06', '2016-03-11 14:20:06'),
(222, 14, '38tnDooXwvyygI6K7wAoi8BFFhq5XI10', '2016-03-14 14:07:15', '2016-03-14 14:07:15'),
(224, 14, 'l3RBhLdQrx7lkTT2g9Csf0gjz4rYVBUq', '2016-03-15 10:08:36', '2016-03-15 10:08:36'),
(227, 22, 'sNGd3en4fV5XDPZQHmAhhLKg3thiVtJR', '2016-03-15 15:02:50', '2016-03-15 15:02:50'),
(228, 19, 'rpAFxtewCKa4nAoODz5n5SIGkBQE8YJP', '2016-03-15 15:08:42', '2016-03-15 15:08:42'),
(230, 19, '2NXb1UFagaRwaEYJuVC8GY2xeQEQwfRF', '2016-03-15 16:24:35', '2016-03-15 16:24:35'),
(232, 16, 'b24dY9OdVHwq5MST6EU4hFVp9mVqDAcK', '2016-03-16 04:11:40', '2016-03-16 04:11:40'),
(236, 19, 'Da3METCQ5LMi4nyeSNanOH70pnb2kJLo', '2016-03-16 13:47:56', '2016-03-16 13:47:56'),
(238, 14, 'XtQoShbynoRgysAs5LOOf9lkexyJPGin', '2016-03-16 13:49:16', '2016-03-16 13:49:16'),
(239, 16, 'dQ1dLBwLsg1lLyHlTEbuX1FCwuYRRhoP', '2016-03-16 14:21:26', '2016-03-16 14:21:26'),
(240, 15, '0Nznt24c2tXR66EJAq6X9pqjh1qVLvSd', '2016-03-16 14:24:01', '2016-03-16 14:24:01'),
(241, 16, 'D8KZyFDRdHMM6MlMUxB6leOsDKxBNc2v', '2016-03-16 02:43:41', '2016-03-16 02:43:41'),
(242, 16, 'kVrAyeYg7ItkmYSe6LP60A7SPakqSusL', '2016-03-21 22:14:38', '2016-03-21 22:14:38'),
(243, 16, 'GMMQ1OEs0eCfGQtm0p06pP0LaxLZHvcC', '2016-03-26 03:23:48', '2016-03-26 03:23:48'),
(246, 21, 'ci6FDbxDtmeOn1R8c9kheZsnzqeRUDE1', '2016-03-28 01:19:10', '2016-03-28 01:19:10'),
(247, 16, 'WVAJ5F2RoDe3bq0Hi25fBDaduLlrUWPm', '2016-03-28 04:48:29', '2016-03-28 04:48:29'),
(248, 16, 'bhJvgIIkKDNRqI42FlsusqaStVlHRZ2w', '2016-03-28 07:30:42', '2016-03-28 07:30:42'),
(249, 16, 'b8dHSCxiKViM8aOqyguWCTv7Jv1dlpz7', '2016-03-28 22:11:45', '2016-03-28 22:11:45'),
(250, 16, 'za7DHeWBkagNpgKjBnIkO49utBw1eTiP', '2016-03-29 09:02:20', '2016-03-29 09:02:20'),
(251, 16, '27Nmz6PpJu8n2J5aPu2grEw7rH9WvhpH', '2016-03-30 01:31:55', '2016-03-30 01:31:55'),
(271, 16, 'NQYSAYyQF1t8DZj8oJxn5cmmXOxJgqBn', '2016-03-31 02:08:58', '2016-03-31 02:08:58'),
(273, 16, 'ss2x9HI8oV8ia53P2bCivivjAJakriUL', '2016-04-06 01:08:28', '2016-04-06 01:08:28'),
(275, 16, 'cqkgSYz5ugScLdugWNjbPVuVgtzQn4vz', '2016-04-10 07:31:08', '2016-04-10 07:31:08'),
(276, 16, 'qdffRh7nNyrgw8MceH38Ee2zzVKZhLSI', '2016-04-10 18:10:40', '2016-04-10 18:10:40'),
(278, 16, 'tPSgX9HcTACeKzxsqxCouAylwkcBqhuw', '2016-04-11 22:19:12', '2016-04-11 22:19:12'),
(279, 16, '920yolcLKEJrqY4g9GBxbLoiMXmIqwf1', '2016-04-12 18:19:38', '2016-04-12 18:19:38'),
(282, 16, 'YFbihg25ZLNOjtbYWGRdgh6ciwx5gkCx', '2016-04-13 01:51:10', '2016-04-13 01:51:10'),
(288, 16, 'EIz0UFVjsauhxEaLueGkYVvtOSXNmTz0', '2016-04-14 22:42:03', '2016-04-14 22:42:03'),
(298, 16, 'QNaMMCY1cLmaxRAqrenJdJsKAdXFVJLv', '2016-04-18 01:36:04', '2016-04-18 01:36:04'),
(299, 16, 'hcbMJ2mTXrPGCoeF8Pfj4MAF9VbXF4SW', '2016-04-18 17:34:02', '2016-04-18 17:34:02'),
(300, 16, 'NF01uGQW6HqO1WWYWDpVE4mX4bEVYgFR', '2016-04-18 21:54:30', '2016-04-18 21:54:30'),
(301, 16, 'Z4CKX1M7EU3FXtQwki4XryTACNobGTM3', '2016-04-19 17:44:29', '2016-04-19 17:44:29'),
(303, 16, 'Z1QyPzU2RPIlwN9TSKXzxyTYyDXeBhX6', '2016-04-21 04:34:42', '2016-04-21 04:34:42'),
(307, 16, 'oSC4gcvmnFjiToQKhZqCCSF9FmNsxqUt', '2016-04-25 18:25:34', '2016-04-25 18:25:34'),
(308, 16, 'z28r42pi7KxJkAnelpvSuYOv0MuUdzwn', '2016-04-26 01:18:16', '2016-04-26 01:18:16'),
(309, 16, 'aEtBzJMuaTohZb4Agxl4nTXcw2fcjQmy', '2016-04-26 18:02:01', '2016-04-26 18:02:01'),
(310, 16, 'Z8kCpzYqb09n6CKKLmuTvWkAYr9ViiHW', '2016-04-26 22:21:19', '2016-04-26 22:21:19'),
(312, 16, 'HQkFKCYXsmrMvFjVMWinL2bOMfcxTgm9', '2016-04-27 08:36:22', '2016-04-27 08:36:22'),
(315, 16, '7IzecRmpZFPVeewfEc35hZBBoMlZ9rvI', '2016-04-28 08:46:51', '2016-04-28 08:46:51'),
(319, 16, '0MJTjbTQLu85g9WRUHdpSPOR5nuWrjuW', '2016-04-29 11:04:37', '2016-04-29 11:04:37'),
(320, 16, 'wdeZQGYxv4mQv2465ERHfJxdjJ71tqNA', '2016-04-29 18:25:37', '2016-04-29 18:25:37'),
(324, 16, 'eDRE6IqIdLO0pRaJksdjVd4uP0DKottb', '2016-04-30 15:59:14', '2016-04-30 15:59:14'),
(325, 16, 'QRvtxe3uqQiyBSHpTmQdJ8durhIio1tG', '2016-05-02 00:57:49', '2016-05-02 00:57:49'),
(326, 16, '7ACtTH3ogIPoiO7j4BZD7uP6vXNiYHBZ', '2016-05-02 01:15:22', '2016-05-02 01:15:22'),
(329, 38, 'SD94JK7vWVOwmEGbTuCabyymWE9Wxx7v', '2016-05-02 16:05:54', '2016-05-02 16:05:54'),
(330, 16, 'h5PPElxi6B6i0ua7JkaKzfuoJwrtFsIi', '2016-05-02 18:15:28', '2016-05-02 18:15:28'),
(332, 38, '81yLEY3OQIrC2e388Eg15ZZrisXheSHI', '2016-05-02 21:01:18', '2016-05-02 21:01:18'),
(334, 38, 'Z1JPhWxkaRvQfPEGWFGgl2AxmdTB9IX9', '2016-05-03 04:19:38', '2016-05-03 04:19:38'),
(335, 38, 'qNl3iV1a5MgatRBOIAYvhKLouzAnPX1X', '2016-05-03 06:20:31', '2016-05-03 06:20:31'),
(336, 38, 'PGSuwWu0osXuQEv5VZM1TnIzFBuMUHoN', '2016-05-03 09:33:35', '2016-05-03 09:33:35'),
(338, 16, 'cbDws7VuRn36Fj2JUeQBElVGHJqyTR4f', '2016-05-03 15:21:15', '2016-05-03 15:21:15'),
(341, 38, 'stz1ujsLl8TRrp5NCcO6jiHJiHndp9Ql', '2016-05-03 16:33:30', '2016-05-03 16:33:30'),
(342, 38, 'e7inCRvUeM5vAHkipGH5otrFmJlGrFm9', '2016-05-04 04:03:52', '2016-05-04 04:03:52'),
(343, 16, 'uVKZRTMjyguqncAxMSiYDMPTF6zZQm65', '2016-05-04 05:33:15', '2016-05-04 05:33:15'),
(344, 38, 'IaWOLW4MXZrmSaOxhtPx1wJAOpF5npNP', '2016-05-04 05:46:28', '2016-05-04 05:46:28'),
(345, 16, 'ysoJx6MvTTLkroWMu7K5N0kdvq8C5y8Q', '2016-05-04 12:21:31', '2016-05-04 12:21:31'),
(346, 38, 'P2KBY0lKkoWODhpsJhFO4mXfSSjNvztl', '2016-05-04 12:23:02', '2016-05-04 12:23:02'),
(349, 38, 'WMFJB1VMuR5aQwxvNwQnJrMTNeJS3fko', '2016-05-05 03:34:36', '2016-05-05 03:34:36'),
(350, 38, '8e4Bvtn1rQVI3b8YkkUWlgwadBKFLWmt', '2016-05-05 12:19:46', '2016-05-05 12:19:46'),
(351, 38, 'HWq5dKA4NiMJr1Gqo7APy4rTmACMmqcy', '2016-05-05 17:13:13', '2016-05-05 17:13:13'),
(352, 38, 'l94c529n35f5ImK3fVTh28xPp2euYDn4', '2016-05-06 03:28:29', '2016-05-06 03:28:29'),
(353, 16, 'Eg4LHg4Lbr0LyHgkVa1rjsrDgQSH0Qop', '2016-05-06 07:59:23', '2016-05-06 07:59:23'),
(354, 16, 'nuDKkgj8ySYwpb7aFErv75WYX91DIPDx', '2016-05-06 08:00:56', '2016-05-06 08:00:56'),
(355, 38, 'xoEpSbZyjczfLR3GwEWo223IXhhdvcBb', '2016-05-06 08:25:13', '2016-05-06 08:25:13'),
(357, 38, 'hunWAo1Mw5optZxbZ0xC0WPtFBraQTGJ', '2016-05-06 11:14:41', '2016-05-06 11:14:41'),
(358, 38, 'yXxgoOP6kWRiP9Vp3I3cTlgjcu6FZRuf', '2016-05-06 19:00:48', '2016-05-06 19:00:48'),
(359, 38, 'Z9dcYruquhsKKpJKs70ScOzcUAdVR4ll', '2016-05-07 05:49:48', '2016-05-07 05:49:48'),
(360, 38, 'Fgn2jWNaDeLKDuhiYzBrOMbBzMiG67Dg', '2016-05-08 07:20:45', '2016-05-08 07:20:45'),
(362, 38, 'Hio2DlgZmqRPdxycK7JdJPC7zjbX5vJy', '2016-05-09 08:44:47', '2016-05-09 08:44:47'),
(364, 38, 'dDZOm6xAEqR8IsbfFznO9OOEVMLSsmFq', '2016-05-11 04:53:25', '2016-05-11 04:53:25'),
(365, 38, 'VbfsnkJ4UfedmiGd9tRYN3cNyJDIpJ4Y', '2016-05-12 03:28:22', '2016-05-12 03:28:22'),
(366, 38, 'CY5gj5K77s54YoK8acNCBGghA1u4126g', '2016-05-13 05:16:38', '2016-05-13 05:16:38'),
(367, 38, 'wNPDa9B2gqCQsxV2MFJl4alQoV9VPjRL', '2016-05-13 09:03:48', '2016-05-13 09:03:48'),
(368, 38, 'S5wdn12Vx5SfV0tw1QJ7aTz9wPYrx4LN', '2016-05-13 12:23:05', '2016-05-13 12:23:05'),
(369, 38, 'fm9qJQgHoBohF5f7nJhwZaSeotvmIbjb', '2016-05-14 06:14:29', '2016-05-14 06:14:29'),
(372, 38, 'EejgQbo7thcUQCTFQtrQJo169ZFn5DZN', '2016-05-16 08:15:11', '2016-05-16 08:15:11'),
(373, 38, 'YWxTQOxVXOBT3dG1FNj9SsD19dQgAwLR', '2016-05-18 08:20:15', '2016-05-18 08:20:15'),
(375, 38, 'pjh0fclYIMYDWjxPKXz3Yvov5Ioxfucx', '2016-05-20 04:38:44', '2016-05-20 04:38:44'),
(379, 38, '2k8MefE02REft0pwWA3KO0W2v5ocEhZV', '2016-05-22 17:56:20', '2016-05-22 17:56:20'),
(380, 38, '7V0GqP3sSL67KYDiROSNVEFY4G57OLaH', '2016-05-25 18:21:08', '2016-05-25 18:21:08'),
(381, 16, '5h8ZosIKS6GQQ1fW23hGglVwGESzKXKL', '2016-05-27 10:44:48', '2016-05-27 10:44:48'),
(382, 38, 'Dhw5sTIEiVfii3GNoDoTH53A67M0enfZ', '2016-06-12 12:20:37', '2016-06-12 12:20:37'),
(389, 38, '748dPQeENI2dKvqLvqHhb5gOAb9eQRKI', '2016-07-02 08:00:58', '2016-07-02 08:00:58'),
(390, 38, 'pZcYGaldg9ngBAQa88ZuMBz9Ubjhpdd5', '2016-07-04 04:51:39', '2016-07-04 04:51:39'),
(391, 38, 'KEszw7hXQ6tBi5RqpV49MlIFsHUK7PFG', '2016-07-05 05:39:28', '2016-07-05 05:39:28'),
(392, 38, 'fHd0IgyLwW8PJDxL1IBWUn4Da4Dteh6X', '2016-07-06 07:14:19', '2016-07-06 07:14:19'),
(393, 38, '8eOn3sNk8Zy5mLSLBj70iPWAUO5fbRH2', '2016-07-11 12:30:27', '2016-07-11 12:30:27'),
(394, 38, '1sjLhYbncd9PHC0RaFY3pcmTu0D0weyo', '2016-07-12 03:36:41', '2016-07-12 03:36:41'),
(396, 38, 'cuVlUKaMopcuCc39cEHpb1QGBAfOYgQS', '2016-09-27 06:34:35', '2016-09-27 06:34:35'),
(397, 38, '3LzFZVdhoGS4F487JZERk8KC6JXeFoMb', '2016-10-01 06:51:26', '2016-10-01 06:51:26'),
(398, 38, 'vNZGRw1l6HkdwTfamlzFGWLJxA0n6zzr', '2016-10-01 08:58:37', '2016-10-01 08:58:37'),
(399, 38, 'hUG7ZSmYuoHaOfnBGWrgsz5Pq9ZxJ774', '2016-10-03 11:55:09', '2016-10-03 11:55:09'),
(400, 38, 'XvBmy0mMaqBsmzdNfV6PN9sLLpIsx1Da', '2017-01-24 12:19:43', '2017-01-24 12:19:43'),
(401, 38, 'ydrCjh49Mtu3LNzf5jKeXizM0LjeY4R6', '2017-02-13 09:53:26', '2017-02-13 09:53:26'),
(403, 38, 'lYHUmvFNStacb2xCw1BEWpgMiVmq7NtN', '2017-07-08 07:49:31', '2017-07-08 07:49:31');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `request_by` int(11) NOT NULL,
  `type` enum('profile','payment','transaction') NOT NULL,
  `customer_no` varchar(60) NOT NULL,
  `type_id` int(11) NOT NULL,
  `borrower_personal_details` text NOT NULL,
  `borrower_spouse` text NOT NULL,
  `borrower_service` text NOT NULL,
  `borrower_financial` text NOT NULL,
  `borrower_memorial` text NOT NULL,
  `borrower_insurance` text NOT NULL,
  `comaker_personal_details` text NOT NULL,
  `comaker_spouse` text NOT NULL,
  `comaker_service` text NOT NULL,
  `comaker_financial` text NOT NULL,
  `payments` text NOT NULL,
  `transactions` text NOT NULL,
  `status` enum('pending','declined','approved','completed') NOT NULL DEFAULT 'pending',
  `date_requested` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_approved` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_declined` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `request_by`, `type`, `customer_no`, `type_id`, `borrower_personal_details`, `borrower_spouse`, `borrower_service`, `borrower_financial`, `borrower_memorial`, `borrower_insurance`, `comaker_personal_details`, `comaker_spouse`, `comaker_service`, `comaker_financial`, `payments`, `transactions`, `status`, `date_requested`, `date_approved`, `date_declined`) VALUES
(17, 21, 'profile', '43450832-260822016', 0, '[\"lastname\",\"cellphone\"]', '[\"spouse_employeraddress\",\"spouse_salary\"]', '', '', '', '[\"insured\",\"spotcash_premium\"]', '[\"age\",\"address\"]', '', '', '', '', '', 'completed', '2016-03-02 21:14:05', '2016-03-03 10:13:40', '0000-00-00 00:00:00'),
(18, 14, 'profile', '46551365-290922016', 0, '[\"firstname\",\"middle_initial\",\"cellphone\"]', '[\"spouse_employeraddress\",\"spouse_salary\"]', '[\"station_no\",\"school_address\"]', '[\"monthly_net_pay\"]', '', '', '[\"middle_initial\",\"age\"]', '', '', '', '', '', 'completed', '2016-03-03 11:16:51', '2016-03-04 00:15:45', '0000-00-00 00:00:00'),
(19, 21, 'transaction', '80440633-40932016', 261, '', '', '', '', '', '', '', '', '', '', '', '[\"remarks\"]', 'completed', '2016-03-06 15:15:00', '2016-03-07 04:09:23', '0000-00-00 00:00:00'),
(21, 14, 'transaction', '20886152-40932016', 247, '', '', '', '', '', '', '', '', '', '', '', '[\"memorial_coop_amount\"]', 'declined', '2016-03-07 10:44:25', '0000-00-00 00:00:00', '2016-03-07 23:44:25'),
(22, 14, 'transaction', '46053651-40932016', 251, '', '', '', '', '', '', '', '', '', '', '', '[\"cash_advance_collected_amount\"]', 'declined', '2016-03-07 10:44:30', '0000-00-00 00:00:00', '2016-03-07 23:44:30'),
(23, 21, 'transaction', '88474334-40932016', 245, '', '', '', '', '', '', '', '', '', '', '', '[\"memorial_coop_amount\"]', 'completed', '2016-03-07 10:47:58', '2016-03-07 23:47:35', '0000-00-00 00:00:00'),
(24, 14, 'transaction', '46053651-40932016', 251, '', '', '', '', '', '', '', '', '', '', '', '[\"cash_advance_collected_amount\"]', 'declined', '2016-03-07 14:24:29', '0000-00-00 00:00:00', '2016-03-08 03:24:29'),
(25, 14, 'transaction', '46053651-40932016', 251, '', '', '', '', '', '', '', '', '', '', '', '[\"cash_advance_collected_amount\"]', 'completed', '2016-03-08 12:31:02', '2016-03-08 03:21:24', '0000-00-00 00:00:00'),
(26, 21, 'transaction', '36580939-161132016', 412, '', '', '', '', '', '', '', '', '', '', '', '[\"transaction_status\"]', 'completed', '2016-03-31 09:10:48', '2016-03-31 09:09:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'guest', 'Guest', NULL, '2016-02-22 05:04:43', '2016-02-22 05:04:43'),
(2, 'editor', 'Editor', NULL, '2016-02-22 05:05:02', '2016-02-22 05:05:02'),
(3, 'admin', 'Administrator', NULL, '2016-02-22 05:05:15', '2016-02-22 05:05:15'),
(4, 'superadmin', 'Super Administrator', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` bigint(20) NOT NULL,
  `setting_name` varchar(64) NOT NULL,
  `setting_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `setting_name`, `setting_value`) VALUES
(1, 'timezone', 'Asia/Manila'),
(72, 'applicationname', 'Marlone Rubio Photography'),
(73, 'dateformat', 'F j, Y'),
(74, 'timeformat', 'g:i A'),
(75, 'mpg_display_count', '10'),
(76, 'operators_display_count', '10'),
(77, 'gamelist_display_count', '30'),
(78, 'gamecategory_display_count', '10'),
(79, 'platforms_display_count', '10'),
(80, 'importurl', 'http://lsl.omegasys.eu//ps/ips/getMostPopularGames?size=200'),
(81, 'apiurl', 'most-popular-games'),
(82, 'access', 'true'),
(83, 'experience_display_count', '10'),
(84, 'categories_display_count', '10'),
(85, 'modellist_display_count', '100'),
(86, 'pages_display_count', '10'),
(87, 'news_display_count', '10'),
(88, 'talent_display_count', '12'),
(89, 'address', 'MARLONE RUBIO PHOTOGRAPHY'),
(90, 'globe_phone', 'https://twitter.com'),
(91, 'phone', '+65 xxxx xx44'),
(92, 'email', 'marlonerubio@gmail.com'),
(93, 'music_app', 'https://www.instagram.com/marlonerubio'),
(95, 'fb_app', 'https://www.facebook.com/marlonerubiophotography/'),
(96, 'customers_display_count', '10'),
(97, 'google_analytics_api', 'UA-79193177-1'),
(98, 'slide_box_height', '400'),
(99, 'slide_box_width', '100'),
(100, 'menu_font_size', '20'),
(101, 'menu_font_weight', 'normal'),
(102, 'menu_text_transform', 'lowercase'),
(103, 'slide_font_size', '74'),
(104, 'slide_line_height', '60'),
(105, 'slide_text_shadow', '0 0 8px #000000'),
(106, 'slide_description_font_size', '18'),
(107, 'slide_description_margin_top', '10'),
(108, 'slide_description_text_shadow', '0 0 0px #000000'),
(109, 'slide_box_line_height', '30'),
(110, 'media_display_count', '10');

-- --------------------------------------------------------

--
-- Table structure for table `shortlists`
--

CREATE TABLE `shortlists` (
  `shortlist_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shortlists`
--

INSERT INTO `shortlists` (`shortlist_id`, `customer_id`, `talent_id`, `created_at`, `updated_at`) VALUES
(1, 37, 115, '2016-04-30 06:26:47', '2016-04-30 06:26:47'),
(2, 37, 87, '2016-04-30 06:28:22', '2016-04-30 06:28:22'),
(3, 38, 112, '2016-04-30 07:23:29', '2016-04-30 07:23:29'),
(4, 38, 109, '2016-04-30 07:23:33', '2016-04-30 07:23:33'),
(5, 38, 111, '2016-04-30 07:23:34', '2016-04-30 07:23:34'),
(6, 38, 124, '2016-04-30 07:23:37', '2016-04-30 07:23:37'),
(7, 38, 108, '2016-04-30 07:23:38', '2016-04-30 07:23:38');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2015-12-17 00:02:26', '2015-12-17 00:02:26'),
(2, NULL, 'ip', '127.0.0.1', '2015-12-17 00:02:26', '2015-12-17 00:02:26'),
(3, NULL, 'global', NULL, '2015-12-17 00:02:38', '2015-12-17 00:02:38'),
(4, NULL, 'ip', '127.0.0.1', '2015-12-17 00:02:38', '2015-12-17 00:02:38'),
(5, NULL, 'global', NULL, '2015-12-17 19:04:02', '2015-12-17 19:04:02'),
(6, NULL, 'ip', '127.0.0.1', '2015-12-17 19:04:03', '2015-12-17 19:04:03'),
(7, NULL, 'global', NULL, '2015-12-17 19:04:22', '2015-12-17 19:04:22'),
(8, NULL, 'ip', '127.0.0.1', '2015-12-17 19:04:22', '2015-12-17 19:04:22'),
(9, NULL, 'global', NULL, '2015-12-17 19:05:24', '2015-12-17 19:05:24'),
(10, NULL, 'ip', '127.0.0.1', '2015-12-17 19:05:24', '2015-12-17 19:05:24'),
(11, NULL, 'global', NULL, '2015-12-17 19:05:41', '2015-12-17 19:05:41'),
(12, NULL, 'ip', '127.0.0.1', '2015-12-17 19:05:41', '2015-12-17 19:05:41'),
(13, NULL, 'global', NULL, '2015-12-17 19:07:26', '2015-12-17 19:07:26'),
(14, NULL, 'ip', '127.0.0.1', '2015-12-17 19:07:26', '2015-12-17 19:07:26'),
(15, NULL, 'global', NULL, '2015-12-17 19:07:51', '2015-12-17 19:07:51'),
(16, NULL, 'ip', '127.0.0.1', '2015-12-17 19:07:51', '2015-12-17 19:07:51'),
(17, NULL, 'global', NULL, '2015-12-17 19:43:59', '2015-12-17 19:43:59'),
(18, NULL, 'ip', '127.0.0.1', '2015-12-17 19:43:59', '2015-12-17 19:43:59'),
(19, NULL, 'global', NULL, '2015-12-17 19:44:12', '2015-12-17 19:44:12'),
(20, NULL, 'ip', '127.0.0.1', '2015-12-17 19:44:12', '2015-12-17 19:44:12'),
(21, NULL, 'global', NULL, '2015-12-17 19:49:09', '2015-12-17 19:49:09'),
(22, NULL, 'ip', '127.0.0.1', '2015-12-17 19:49:09', '2015-12-17 19:49:09'),
(23, NULL, 'global', NULL, '2015-12-17 19:52:27', '2015-12-17 19:52:27'),
(24, NULL, 'ip', '127.0.0.1', '2015-12-17 19:52:27', '2015-12-17 19:52:27'),
(25, NULL, 'global', NULL, '2015-12-17 19:52:38', '2015-12-17 19:52:38'),
(26, NULL, 'ip', '127.0.0.1', '2015-12-17 19:52:38', '2015-12-17 19:52:38'),
(27, NULL, 'global', NULL, '2015-12-17 19:52:48', '2015-12-17 19:52:48'),
(28, NULL, 'global', NULL, '2015-12-20 18:03:52', '2015-12-20 18:03:52'),
(29, NULL, 'ip', '127.0.0.1', '2015-12-20 18:03:53', '2015-12-20 18:03:53'),
(30, NULL, 'global', NULL, '2015-12-20 18:04:03', '2015-12-20 18:04:03'),
(31, NULL, 'ip', '127.0.0.1', '2015-12-20 18:04:03', '2015-12-20 18:04:03'),
(33, NULL, 'global', NULL, '2015-12-20 22:22:19', '2015-12-20 22:22:19'),
(34, NULL, 'ip', '127.0.0.1', '2015-12-20 22:22:19', '2015-12-20 22:22:19'),
(35, NULL, 'global', NULL, '2016-01-05 23:17:22', '2016-01-05 23:17:22'),
(36, NULL, 'ip', '127.0.0.1', '2016-01-05 23:17:22', '2016-01-05 23:17:22'),
(37, NULL, 'global', NULL, '2016-01-11 17:31:31', '2016-01-11 17:31:31'),
(38, NULL, 'ip', '127.0.0.1', '2016-01-11 17:31:31', '2016-01-11 17:31:31'),
(39, NULL, 'global', NULL, '2016-02-17 18:09:20', '2016-02-17 18:09:20'),
(40, NULL, 'ip', '127.0.0.1', '2016-02-17 18:09:20', '2016-02-17 18:09:20'),
(42, NULL, 'global', NULL, '2016-02-19 07:33:55', '2016-02-19 07:33:55'),
(43, NULL, 'ip', '121.127.6.147', '2016-02-19 07:33:55', '2016-02-19 07:33:55'),
(45, NULL, 'global', NULL, '2016-02-19 19:32:37', '2016-02-19 19:32:37'),
(46, NULL, 'ip', '::1', '2016-02-19 19:32:37', '2016-02-19 19:32:37'),
(48, NULL, 'global', NULL, '2016-02-22 03:20:22', '2016-02-22 03:20:22'),
(49, NULL, 'ip', '127.0.0.1', '2016-02-22 03:20:22', '2016-02-22 03:20:22'),
(51, NULL, 'global', NULL, '2016-02-22 05:28:12', '2016-02-22 05:28:12'),
(52, NULL, 'ip', '127.0.0.1', '2016-02-22 05:28:12', '2016-02-22 05:28:12'),
(53, NULL, 'global', NULL, '2016-02-28 12:57:22', '2016-02-28 12:57:22'),
(54, NULL, 'ip', '114.108.222.160', '2016-02-28 12:57:22', '2016-02-28 12:57:22'),
(55, 16, 'user', NULL, '2016-02-28 12:57:22', '2016-02-28 12:57:22'),
(56, NULL, 'global', NULL, '2016-03-01 10:13:11', '2016-03-01 10:13:11'),
(57, NULL, 'ip', '112.198.77.170', '2016-03-01 10:13:11', '2016-03-01 10:13:11'),
(58, 15, 'user', NULL, '2016-03-01 10:13:11', '2016-03-01 10:13:11'),
(59, NULL, 'global', NULL, '2016-03-02 16:51:23', '2016-03-02 16:51:23'),
(60, NULL, 'ip', '112.198.78.60', '2016-03-02 16:51:23', '2016-03-02 16:51:23'),
(61, 15, 'user', NULL, '2016-03-02 16:51:23', '2016-03-02 16:51:23'),
(62, NULL, 'global', NULL, '2016-03-02 16:51:33', '2016-03-02 16:51:33'),
(63, NULL, 'ip', '112.198.78.60', '2016-03-02 16:51:33', '2016-03-02 16:51:33'),
(64, 15, 'user', NULL, '2016-03-02 16:51:33', '2016-03-02 16:51:33'),
(65, NULL, 'global', NULL, '2016-03-03 02:12:02', '2016-03-03 02:12:02'),
(66, NULL, 'ip', '180.191.158.56', '2016-03-03 02:12:02', '2016-03-03 02:12:02'),
(67, NULL, 'global', NULL, '2016-03-07 19:21:39', '2016-03-07 19:21:39'),
(68, NULL, 'ip', '112.198.90.77', '2016-03-07 19:21:39', '2016-03-07 19:21:39'),
(69, 19, 'user', NULL, '2016-03-07 19:21:39', '2016-03-07 19:21:39'),
(70, NULL, 'global', NULL, '2016-03-14 05:40:05', '2016-03-14 05:40:05'),
(71, NULL, 'ip', '180.232.70.122', '2016-03-14 05:40:05', '2016-03-14 05:40:05'),
(72, 14, 'user', NULL, '2016-03-14 05:40:05', '2016-03-14 05:40:05'),
(73, NULL, 'global', NULL, '2016-03-15 16:06:21', '2016-03-15 16:06:21'),
(74, NULL, 'ip', '222.127.46.149', '2016-03-15 16:06:21', '2016-03-15 16:06:21'),
(75, 14, 'user', NULL, '2016-03-15 16:06:21', '2016-03-15 16:06:21'),
(76, NULL, 'global', NULL, '2016-03-15 16:47:31', '2016-03-15 16:47:31'),
(77, NULL, 'ip', '222.127.46.149', '2016-03-15 16:47:31', '2016-03-15 16:47:31'),
(78, NULL, 'global', NULL, '2016-03-16 12:36:51', '2016-03-16 12:36:51'),
(79, NULL, 'ip', '222.127.46.149', '2016-03-16 12:36:51', '2016-03-16 12:36:51'),
(80, 14, 'user', NULL, '2016-03-16 12:36:51', '2016-03-16 12:36:51'),
(81, NULL, 'global', NULL, '2016-03-31 01:50:38', '2016-03-31 01:50:38'),
(82, NULL, 'ip', '127.0.0.1', '2016-03-31 01:50:38', '2016-03-31 01:50:38'),
(83, 23, 'user', NULL, '2016-03-31 01:50:38', '2016-03-31 01:50:38'),
(84, NULL, 'global', NULL, '2016-04-14 23:58:22', '2016-04-14 23:58:22'),
(85, NULL, 'ip', '127.0.0.1', '2016-04-14 23:58:22', '2016-04-14 23:58:22'),
(86, 25, 'user', NULL, '2016-04-14 23:58:22', '2016-04-14 23:58:22'),
(87, NULL, 'global', NULL, '2016-04-15 00:17:26', '2016-04-15 00:17:26'),
(88, NULL, 'ip', '127.0.0.1', '2016-04-15 00:17:26', '2016-04-15 00:17:26'),
(89, 33, 'user', NULL, '2016-04-15 00:17:26', '2016-04-15 00:17:26'),
(90, NULL, 'global', NULL, '2016-04-15 00:35:44', '2016-04-15 00:35:44'),
(91, NULL, 'ip', '127.0.0.1', '2016-04-15 00:35:44', '2016-04-15 00:35:44'),
(92, 35, 'user', NULL, '2016-04-15 00:35:44', '2016-04-15 00:35:44'),
(93, NULL, 'global', NULL, '2016-06-12 12:20:27', '2016-06-12 12:20:27'),
(94, NULL, 'ip', '202.27.30.74', '2016-06-12 12:20:27', '2016-06-12 12:20:27'),
(95, 38, 'user', NULL, '2016-06-12 12:20:27', '2016-06-12 12:20:27'),
(96, NULL, 'global', NULL, '2016-07-13 08:50:36', '2016-07-13 08:50:36'),
(97, NULL, 'ip', '219.75.5.31', '2016-07-13 08:50:36', '2016-07-13 08:50:36'),
(98, 38, 'user', NULL, '2016-07-13 08:50:36', '2016-07-13 08:50:36'),
(99, NULL, 'global', NULL, '2017-03-30 11:07:35', '2017-03-30 11:07:35'),
(100, NULL, 'ip', '122.59.221.85', '2017-03-30 11:07:35', '2017-03-30 11:07:35'),
(101, 16, 'user', NULL, '2017-03-30 11:07:35', '2017-03-30 11:07:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_gallery`
--
ALTER TABLE `model_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_list`
--
ALTER TABLE `model_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pages_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `shortlists`
--
ALTER TABLE `shortlists`
  ADD PRIMARY KEY (`shortlist_id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `media_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `model_gallery`
--
ALTER TABLE `model_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=803;
--
-- AUTO_INCREMENT for table `model_list`
--
ALTER TABLE `model_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pages_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=404;
--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `shortlists`
--
ALTER TABLE `shortlists`
  MODIFY `shortlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
