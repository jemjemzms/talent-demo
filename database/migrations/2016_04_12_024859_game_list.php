<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GameList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_list', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gameId');
            $table->string('gameName');
            $table->string('platformCode');
            $table->string('gameCategory');
            $table->text('imageName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('game_list');
    }
}
