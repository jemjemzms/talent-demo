<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        //factory(App\Http\Models\Backoffice\game_category::class,15)->create();
        factory(App\Http\Models\Backoffice\platforms::class,15)->create();

        Model::reguard();
    }
}
