<!DOCTYPE html>
<html class="no-js css-menubar" lang="en" ng-app="contentApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>TalentDemo | Back Office</title>
</head>
<body class="dashboard site-navbar-small">

  <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse bg-grey-600"
  role="navigation">

    <div class="navbar-container container-fluid">

      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">

        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
          <li class="dropdown">
            <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="javascript:;" aria-expanded="false"
            data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="{{asset('/remark/global/portraits/1.jpg')}}" alt="...">
                <i></i>
              </span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li role="presentation">
                <a href="{{asset('/backoffice/logout')}}" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Logout</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- End Navbar Collapse -->

    </div>
  </nav>

  <div class="site-menubar site-menubar-light" ng-controller="menuController">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-category">General</li>
            <li class="site-menu-item has-sub" ng-class="getClass('/dashboard')">
              <a href="{{ asset('/backoffice#/dashboard') }}" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>
                <span class="site-menu-arrow"></span>
              </a>
            </li>
            <li class="site-menu-item has-sub" ng-class="getParentClass('albums')">
              <a href="{{ asset('/backoffice#/albums') }}" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-tag" aria-hidden="true"></i>
                <span class="site-menu-title">Portfolio</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item" ng-class="getClass('/albums')">
                  <a href="{{ asset('/backoffice#/albums') }}">
                    <span class="site-menu-title">List</span>
                  </a>
                </li>
                <li class="site-menu-item" ng-class="getClass('/categories')">
                  <a href="{{ asset('/backoffice#/categories') }}">
                    <span class="site-menu-title">Categories</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-item has-sub" ng-class="getClass('/pages')">
              <a href="{{ asset('/backoffice#/pages') }}" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                <span class="site-menu-title">Pages</span>
                <span class="site-menu-arrow"></span>
              </a>
            </li>
            <li class="site-menu-item has-sub" ng-class="getClass('/blog')">
              <a href="{{ asset('/backoffice#/blog') }}" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                <span class="site-menu-title">Blog</span>
                <span class="site-menu-arrow"></span>
              </a>
            </li>
            <li class="site-menu-item has-sub" ng-class="getClass('/users')">
              <a href="{{ asset('/backoffice#/users') }}" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-user" aria-hidden="true"></i>
                <span class="site-menu-title">Users</span>
                <span class="site-menu-arrow"></span>
              </a>
            </li>
            <li class="site-menu-item has-sub" ng-class="getClass('/media')">
              <a href="{{ asset('/backoffice#/media') }}" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-camera" aria-hidden="true"></i>
                <span class="site-menu-title">Media</span>
                <span class="site-menu-arrow"></span>
              </a>
            </li>
            <li class="site-menu-item has-sub" ng-class="getParentClass('set')">
              <a href="{{ asset('/backoffice#/settings') }}" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-wrench" aria-hidden="true"></i>
                <span class="site-menu-title">Setings</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item" ng-class="getClass('/settings')">
                  <a href="{{ asset('/backoffice#/settings') }}">
                    <span class="site-menu-title">General</span>
                  </a>
                </li>
                <li class="site-menu-item" ng-class="getClass('/frontend-settings')">
                  <a href="{{ asset('/backoffice#/frontend-settings') }}">
                    <span class="site-menu-title">Frontend</span>
                  </a>
                </li>
                <li class="site-menu-item" ng-class="getClass('/backend-settings')">
                  <a href="{{ asset('/backoffice#/backend-settings') }}">
                    <span class="site-menu-title">Backend</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="page">

  <div ng-view></div>
  @yield('content')

  <!-- End Page -->
  </div>

  <!-- Footer -->
  <footer class="site-footer">
    <div>© {{ date('Y') }} <a href="{{ asset('/backoffice#/dashboard') }}">DemoBlog Backoffice</a></div>
  </footer>

  <!-- Core  -->
  <script src="{{asset('/custom/js/modules/ckeditor/ckeditor.js')}}"></script>

  <script src="{{ asset('custom/js/plugins/angularjs/angular.min.js') }}"></script>
  <script src="{{ asset('custom/js/plugins/angularjs/angular-route.js') }}"></script>
  <script src="{{ asset('custom/js/modules/angularPaginate/dirPagination.js') }}"></script>
  <script src="{{ asset('custom/js/modules/checklistModel/checklist-model.js') }}"></script>
  <script src="{{asset('/custom/js/modules/angular-ckeditor/angular-ckeditor.js')}}"></script>
  <script src="{{ asset('custom/js/modules/angular-block-ui/angular-block-ui.js')}}"></script>
  
  <script src="{{ asset('custom/backend/js/routes.js') }}"></script>
  
  <!-- Models -->
  <script src="{{ asset('custom/backend/js/controllers/models/modelListController.js') }}"></script>
  <script src="{{ asset('custom/backend/js/controllers/models/experienceController.js') }}"></script>
  <script src="{{ asset('custom/backend/js/controllers/models/categoryController.js') }}"></script>

  <!-- Customers -->
  <script src="{{ asset('custom/backend/js/controllers/customers/customersController.js') }}"></script>

  <!-- Pages -->
  <script src="{{ asset('custom/backend/js/controllers/pages/pagesController.js') }}"></script>

  <!-- News -->
  <script src="{{ asset('custom/backend/js/controllers/blogs/blogController.js') }}"></script>

  <!-- Media -->
  <script src="{{ asset('custom/backend/js/controllers/media/mediaController.js') }}"></script>

  <script src="{{ asset('custom/backend/js/controllers/settings/usersController.js') }}"></script> 
  <script src="{{ asset('custom/backend/js/controllers/settings/settingsController.js') }}"></script> 
  <script src="{{ asset('custom/backend/js/controllers/settings/frontendController.js') }}"></script> 
  <script src="{{ asset('custom/backend/js/controllers/settings/backendController.js') }}"></script> 
</body>
</html>