<!DOCTYPE html>
<html class="no-js" lang="en-US" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TalentDemo | Demo Project</title>
</head>

<body>
    <header id="header" class="to-move header-variant-left tools-icons-3">
       <!-- Insert Header -->
    </header>
    
    @yield('content')

    <footer id="footer">
        <!-- Insert Footer Here -->
    </footer>

</body>
</html>