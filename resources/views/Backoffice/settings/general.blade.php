<div class="page bg-white" style="margin-top:20px;">

    <!-- Mailbox Sidebar -->
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" data-plugin="pageAsideScroll">
        <div data-role="container">
          <div data-role="content">
            <div class="page-aside-section">
              <div class="list-group">
                <a class="list-group-item active" href="{{ asset('/backoffice#/settings') }}"><i class="fa fa-wrench" aria-hidden="true"></i> General</a>
                <a class="list-group-item" href="{{ asset('/backoffice#/frontend-settings') }}"><i class="fa fa-bookmark" aria-hidden="true"></i> Frontend</a>
                <a class="list-group-item" href="{{ asset('/backoffice#/backend-settings') }}"><i class="fa fa-bookmark" aria-hidden="true"></i> Backend</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">General Settings <i class="fa fa-spinner fa-spin" ng-show="loading"></i></h1>
      </div>

      <!-- General Content -->
      <div class="page-content page-content-table" data-selectable="selectable">


        <!-- start form -->
        <form class="form-horizontal" name="generalForm" ng-submit="submitForm()" novalidate>
          <div class="form-group">
            <label class="col-sm-3 control-label">Application Name: </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="name" ng-model="generalAppName" placeholder="Application Name" autocomplete="off" value="<% general.applicationname %>" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Timezone: </label>
            <div class="col-sm-9">
              <select ng-model="generalTimezone" class="form-control" required> 
                <option value="<% timezone.format %>" ng-repeat="timezone in general.timezone">
                    <% timezone.title %>
                </option>
              </select> 
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Date Format: </label>
            <div class="col-sm-9">
              <div class="radio-custom radio-default" ng-repeat="dateformat in general.dateformat">
                <input type="radio" id="dateFormat" name="dateFormat" value="<% dateformat.format %>" ng-model="generalDateformat.index" required/>
                <label for="inputHorizontalMale"><% dateformat.date %></label>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Time Format: </label>
            <div class="col-sm-9">
              <div class="radio-custom radio-default" ng-repeat="timeformat in general.timeformat">
                <input type="radio" id="timeformat" name="timeformat" value="<% timeformat.format %>" ng-model="generalTimeformat.index" required/>
                <label for="inputHorizontalMale"><% timeformat.time %></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
              <button type="submit" class="btn btn-primary" ng-disabled="generalForm.$invalid">Save Changes </button>
            </div>
          </div>
        </form>
        <!-- eof form -->

        <ul data-plugin="paginator" data-total="50" data-skin="pagination-gap"></ul>
      </div>
    </div>
  </div>