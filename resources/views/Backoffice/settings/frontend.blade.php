<div class="page bg-white" style="margin-top:20px;">

    <!-- Mailbox Sidebar -->
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" data-plugin="pageAsideScroll">
        <div data-role="container">
          <div data-role="content">
            <div class="page-aside-section">
              <div class="list-group">
                <a class="list-group-item" href="{{ asset('/backoffice#/settings') }}"><i class="fa fa-wrench" aria-hidden="true"></i> General</a>
                <a class="list-group-item active" href="{{ asset('/backoffice#/frontend-settings') }}"><i class="fa fa-bookmark" aria-hidden="true"></i> Frontend</a>
                <a class="list-group-item" href="{{ asset('/backoffice#/backend-settings') }}"><i class="fa fa-bookmark" aria-hidden="true"></i> Backend</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">Frontend Settings <i class="fa fa-spinner fa-spin" ng-show="loading"></i></h1>
      </div>

      <!-- General Content -->
      <div class="page-content page-content-table" data-selectable="selectable">
        <!-- start form -->
        <form class="form-horizontal" name="mpgForm" ng-submit="submitForm()" novalidate>
          <div class="form-group">
            <label class="col-sm-3 control-label">Portfolio Display Count:</label>
            <div class="col-sm-9">
              <input type="number" class="form-control" name="talentDisplayCount" ng-model="talentDisplayCount" placeholder="Talent Display Count" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">About:</label>
            <div class="col-sm-9">
              <textarea class="form-control" name="address" ng-model="address" cols="10" rows="5" placeholder="Address" required></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Phone:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="phone" ng-model="phone" placeholder="Phone" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Email:</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" name="email" ng-model="email" placeholder="Email" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Twiter Link:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="globePhone" ng-model="globePhone" placeholder="Globe Phone" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Facebook Link:</label>
            <div class="col-sm-9">
              <textarea class="form-control" name="fbApp" ng-model="fbApp" cols="10" rows="5" placeholder="Facebook API" required></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Instagram Link:</label>
            <div class="col-sm-9">
              <textarea class="form-control" name="musicApp" ng-model="musicApp" cols="10" rows="5" placeholder="Music API" required></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Google Analytics API:</label>
            <div class="col-sm-9">
              <textarea class="form-control" name="googleAnalytics" ng-model="googleAnalytics" cols="10" rows="5" placeholder="Google Analytics API" required></textarea>
            </div>
          </div>
          <div class="page-header">
            <b>SLIDE BOX STYLE</b> 
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Box Height:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_box_height" ng-model="slide_box_height" placeholder="259" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Box Width:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_box_width" ng-model="slide_box_width" placeholder="60" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Line Height:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_box_line_height" ng-model="slide_box_line_height" placeholder="20" autocomplete="off" required />
            </div>
          </div>
          <div class="page-header">
            <b>SLIDE BOX TITLE STYLE</b>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Font Size:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_font_size" ng-model="slide_font_size" placeholder="60" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Line Height:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_line_height" ng-model="slide_line_height" placeholder="40" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Text Shadow:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_text_shadow" ng-model="slide_text_shadow" placeholder="0 0 10px #000000" autocomplete="off" required />
            </div>
          </div>
          <div class="page-header">
            <b>SLIDE BOX DESCRIPTION STYLE</b>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Font Size:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_description_font_size" ng-model="slide_description_font_size" placeholder="40" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Margin Top:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_description_margin_top" ng-model="slide_description_margin_top" placeholder="20" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Text Shadow:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="slide_description_text_shadow" ng-model="slide_description_text_shadow" placeholder="0 0 10px #000000" autocomplete="off" required />
            </div>
          </div>
          <div class="page-header">
            <b>MENU BOX STYLE</b>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Font Size:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="menu_font_size" ng-model="menu_font_size" placeholder="50" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Font Weight:</label>
            <div class="col-sm-9">
              <select class="form-control" ng-model="menu_font_weight" required>
                <option value="normal">Normal</option>
                <option value="bold">Bold</option>
                <option value="900">Thicker</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Text Transform:</label>
            <div class="col-sm-9">
              <select class="form-control" ng-model="menu_text_transform" required>
                <option value="none">None</option>
                <option value="uppercase">Uppercase</option>
                <option value="lowercase">Lowercase</option>
                <option value="capitalize">Capitalize</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
              <button type="submit" class="btn btn-primary" ng-disabled="mpgForm.$invalid">Save Changes </button>
            </div>
          </div>
        </form>
        <!-- eof form -->

        <ul data-plugin="paginator" data-total="50" data-skin="pagination-gap"></ul>
      </div>
    </div>
  </div>