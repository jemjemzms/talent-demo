<div class="page">
    <div class="page-header">
      <h1 class="page-title">Users</h1>
      <ol class="breadcrumb">
        <li><a href="{{ asset('/backoffice#/dashboard') }}">Dashboard</a></li>
        <li class="active">Users</li>
      </ol>
    </div>
    <div class="page-content">
<div class="cssload-thecube" ng-show="content">
        <div class="cssload-cube cssload-c1"></div>
        <div class="cssload-cube cssload-c2"></div>
        <div class="cssload-cube cssload-c4"></div>
        <div class="cssload-cube cssload-c3"></div>
      </div>
      
<div class="panel" ng-hide="content">
        <header class="panel-heading">
          <h3 class="panel-title">Users List</h3>
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-danger" type="button" ng-click="deleteAllSelected();">
                  <i class="fa fa-trash-o" aria-hidden="true"></i> Delete All
                </button>
              </div>
            </div>
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-primary" type="button" data-toggle="modal" data-target="#addnewCategory">
                  <i class="icon wb-plus" aria-hidden="true"></i> Add row
                </button>
              </div>
            </div>
          </div>
          <div class="row margin-bottom-15">
                  <div class="col-sm-4">
                    <i class="fa fa-spinner fa-spin" ng-show="loading"></i>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input ng-model="q" id="search" type="text" class="form-control" name="search" placeholder="Search...">
                        <button type="button" class="input-search-close icon wb-close" aria-label="Close"></button>
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-4">
                    <input type="number" min="1" max="100" class="form-control round" id="inputRounded" ng-model="pageSize" placeholder="Page Size">
                  </div>
          </div>
          <p class="total-records"><b>Total:</b> <% getTotal() %></p>
          <div align="center" class="no-records well well-lg">No Record Found.</div>
          <table class="table table-bordered table-hover table-striped" id="exampleAddRow">
            <thead>
              <tr>
                <th><input type="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" /></th>
                <th><a href="javascript:;" ng-click="sortCategoryByID()"><i class="fa fa-arrows-v"></i></a> Id</th>
                <th><a href="javascript:;" ng-click="sortCategoryByName()"><i class="fa fa-arrows-v"></i></a> Email or Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Last Login</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr class="gradeA category-<% category.id %>" dir-paginate="category in model | filter:q | itemsPerPage: pageSize | orderBy: order" current-page="currentPage" ng-dblclick="modify(category)">
                <td>
                  <div ng-hide="editingData[category.id]">
                    <input type="checkbox" ng-model="category.selected" ng-change="optionToggled()" />
                  </div>
                </td>
                <td><% category.id %></td>
                <td>
                 <div ng-hide="editingData[category.id]"><% category.email %></div>
                 <div ng-show="editingData[category.id]"><input type="text" ng-model="category.email" /></div>
                </td>
                <td>
                 <div ng-hide="editingData[category.id]"><% category.first_name %></div>
                 <div ng-show="editingData[category.id]"><input type="text" ng-model="category.first_name" /></div>
                </td>
                <td>
                 <div ng-hide="editingData[category.id]"><% category.last_name %></div>
                 <div ng-show="editingData[category.id]"><input type="text" ng-model="category.last_name" /></div>
                </td> 
                <td><% category.last_login %></td>
                <td class="actions">
                  <a href="javascript:;" ng-click="setUser(category);" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-original-title="Change Password" data-toggle="modal" data-target="#changePassword"><i class="fa fa-lock" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-hide="editingData[category.id]" ng-click="modify(category)" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-toggle="tooltip" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-show="editingData[category.id]" ng-click="update(category)" class="btn btn-sm btn-icon btn-pure btn-default on-editing save-row" data-toggle="tooltip" data-original-title="Save"><i class="fa fa-save" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-show="editingData[category.id]" ng-click="cancel(category)" class="btn btn-sm btn-icon btn-pure btn-default on-editing cancel-row" data-toggle="tooltip" data-original-title="Cancel"><i class="icon wb-close" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-click="remove( category.id )" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                </td>
              </tr>
            </tbody>
          </table>
          <div ng-controller="OtherController" class="other-controller">
              <div class="text-center">
              <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="{{asset('/custom/js/modules/angularPaginate')}}/dirPagination.tpl.html"></dir-pagination-controls>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="addnewCategory" aria-hidden="true" aria-labelledby="addnewCategory" role="dialog" tabindex="-1">
<div class="modal-dialog modal-sidebar modal-sm">
 <form name="userForm" ng-submit="submitForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Add User</h4>
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <input id="email" name="email" class="form-control" type="text" value="" placeholder="Email or Username" ng-model="textEmail" required />
          <!--<div ng-show="userForm.$submitted || userForm.email.$touched">
            <span ng-show="userForm.uEmail.$error.required">Tell us your email.</span>
            <span ng-show="userForm.uEmail.$error.email">This is not a valid email.</span>
          </div>-->
      </div>
      <div class="form-group form-material">
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="First Name" ng-model="textFirst" required />
      </div>
      <div class="form-group form-material">
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="Last Name" ng-model="textLast" required />
      </div>
      <div class="form-group form-material">
          <input id="inputFocus" class="form-control" type="password" value="" placeholder="Password" ng-model="textPass" required />
      </div>
      <div class="form-group form-material">
          <input id="inputFocus" class="form-control" type="password" value="" placeholder="Confirm Password" ng-model="textConfPass" required />
      </div>
    </div>
    <div class="modal-footer">
      <!--<button type="button" class="btn btn-primary btn-block" ng-click="addRow();">Save changes</button>-->
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="userForm.$invalid">Add User</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>


<div class="modal fade" id="changePassword" aria-hidden="true" aria-labelledby="changePassword" role="dialog" tabindex="-1">
<div class="modal-dialog modal-sidebar modal-sm">
 <form name="changePassForm" ng-submit="submitPassForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Change Password for</h4>
      <h4 class="modal-title"><% selecteduser.first_name %> <% selecteduser.last_name %></h4>
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <input id="inputFocus" class="form-control" type="password" value="" placeholder="Password" ng-model="textPass" required />
      </div>
      <div class="form-group form-material">
          <input id="inputFocus" class="form-control" type="password" value="" placeholder="Confirm Password" ng-model="textConfPass" required />
      </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="changePassForm.$invalid">Change Password</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>
