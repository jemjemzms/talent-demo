<div class="page bg-white" style="margin-top:20px;">

    <!-- Mailbox Sidebar -->
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" data-plugin="pageAsideScroll">
        <div data-role="container">
          <div data-role="content">
            <div class="page-aside-section">
              <div class="list-group">
                <a class="list-group-item" href="{{ asset('/backoffice#/settings') }}"><i class="fa fa-wrench" aria-hidden="true"></i> General</a>
                <a class="list-group-item" href="{{ asset('/backoffice#/frontend-settings') }}"><i class="fa fa-bookmark" aria-hidden="true"></i> Frontend</a>
                <a class="list-group-item active" href="{{ asset('/backoffice#/backend-settings') }}"><i class="fa fa-bookmark" aria-hidden="true"></i> Backend</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Mailbox Content -->
    <div class="page-main">

      <!-- Mailbox Header -->
      <div class="page-header">
        <h1 class="page-title">Backend Settings <i class="fa fa-spinner fa-spin" ng-show="loading"></i></h1>
      </div>

      <!-- General Content -->
      <div class="page-content page-content-table" data-selectable="selectable">
        <!-- start form -->
        <form class="form-horizontal" name="mpgForm" ng-submit="submitForm()" novalidate>
          <div class="form-group">
            <label class="col-sm-3 control-label">Model List Display Count:</label>
            <div class="col-sm-9">
              <input type="number" class="form-control" name="name" ng-model="modelListDisplayCount" placeholder="Model List Count" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Experience Display Count: </label>
            <div class="col-sm-9">
              <input type="number" class="form-control" name="name" ng-model="experienceDisplayCount" placeholder="Experience Display Count" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Category Display Count: </label>
            <div class="col-sm-9">
              <input type="number" class="form-control" name="name" ng-model="categoryDisplayCount" placeholder="Category Display Count" autocomplete="off" required />
            </div>
          </div> 
          <div class="form-group">
            <label class="col-sm-3 control-label">Customers Display Count: </label>
            <div class="col-sm-9">
              <input type="number" class="form-control" name="name" ng-model="customersDisplayCount" placeholder="Customers Display Count" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Pages Display Count: </label>
            <div class="col-sm-9">
              <input type="number" class="form-control" name="name" ng-model="pagesDisplayCount" placeholder="Pages Display Count" autocomplete="off" required />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">News Display Count: </label>
            <div class="col-sm-9">
              <input type="number" class="form-control" name="name" ng-model="newsDisplayCount" placeholder="News Display Count" autocomplete="off" />
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
              <button type="submit" class="btn btn-primary" ng-disabled="mpgForm.$invalid">Save Changes </button>
            </div>
          </div>
        </form>
        <!-- eof form -->

        <ul data-plugin="paginator" data-total="50" data-skin="pagination-gap"></ul>
      </div>
    </div>
  </div>