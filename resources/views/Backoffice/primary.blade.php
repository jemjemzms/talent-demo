<div class="page">
    <div class="page-content padding-30 container-fluid">
      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xlg-4 col-lg-6 col-md-6">
          <div id="personalCompletedWidget" class="widget widget-shadow padding-bottom-20">
            <div class="widget-content">
              <div class="row text-center margin-bottom-20">
                <div class="col-xs-6">
                  <div class="counter">
                    <div class="counter-label total-completed">TOTAL Albums</div>
                    <div class="counter-number red-600"><% totals.totalAlbum %></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>