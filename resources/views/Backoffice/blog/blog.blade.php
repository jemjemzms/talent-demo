<div class="page">
    <div class="page-header">
      <h1 class="page-title">Blog</h1>
      <ol class="breadcrumb">
        <li><a href="{{ asset('/backoffice#/dashboard') }}">Dashboard</a></li>
        <li class="active">Blog</li>
      </ol>
    </div>
    <div class="page-content">

<div class="cssload-thecube" ng-show="content">
        <div class="cssload-cube cssload-c1"></div>
        <div class="cssload-cube cssload-c2"></div>
        <div class="cssload-cube cssload-c4"></div>
        <div class="cssload-cube cssload-c3"></div>
      </div>
      
<div class="panel" ng-hide="content">
        <header class="panel-heading">
          <h3 class="panel-title">Blog List</h3>
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-danger" type="button" ng-click="deleteAllSelected();">
                  <i class="fa fa-trash-o" aria-hidden="true"></i> Delete All
                </button>
              </div>
            </div>
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-primary" type="button" data-toggle="modal" data-target="#addnewNews" ng-click="addNewNews()">
                  <i class="icon wb-plus" aria-hidden="true"></i> Add Blog
                </button>
              </div>
            </div>
          </div>
          <div class="row margin-bottom-15">
                  <div class="col-sm-4">
                    <i class="fa fa-spinner fa-spin" ng-show="loading"></i>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input ng-model="q" id="search" type="text" class="form-control" name="search" placeholder="Search...">
                        <button type="button" class="input-search-close icon wb-close" aria-label="Close"></button>
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-4">
                    <input type="number" min="1" max="100" class="form-control round" id="inputRounded" ng-model="pageSize" placeholder="Page Size">
                  </div>
          </div>
          <p class="total-records"><b>Total:</b> <% getTotal() %></p>
          <table class="table table-bordered table-hover table-striped" id="recordTable">
            <thead>
              <tr>
                <th><input type="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" /></th>
                <th><a href="javascript:;" ng-click="sortRowByName()"><i class="fa fa-arrows-v"></i></a> Title</th>
                <th> Author</th>
                <th> Status</th>
                <th> Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr class="gradeA" dir-paginate="news in model | filter:q | itemsPerPage: pageSize | orderBy: order" current-page="currentPage" ng-dblclick="modify(news)">
                <td>
                  <div ng-hide="editingData[news.blog_id]">
                    <input type="checkbox" ng-model="news.selected" ng-change="optionToggled()" />
                  </div>
                </td>
                <td><% news.title %></td>
                <td><% news.name  %></td>
                <td><% news.status | capitalize  %></td>
                <td class="actions">  
                  <a href="javascript:;" ng-click="editNews(news)" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-toggle="modal" data-target="#editNews" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-click="remove( news.blog_id )" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                </td>
              </tr>
            </tbody>
          </table>
          <div align="center" class="no-records well well-lg" ng-show="!model.length">No blog yet.</div>

          <div ng-controller="OtherController" class="other-controller">
              <div class="text-center">
              <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="{{asset('/custom/js/modules/angularPaginate')}}/dirPagination.tpl.html"></dir-pagination-controls>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade modal-fill-in" id="addnewNews" aria-hidden="true" aria-labelledby="addnewNews" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg">
 <form class="pages-form" name="newsForm" ng-submit="submitForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Add Blog</h4>
    </div>
    <div style="margin-top:200px;">
    &nbsp;
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <label>Title</label>
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="Title" ng-model="texttitle" required />
      </div>
      <div class="form-group form-material">
          <label>Summary</label>
          <textarea class="form-control" id="textsummary" rows="3" placeholder="Summary" ng-model="textsummary" required></textarea>
      </div>
      <div class="form-group form-material">
          <label>Description</label>
          <div ckeditor="options" ng-model="textdescription" ready="onReady()" required></div>
      </div>
      <label>Publish</label>
      <!-- Status -->
      <div class="form-group form-material">
          <div class="row">
            <div class="col-sm-4">
                Status: <label><% textstatus | capitalize %></label> <a href="javascript:;" ng-click="changeStatus()" ng-hide="publishStatus">Edit</a> 
            </div>
          </div>
      </div>
      <div class="form-group form-material" ng-show="publishStatus">
          <div class="row">
            <div class="col-sm-3">
                  <select class="form-control" ng-model="textstatus">
                    <option value="draft">Draft</option>
                    <option value="published">Published</option>
                  </select>
            </div>
            <div class="col-sm-3">
                  <button type="button" class="btn btn-outline btn-default" ng-click="changeStatus()">Ok</button>
            </div>
          </div>
      </div>
      <!-- EOF Status -->
      <label>Featured Image</label>
      <div class="form-group form-material form-control" align="center" style="height:100%">
            <input type="hidden" class="featured-photo-image" />
            <input type="file" file-model="myFile" value=""/><img src="{{asset('img//news/news.png')}}" class="featured-photo-image-url" width="150" /><span class="featured-photo">Choose Photo</span>
      </div>
       <button type="button" ng-click="uploadNewsFile()">set featured photo</button>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="newsForm.$invalid">Save as <% textstatus | capitalize %></button>
      <button type="button" class="btn btn-danger btn-block" ng-click="AddNewTrash()">Move to Trash</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>

<div class="modal fade modal-fill-in" id="editNews" aria-hidden="true" aria-labelledby="editNews" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg">
 <form class="pages-form" name="editNewsForm" ng-submit="submitUpdateForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerEditClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Edit Blog</h4>
    </div>
    <div style="margin-top:200px;">
    &nbsp;
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <label>Title</label>
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="Title" ng-model="textedittitle" required />
      </div>
      <div class="form-group form-material">
          <label>Summary</label>
          <textarea class="form-control" id="textsummary" rows="3" placeholder="Summary" ng-model="texteditsummary" required></textarea>
      </div>
      <div class="form-group form-material">
          <label>Description</label>
          <div ckeditor="options" ng-model="texteditdescription" ready="onReady()"></div>
      </div>
      <div class="form-group form-material">
          <div class="row">
            <div class="col-sm-4">
                Status: <label><% texteditstatus | capitalize %></label> <a href="javascript:;" ng-click="changeStatus()" ng-hide="publishStatus">Edit</a> 
            </div>
          </div>
      </div>
      <div class="form-group form-material" ng-show="publishStatus">
          <div class="row">
            <div class="col-sm-3">
                  <select class="form-control" ng-model="texteditstatus">
                    <option value="draft">Draft</option>
                    <option value="published">Published</option>
                  </select>
            </div>
            <div class="col-sm-3">
                  <button type="button" class="btn btn-outline btn-default" ng-click="changeStatus()">Ok</button>
            </div>
          </div>
      </div>
      <label>Featured Image</label>
      <div class="form-group form-material form-control" align="center" style="height:100%">
            <input type="hidden" class="featured-photo-image" name="" />
            <input type="file" file-model="myFile" value=""/><img src="" class="featured-photo-image-url" width="150" />
            <span class="featured-photo">Choose Photo</span>
      </div>
       <button type="button" ng-click="uploadNewsFile()">set featured photo</button>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="editNewsForm.$invalid">Edit <% texteditstatus | capitalize %> Blog</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>
