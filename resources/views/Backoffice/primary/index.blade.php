@extends('layout.backend_layout.master')
@section('content')

<?php
  if(Session::get('message')){ 
  ?>
        <div class="alert alert-info">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading">Info!</h4>
                                    <p>
                                       <?php echo Session::get('message'); ?>
                                    </p>
                                </div>
  <?php
  }
  ?>
        <section>
        <ol class="breadcrumb">
                                    <li><a href="#">home</a></li>
                                                <li class="active">Loans</li>
                        </ol>

    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> List Loans</h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-head">
                        <header>
                           <button class="btn btn-primary" type="submit" onclick="location.href='<?=URL::to('/')?>/backend/<?php echo 'users/register'; ?>';">
                                <i class="fa fa-times"></i>
                                Delete
                           </button>
                           <button class="btn btn-primary" type="submit" onclick="location.href='<?=URL::to('/')?>/backend/<?php echo 'loans/overdue'; ?>';">
                                Overdue (0)
                           </button>
                           <button class="btn btn-primary" type="submit" onclick="location.href='<?=URL::to('/')?>/backend/<?php echo 'loans/add'; ?>';">
                                <i class="fa fa-plus"></i>
                                New Loan
                           </button>
                        </header>
                    </div>
                    <?php
                    if(count($data) == 0){
                    ?>
                        <div class="box-body">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">No record found.</h3>            
                                </div>
                            </div>
                        </div>
                    <?php
                    } else{
                    ?> 
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox" id="cb1" >
                                            </label>
                                        </div>
                                    </th>
                                    <th>ID</th>
                                    <th>Kind of Loan</th>
                                    <th>Account #</th>
                                    <th>Amount</th>
                                    <th>Customer</th>
                                    <th>Agent</th>
                                    <th>Date Release</th>
                                    <th>Payment Date</th>
                                    <th>Status</th>
                                    <th class="text-right1" style="width:90px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($data as $row){
                                 ?>
                                     <tr>
                                        <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="cb1" >
                                                </label>
                                            </div>
                                        </td>
                                        <td><?php echo $row->id; ?></td>
                                        <td><?php echo $row->loan_type_name; ?></td>
                                        <td><?php echo $row->account_number; ?></td>
                                        <td><?php echo $row->amount; ?></td>
                                        <td><?php echo $row->firstname.' '.$row->lastname; ?></td>
                                        <td><?php echo $row->first_name.' '.$row->last_name; ?></td>
                                        <!--<td><?php echo $row->approve_by; ?></td>-->
                                        <td><?php echo $row->date_release; ?></td>
                                        <td><?php echo $row->payment_date; ?></td>
                                        <td><?php echo strtoupper($row->status); ?></td>
                                        <td class="text-right">
                                            <button type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Edit row"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Delete row" onclick="location.href='<?=URL::to('/')?>/backend/<?php echo 'customers/delete/'.$row->id; ?>';"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                     </tr>
                                <?php
                                    }
                                ?>
                                <?php 
                                    echo $data->appends(['_token' => $token])->render();
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    }
                    ?> 
                </div>
            </div>
        </div>
    </div>
</section>

@stop()