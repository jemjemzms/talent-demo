@extends('layout.backend_layout.master')
@section('content')
  <?php
  if(Session::get('message')){ 
  ?>
        <div class="alert alert-info">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading">Info!</h4>
                                    <p>
                                       <?php echo Session::get('message'); ?>
                                    </p>
                                </div>
  <?php
  }
  ?>
<section>
        <ol class="breadcrumb">
          <li><a href="{{asset('/backend/dashboard')}}">home</a></li>
          <li><a href="{{asset('/backend/blocked')}}">Blocked List</a></li>
          <li class="active">Edit Block Customer</li>
        </ol>
    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Blocked <small>Information</small></h3>
    </div>
    <form action="{{action('Backend\UsersController@updatePermission')}}" method="post" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" value="{{ $row->id }}" />
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-body">

                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="select1" class="control-label">Permision Name</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    {{ str_replace('_',' ', ucwords($row->permission_name)) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="select1" class="control-label">Guest</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    <select class="form-control" name="guest">
                                        <?php 
                                        $type = ['Yes' => '1', 'No' => '0'];
                                        ?>
                                        <?php foreach($type as $key => $val){ ?>
                                             <?php if ($val == $row->guest){  ?>
                                                  <option value="<?php echo $val; ?>" selected="selected"><?php echo $key; ?></option>
                                             <?php } else{ ?>
                                                  <option value="<?php echo $val; ?>"><?php echo $key; ?></option>
                                             <?php
                                                   }
                                             ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="select1" class="control-label">Editor</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    <select class="form-control" name="editor">
                                        <?php 
                                        $type = ['Yes' => '1', 'No' => '0'];
                                        ?>
                                        <?php foreach($type as $key => $val){ ?>
                                             <?php if ($val == $row->editor){  ?>
                                                  <option value="<?php echo $val; ?>" selected="selected"><?php echo $key; ?></option>
                                             <?php } else{ ?>
                                                  <option value="<?php echo $val; ?>"><?php echo $key; ?></option>
                                             <?php
                                                   }
                                             ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="select1" class="control-label">Admin</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    <select class="form-control" name="admin">
                                        <?php 
                                        $type = ['Yes' => '1', 'No' => '0'];
                                        ?>
                                        <?php foreach($type as $key => $val){ ?>
                                             <?php if ($val == $row->admin){  ?>
                                                  <option value="<?php echo $val; ?>" selected="selected"><?php echo $key; ?></option>
                                             <?php } else{ ?>
                                                  <option value="<?php echo $val; ?>"><?php echo $key; ?></option>
                                             <?php
                                                   }
                                             ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="select1" class="control-label">Super Admin</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    <select class="form-control" name="superadmin">
                                        <?php 
                                        $type = ['Yes' => '1', 'No' => '0'];
                                        ?>
                                        <?php foreach($type as $key => $val){ ?>
                                             <?php if ($val == $row->superadmin){  ?>
                                                  <option value="<?php echo $val; ?>" selected="selected"><?php echo $key; ?></option>
                                             <?php } else{ ?>
                                                  <option value="<?php echo $val; ?>"><?php echo $key; ?></option>
                                             <?php
                                                   }
                                             ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="select1" class="control-label">Order</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    <input type="text" name="order" id="order" class="form-control" placeholder="Order" value="{{ $row->order }}">
                                </div>
                            </div> 
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-body">
                            <div class="form-footer col-lg-offset-1 col-md-offset-2 col-sm-offset-3">
                                <button class="btn btn-default" type="button" onclick="location.href='<?php echo url('backend/users/permission', $parameters = array(), $secure = null); ?>';">
                                  Back
                                </button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>

@stop()