@extends('layout.backend_layout.master')
@section('content')

<?php
  if(Session::get('message')){ 
  ?>
        <div class="alert alert-info">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading">Info!</h4>
                                    <p>
                                       <?php echo Session::get('message'); ?>
                                    </p>
                                </div>
  <?php
  }
  ?>
        <section class="users-section">
        <ol class="breadcrumb">
                                    <li><a href="{{asset('/backend/dashboard')}}">home</a></li>
                                                <li class="active">Permissions</li>
                        </ol>

    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> List of Permissions</h3>
    </div>
    <div class="section-body">

        <!-- START BASIC TABLE -->
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-head">
                        <header>
                           <input type="hidden" name="root" id="root" value="{{URL::to('/')}}" />
                        </header>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Permission Name</th>
                                    <th>Guest</th>
                                    <th>Editor</th>
                                    <th>Administrator</th>
                                    <th>Super Administrator</th>
                                    <th>Order</th>
                                    <th class="text-right1" style="width:90px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($data as $row){
                                 ?>
                                     <tr>
                                        <td><?php echo str_replace('_',' ', ucwords($row->permission_name)); ?></td>
                                        <td><?php echo ($row->guest) ? 'Yes' : 'No'; ?></td>
                                        <td><?php echo ($row->editor) ? 'Yes' : 'No'; ?></td>
                                        <td><?php echo ($row->admin) ? 'Yes' : 'No'; ?></td>
                                        <td><?php echo ($row->superadmin) ? 'Yes' : 'No'; ?></td>
                                        <td><?php echo $row->order; ?></td>
                                        <td class="text-right">
                                            <button type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Edit row" onclick="location.href='<?=URL::to('/')?>/backend/<?php echo 'users/edit-permission/'.$row->id; ?>';"><i class="fa fa-pencil"></i></button>
                                        </td>
                                     </tr>
                                <?php
                                    }
                                ?>
                                <?php 
                                    echo $data->render();
                                ?>
                            </tbody>
                        </table>
                    </div><!--end .box-body -->
                </div><!--end .box -->
            </div><!--end .col-lg-12 -->
        </div>
        <!-- END BASIC TABLE -->
    </div>
</section>

@stop()