@extends('layout.backoffice_layout.master')
@section('content')
<?php
  if(Session::get('message')){ 
  ?>
        <div class="alert alert-info">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading">Info!</h4>
                                    <p>
                                       <?php echo Session::get('message'); ?>
                                    </p>
                                </div>
  <?php
  }
  ?>
        <section class="users-section">
        <ol class="breadcrumb">
                                    <li><a href="{{asset('/backend/dashboard')}}">home</a></li>
                                                <li class="active">Users</li>
                        </ol>

    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> List of Users <small>Accounts</small></h3>
    </div>
    <div class="section-body">

        <!-- START BASIC TABLE -->
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-head">
                        <header>
                           <?php
                           if(Session::get('role') == 'superadmin'){
                           ?>
                           <button class="btn btn-primary" type="submit" onclick="location.href='<?=URL::to('/')?>/backend/<?php echo 'users/register'; ?>';">
                                <i class="fa fa-plus"></i>
                                New User
                           </button>
                           <?php
                           }
                           ?>
                           <input type="hidden" name="root" id="root" value="{{URL::to('/')}}" />
                        </header>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <?php
                                    if(Session::get('role') == 'superadmin'){
                                    ?>
                                    <th class="text-right1" style="width:90px">Actions</th>
                                    <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($data as $row){
                                 ?>
                                     <tr>
                                        <td><?php echo $row->id; ?></td>
                                        <td><?php echo $row->first_name; ?></td>
                                        <td><?php echo $row->last_name; ?></td>
                                        <td><?php echo $row->email; ?></td>
                                        <td><?php 

                                            $userRole = DB::table('role_users')
                                                        ->leftJoin('roles', 'roles.id','=','role_users.role_id')
                                                        ->where('role_users.user_id', $row->id)->first();

                                            echo $userRole->name;

                                        ?></td>
                                        <?php
                                        if(Session::get('role') == 'superadmin'){
                                        ?>
                                        <td class="text-right">
                                            <!--<a class="confirm" href="<?=URL::to('/').'/backend/users/delete/'.$row->id; ?>">Delete</a>-->
                                            <button type="button" class="btn btn-xs btn-default btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Edit row" onclick="location.href='<?=URL::to('/')?>/backend/<?php echo 'users/edit/'.$row->id; ?>';"><i class="fa fa-pencil"></i></button>
                                            <?php 
                                            if($row->id !== 3){
                                            ?> 
                                            <button type="button" class="btn btn-xs btn-default btn-equal remove" data-toggle="tooltip" data-placement="top" data-original-title="Delete row" data-id="{{$row->id}}"><i class="fa fa-trash-o"></i></button>
                                            <?php
                                            }
                                            ?> 
                                        </td>
                                        <?php
                                        }
                                        ?>
                                     </tr>
                                <?php
                                    }
                                ?>
                                <?php 
                                    echo $data->render();
                                ?>
                            </tbody>
                        </table>
                    </div><!--end .box-body -->
                </div><!--end .box -->
            </div><!--end .col-lg-12 -->
        </div>
        <!-- END BASIC TABLE -->
    </div>
</section>

@stop()