@extends('layout.backend_layout.master')
@section('content')
<?php
  if($errors->first('email') || $errors->first('password') || $errors->first('repeatpassword')){ 
  ?>
        <div class="alert alert-info">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading">Info!</h4>
                                    <p style="color:red;">{{ $errors->first('email') }}</p>
                                    <p style="color:red;">{{ $errors->first('password') }}</p>
                                    <p style="color:red;">{{ $errors->first('repeatpassword') }}</p>
                                </div>
  <?php
  }
  ?>

  <?php
  if(Session::get('message')){ 
  ?>
        <div class="alert alert-info">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading">Info!</h4>
                                    <p>
                                       <?php echo Session::get('message'); ?>
                                    </p>
                                </div>
  <?php
  }
  ?>
<section>
        <ol class="breadcrumb">
          <li><a href="{{asset('/backend/dashboard')}}">home</a></li>
          <li><a href="{{asset('/backend/users')}}">Users</a></li>
          <li class="active">New User</li>
        </ol>
    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> New User <small>Information</small></h3>
    </div>
    <form action="{{action('Backend\UsersController@save')}}" method="post" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-head">
                        <header><h4 class="text-light">Basic Information</h4></header>
                    </div>
                    <div class="box-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="select1" class="control-label">Firstname</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    <input type="text" name="firstname" id="firstname" class="form-control" placeholder="Firstname">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="select1" class="control-label">Lastname</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Lastname">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-head">
                        <header><h4 class="text-light">Login Information</h4></header>
                    </div>
                    <div class="box-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-lg-1 col-md-2 col-sm-3">
                                    <label for="email1" class="control-label">Email</label>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-9">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-lg-2 col-md-4 col-sm-6">
                                            <label for="password1" class="control-label">Password</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-6">
                                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                                            <p class="help-block">Use Alphanumeric characters.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-lg-2 col-md-4 col-sm-6">
                                            <label for="repeatpassword1" class="control-label">Repeat</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-6">
                                            <input type="password" name="repeatpassword" id="repeatpassword" class="form-control" placeholder="Repeat password">
                                            <p class="help-block">Retype your password.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-head">
                        <header>
                            <h4 class="text-light">User Role</h4>
                        </header>
                    </div>
                    <div class="box-body">
                        <div class="form-horizontal">
                            <p>
                               Select to grant access to this user: 
                               <p style="margin-left:9px;">
                                <select class="form-group" name="role" id="role">
                                        <option value="Guest">Guest</option>
                                        <option value="Editor">Editor</option>
                                        <option value="Administrator">Administrator</option>
                                        <option value="Super Administrator">Super Administrator</option>
                                </select>
                               </p>
                            </p>
                         </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-body">
                            <div class="form-footer col-lg-offset-1 col-md-offset-2 col-sm-offset-3">
                                <button class="btn btn-default" type="button" onclick="location.href='<?php echo url('backend/users', $parameters = array(), $secure = null); ?>';">
                                  Back
                                </button>
                                <button type="reset" class="btn btn-default">Reset</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>

<!--</body>
</html>-->
@stop()