<div class="page">
    <div class="page-header">
      <h1 class="page-title">Experience</h1>
      <ol class="breadcrumb">
        <li><a href="{{ asset('/backoffice#/') }}">Home</a></li>
        <li class="active">Experience</li>
      </ol>
    </div>
    <div class="page-content">
    
<div class="panel" ng-hide="content">
        <header class="panel-heading">
          <h3 class="panel-title">Experience List</h3>
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-danger" type="button" ng-click="deleteAllSelected();">
                  <i class="fa fa-trash-o" aria-hidden="true"></i> Delete All
                </button>
              </div>
            </div>
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-primary" type="button" data-toggle="modal" data-target="#addNewRow">
                  <i class="icon wb-plus" aria-hidden="true"></i> Add row
                </button>
              </div>
            </div>
          </div>
          <div class="row margin-bottom-15">
                  <div class="col-sm-4">
                    <i class="fa fa-spinner fa-spin" ng-show="loading"></i>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input ng-model="q" id="search" type="text" class="form-control" name="search" placeholder="Search...">
                        <button type="button" class="input-search-close icon wb-close" aria-label="Close"></button>
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-4">
                    <input type="number" min="1" max="100" class="form-control round" id="inputRounded" ng-model="pageSize" placeholder="Page Size">
                  </div>
          </div>
          <p class="total-records"><b>Total:</b> <% getTotal() %></p>
          <div align="center" class="no-records well well-lg">No Record Found.</div>
          <table class="table table-bordered table-hover table-striped" id="recordTable">
            <thead>
              <tr>
                <th><input type="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" /></th>
                <th><a href="javascript:;" ng-click="sortRowByName()"><i class="fa fa-arrows-v"></i></a> Name</th>
                <th><a href="javascript:;" ng-click="sortRowBySlug()"><i class="fa fa-arrows-v"></i></a> Slug</th>
                <th>Description</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr class="gradeA" dir-paginate="row in model | filter:q | itemsPerPage: pageSize | orderBy: order" current-page="currentPage" ng-dblclick="modify(row)">
                <td>
                  <div ng-hide="editingData[row.id]">
                    <input type="checkbox" ng-model="row.selected" ng-change="optionToggled()" />
                  </div>
                </td>
                <td>
                 <div ng-hide="editingData[row.id]"><% row.name %></div>
                 <div ng-show="editingData[row.id]"><input type="text" ng-model="row.name" /></div>
                </td>
                <td>
                 <div ng-hide="editingData[row.id]"><% row.slug %></div>
                 <div ng-show="editingData[row.id]"><input type="text" ng-model="row.slug" /></div>
                </td>
                <td>
                 <div ng-hide="editingData[row.id]"><% row.description %></div>
                 <div ng-show="editingData[row.id]"><textarea ng-model="row.description" style="width:100%"></textarea></div>
                </td>
                <td class="actions">
                  <a href="javascript:;" ng-hide="editingData[row.id]" ng-click="modify(row)" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-toggle="tooltip" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-show="editingData[row.id]" ng-click="update(row)" class="btn btn-sm btn-icon btn-pure btn-default on-editing save-row" data-toggle="tooltip" data-original-title="Save"><i class="fa fa-save" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-show="editingData[row.id]" ng-click="cancel(row)" class="btn btn-sm btn-icon btn-pure btn-default on-editing cancel-row" data-toggle="tooltip" data-original-title="Cancel"><i class="icon wb-close" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-click="remove( row.id )" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                </td>
              </tr>
            </tbody>
          </table>
          <div ng-controller="OtherController" class="other-controller">
              <div class="text-center">
              <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="{{asset('/custom/js/modules/angularPaginate')}}/dirPagination.tpl.html"></dir-pagination-controls>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="addNewRow" aria-hidden="true" aria-labelledby="addNewRow" role="dialog" tabindex="-1">
<div class="modal-dialog modal-sidebar modal-sm">
 <form name="tableForm" ng-submit="submitForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Add New Experience</h4>
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="Name" ng-model="textName" required />
      </div>
      <div class="form-group form-material">
          <input id="inputFocus" class="form-control" type="text" value="<% textSlug %>" placeholder="Slug" ng-model="textSlug" required />
      </div>
      <div class="form-group form-material">
          <textarea id="inputFocus" class="form-control" ng-model="textDescription" style="width:100%" placeholder="Description"></textarea>
      </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="tableForm.$invalid">Add Experience</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>
