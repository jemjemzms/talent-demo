<div class="page">
    <div class="page-header">
      <h1 class="page-title">Portfolio List</h1>
      <ol class="breadcrumb">
        <li><a href="{{ asset('/backoffice#/dashboard') }}">Dashboard</a></li>
        <li class="active">Portfolio List</li>
      </ol>
    </div>
    <div class="page-content">

<div class="panel" ng-hide="content">
        <header class="panel-heading">
          <h3 class="panel-title">Portfolio List</h3>
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-danger" type="button" ng-click="deleteAllSelected();">
                  <i class="fa fa-trash-o" aria-hidden="true"></i> Delete All
                </button>
              </div>
            </div>
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-primary" type="button" data-target="#addNewModel" data-toggle="modal">
                  <i class="icon wb-plus" aria-hidden="true"></i> Add an Portfolio
                </button>
              </div>
            </div>
          </div>
          <div class="row margin-bottom-15">
                  <div class="col-sm-4">
                    <i class="fa fa-spinner fa-spin" ng-show="loading"></i>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input ng-model="q" id="search" type="text" class="form-control" name="search" placeholder="Search...">
                        <button type="button" class="input-search-close icon wb-close" aria-label="Close"></button>
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-4">
                    <input type="number" min="1" max="100" class="form-control round" id="inputRounded" ng-model="pageSize" placeholder="Page Size">
                  </div>
          </div>
          <p class="total-records"><b>Total:</b> <% getTotal() %></p>
          <div align="center" class="no-records well well-lg">No Record Found.</div>
          <table class="table table-bordered table-hover table-striped" id="recordTable">
            <thead>
              <tr>
                <th><input type="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" /></th>
                <th style="width:120px;"> Photo</th>
                <th><a href="javascript:;" ng-click="sortRowByID()"><i class="fa fa-arrows-v"></i></a> ID</th>
                <th><a href="javascript:;" ng-click="sortRowByName()"><i class="fa fa-arrows-v"></i></a> Name</th>
                <th><a href="javascript:;" ng-click="sortRowByOrder()"><i class="fa fa-arrows-v"></i></a> Order</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr class="gradeA" dir-paginate="row in model | filter:q | itemsPerPage: pageSize | orderBy: order" current-page="currentPage" ng-dblclick="modify(row)">
                <td>
                  <div ng-hide="editingData[row.id]">
                    <input type="checkbox" ng-model="row.selected" ng-change="optionToggled()" />
                  </div>
                </td>
                <td>
                  <figure class="widget-header overlay-hover overlay">
                    <img src="{{asset('/img/profiles/small')}}/<% row.primary_photo %>" alt="No Photo" style="width:100px;" />
                    <figcaption class="overlay-panel overlay-background overlay-fade overlay-icon">
                      <a class="icon wb-search" href="javascript:;" data-target="#expandPhotoModal" data-toggle="modal" data-dismiss="modal" ng-click="selectExpandPhoto(row)"></a>
                    </figcaption>
                  </figure>
                </td>
                <td>
                 <div ng-hide="editingData[row.id]"><% row.id %></div>
                </td>
                <td>
                 <div ng-hide="editingData[row.id]"><% row.model_name %></div>
                </td>
                <td>
                 <div ng-hide="editingData[row.id]"><% row.order %></div>
                </td>
                <td class="actions"> 
                  <a href="javascript:;" ng-click="modify(row)" data-target="#editNewModel" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" title="Edit" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-click="setGallery(row)" data-target="#galleryModel" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" title="Gallery" data-original-title="Edit"><i class="icon wb-image" aria-hidden="true"></i></a>

                  <a href="javascript:;" ng-click="remove( row.id )" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip" title="Remove" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                </td>
              </tr>
            </tbody>
          </table>
          <div ng-controller="OtherController" class="other-controller">
              <div class="text-center">
              <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="{{asset('/custom/js/modules/angularPaginate')}}/dirPagination.tpl.html"></dir-pagination-controls>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
<input type="hidden" id="profileDefaultImage" name="profileDefaultImage" value="{{ asset('/img/profiles/profile.png') }}" />
<div class="modal fade modal-3d-sign" id="addNewModel" aria-hidden="true" aria-labelledby="addNewModel" role="dialog" tabindex="-1">
 <div class="modal-dialog modal-md">
  <div class="modal-content">
    <form name="newForm" ng-submit="submitForm()" novalidate>      
    <div class="modal-header">
      <button type="button" class="close triggerAddClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="exampleModalTitle">Add New Portfolio</h4>
    </div>
    <div class="modal-body">
    <div class="row">
       <div class="col-sm-10 col-lg-10">
      <div class="form-group form-material form-control" align="center" style="height:100%">
         <img src="" id="profile-image" width="150" /> 
      </div>
      <input type="hidden" id="profilePicName" name="profilePicName" value="" />
      <div class="form-group">
        <div class="input-group input-group-file form-control">
         <input type="file" style="height:40px;" file-model="myFile" name="file-profilePhoto" id="file-profilePhoto" multiple />
         <span class="input-group-btn">
          <span class="btn btn-file">
            <button type="button" ng-click="uploadFile()"><i class="icon wb-upload" aria-hidden="true"></i></button>
          </span>
        </span>
      </div>
     </div>
      <div class="form-group form-material">
        <label>Portfolio Name</label>
        <input type="text" class="form-control" id="modelname" name="modelname" placeholder="Model Name" ng-model="textModelName" required/>
      </div>
      <div class="form-group">
        <label>Slug</label>
        <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug" ng-model="textSlug" required/>
      </div>
      <div class="form-group form-material">
          <label>Description</label>
          <textarea id="notes" name="notes" ng-model="textNotes" style="width:100%" placeholder="Description"></textarea>
      </div>
      <div class="form-group form-material">
          <label>Select Category</label>
          <div class="checkbox-custom" ng-repeat="category in categories">
            <input type="checkbox" checklist-model="textCategories.category" checklist-value="category.id" />
            <label><% category.name %></label>
          </div>
      </div>
      <div class="form-group form-material">
          <label>Featured Portfolio</label>
          <select class="form-control" ng-model="textfeatured">
            <option value="no">No</option>
            <option value="yes">Yes</option>
          </select>
      </div>
      <div class="form-group form-material">
        <label>Order</label>
        <input type="text" class="form-control" id="order" name="order" placeholder="Order" ng-model="textorder"/>
      </div>
       </div>
     </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="newForm.$invalid">Add Portfolio</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
    </form>
  </div>
</div>
</div>


<!-- Modal -->
<div class="modal fade modal-3d-sign" id="editNewModel" aria-hidden="true" aria-labelledby="editNewModel" role="dialog" tabindex="-1">
 <div class="modal-dialog modal-md">
  <div class="modal-content">
  <form name="editForm" ng-submit="submitEditForm()" novalidate>      
    <div class="modal-header">
      <button type="button" class="close triggerEditClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="exampleModalTitle">Edit New Model</h4>
      Category : <% editCategories.category %>
    </div>
    <div class="modal-body">
    <div class="row">
       <div class="col-sm-10 col-lg-10">
      <div class="form-group form-material form-control" align="center" style="height:100%">
         <img src="" id="edit-profile-image" width="150" /> 
      </div>
      <input type="hidden" id="editProfilePicName" name="editProfilePicName" value="" ng-model="editPrimaryPhoto" />
      <div class="form-group">
                  <div class="input-group input-group-file form-control">
                   <input type="file" style="height:40px;" file-model="myFile" name="file-editprofilePhoto" id="file-editprofilePhoto" />
                    <span class="input-group-btn">
                      <span class="btn btn-file">
                        <button type="button" ng-click="uploadEditFile()"><i class="icon wb-upload" aria-hidden="true"></i></button>
                      </span>
                    </span>
                  </div>
               </div>
      <div class="form-group form-material">
        <label>Model Name</label>
        <input type="text" class="form-control" id="modelname" name="modelname" placeholder="Model Name" ng-model="editModelName" required/>
      </div>
      <div class="form-group form-material">
        <label>Slug</label>
        <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug" ng-model="editSlug" required/>
      </div>
      <div class="form-group form-material">
          <label>Description</label>
          <textarea id="notes" name="notes" ng-model="editNotes" style="width:100%" placeholder="Description"></textarea>
      </div>
      <div class="form-group form-material">
          <label>Select Category</label> 
          <!--<div class="checkbox-custom" ng-repeat="category in categories">
            <input type="checkbox" checklist-model="editCategories.category" checklist-value="category.id" />
            <label><% category.name %></label>
          </div>-->
          <select data-plugin="selectpicker" ng-model="editCategories" class="selectpicker show-tick form-control"> 
          <option value="<% category.id %>" ng-repeat="category in categories" ng-selected="<% category.id == editCategories %>">
              <% category.name %>
            </option>
          </select>    
      </div>
      <div class="form-group form-material">
          <label>Featured Portfolio</label>
          <select class="form-control" ng-model="editFeatured">
            <option value="no">No</option>
            <option value="yes">Yes</option>
          </select>
      </div>
      <div class="form-group form-material">
        <label>Order</label>
        <input type="text" class="form-control" id="order" name="order" placeholder="Order" ng-model="editOrder"/>
      </div>
       </div>
     </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="editForm.$invalid">Update Portfolio</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
    </form>
  </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade modal-3d-sign" id="galleryModel" aria-hidden="true" aria-labelledby="galleryModel" role="dialog" tabindex="-1">
 <div class="modal-dialog modal-lg">
  <div class="modal-content">
  <form name="editForm" ng-submit="submitEditForm()" novalidate>      
    <div class="modal-header">
      <button type="button" class="close triggerGalleryClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="exampleModalTitle">Manage Gallery</h4>
    </div>
    <div class="modal-body">
     <div class="row">
      <div class="form-group">
        <div ng-show="captionText">
          Caption: <textarea class="form-control" ng-model="captionMessage"></textarea> 
          Order: <input type="text" class="form-control" ng-model="orderPhoto" /> 
                   <div align="right" style="margin:10px;"><input type="button" value="Save Image Info." ng-click="saveCaption();"></div>
        </div>
        <div class="input-group input-group-file form-control">
         <input type="file" style="height:40px;" file-model="myFile" name="file-galleryPhoto" id="file-galleryPhoto" multiple />
         <span class="input-group-btn">
          <span class="btn btn-file">
            <button type="button" ng-click="uploadGalleryFile()"><i class="icon wb-upload" aria-hidden="true"></i></button>
          </span>
        </span>
        </div>
      </div>
      <ul class="blocks blocks-100 blocks-xlg-4 blocks-md-3 blocks-sm-2" id="exampleList"
      data-filterable="true">
        <li data-type="animal" ng-repeat="gallery in galleries">
          <div class="widget widget-shadow">
            <figure class="widget-header overlay-hover overlay">
              <img class="overlay-figure overlay-scale" src="{{asset('/img/uploads/small')}}/<% gallery.image_name %>" alt="..." style="width:80%">
              <figcaption class="overlay-panel overlay-background overlay-fade overlay-icon">
               <a class="icon wb-edit" ng-click="editCaption(gallery.id)" href="javascript:;" title="Enlarge"></a>
                <a class="icon wb-search" href="{{asset('/img/uploads/small')}}/<% gallery.image_name %>" target="_blank" title="Enlarge"></a>
                <a class="icon wb-trash" href="javascript:;" title="Remove" ng-click="removePhoto(gallery.id)"></a>
              </figcaption>
            </figure>
            <h4 class="widget-title">
              <input type="radio" id="galleryIndex" ng-click="selectFeatured(gallery)" name="galleryIndex" value="<% gallery.id %>" ng-model="gallerySelect.index" required/> <span id="gallery-<% gallery.id %>" class="gallery-label"><% postLabelFeatured(gallery.featured) %></span>
            </h4>
          </div>
        </li>
      </ul>
      <div align="center" ng-show="!galleries.length">
        Please add Photos..    
      </div>
      <div align="center" ng-show="!gallerySelect.index.length">
        <div align="center" ng-show="galleries.length">
            You can select a Featured Image.
        </div>
      </div>
        <!--<% galleryId %>-->
     </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
    </form>
  </div>
</div>
</div>

<!-- Expand Photo -->
<div class="modal fade modal-3d-sign" id="expandPhotoModal" aria-hidden="true" aria-labelledby="expandPhotoModal" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Profile Photo</h4>
    </div>
    <div class="modal-body">
      <div align="center">
          <img src="<% expandImage %>" width="35%" />
      </div>
    </div>
    <div class="modal-footer">
      <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:;"  role="button">Close</a>
    </div>
  </div>
</div>
</div>
<!-- End Expand Photo -->