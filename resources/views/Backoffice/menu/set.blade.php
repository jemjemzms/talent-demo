@extends('layout.backend_layout.master')
@section('content')
<p style="color:green"><?php echo Session::get('message'); ?></p>
<section class="content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Menu Settings</h3>
                                </div>
                                <form action="{{action('Backend\MenuController@update')}}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="box-body">
                                    <?php
                                    foreach($data as $key => $value){
                                    ?>
                                        <div class="form-group">
                                            <label><?=str_replace('_', ' ', ucwords($value->setting_name))?></label>
                                            <input type="text" class="form-control" name="<?=$value->setting_name?>" value="<?=$value->setting_value?>" placeholder="Enter <?=$value->setting_name?>">
                                        </div>
                                    <?php
                                    }
                                    ?>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
</section>
@stop()