<div class="page">
    <div class="page-header">
      <h1 class="page-title">Pages</h1>
      <ol class="breadcrumb">
        <li><a href="{{ asset('/backoffice#/dashboard') }}">Dashboard</a></li>
        <li class="active">Pages</li>
      </ol>
    </div>
    <div class="page-content">

<div class="cssload-thecube" ng-show="content">
        <div class="cssload-cube cssload-c1"></div>
        <div class="cssload-cube cssload-c2"></div>
        <div class="cssload-cube cssload-c4"></div>
        <div class="cssload-cube cssload-c3"></div>
      </div>
      
<div class="panel" ng-hide="content">
        <header class="panel-heading">
          <h3 class="panel-title">Pages List</h3>
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-2">
              <div class="margin-bottom-15">
                <button id="addToTable" class="btn btn-outline btn-danger" type="button" ng-click="deleteAllSelected();">
                  <i class="fa fa-trash-o" aria-hidden="true"></i> Delete All
                </button>
              </div>
            </div>
          </div>
          <div class="row margin-bottom-15">
                  <div class="col-sm-4">
                    <i class="fa fa-spinner fa-spin" ng-show="loading"></i>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input ng-model="q" id="search" type="text" class="form-control" name="search" placeholder="Search...">
                        <button type="button" class="input-search-close icon wb-close" aria-label="Close"></button>
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-4">
                    <input type="number" min="1" max="100" class="form-control round" id="inputRounded" ng-model="pageSize" placeholder="Page Size">
                  </div>
          </div>
          <p class="total-records"><b>Total:</b> <% getTotal() %></p>
          <table class="table table-bordered table-hover table-striped" id="recordTable">
            <thead>
              <tr>
                <th><input type="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" /></th>
                <th><a href="javascript:;" ng-click="sortRowByName()"><i class="fa fa-arrows-v"></i></a> Title</th>
                <th> Author</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr class="gradeA" dir-paginate="page in model | filter:q | itemsPerPage: pageSize | orderBy: order" current-page="currentPage" ng-dblclick="modify(page)">
                <td>
                  <div ng-hide="editingData[page.page_id]">
                    <input type="checkbox" ng-model="page.selected" ng-change="optionToggled()" />
                  </div>
                </td>
                <td><% page.title %></td>
                <td><% page.name  %></td>
                <td class="actions">  
                  <a href="javascript:;" ng-click="editPage(page)" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-toggle="modal" data-target="#editPage" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>
                </td>
              </tr>
            </tbody>
          </table>
          <div align="center" class="no-records well well-lg" ng-show="!model.length">No Pages yet.</div>

          <div ng-controller="OtherController" class="other-controller">
              <div class="text-center">
              <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="{{asset('/custom/js/modules/angularPaginate')}}/dirPagination.tpl.html"></dir-pagination-controls>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade modal-fill-in" id="addnewPage" aria-hidden="true" aria-labelledby="addnewPage" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg">
 <form class="pages-form" name="pagesForm" ng-submit="submitForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Add Page</h4>
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <label>Title</label>
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="Title" ng-model="texttitle" required />
      </div>
      <div class="form-group form-material">
          <label>Description</label>
          <div ckeditor="options" ng-model="textdescription" ready="onReady()"></div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="pagesForm.$invalid">Add Page</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>

<div class="modal fade modal-fill-in" id="editPage" aria-hidden="true" aria-labelledby="editPage" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg">
 <form class="pages-form" name="editPagesForm" ng-submit="submitUpdateForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerEditClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Edit Page</h4>
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <label>Title</label>
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="Title" ng-model="textedittitle" required />
      </div>
      <div class="form-group form-material">
          <label>Description</label>
          <div ckeditor="options" ng-model="texteditdescription" ready="onReady()"></div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="editPagesForm.$invalid">Edit Page</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>
