<div class="page">
    <div class="page-header">
      <h1 class="page-title">Media</h1>
      <ol class="breadcrumb">
        <li><a href="{{ asset('/backoffice#/dashboard') }}">Dashboard</a></li>
        <li class="active">Media</li>
      </ol>
    </div>
    <div class="page-content">

<div class="cssload-thecube" ng-show="content">
        <div class="cssload-cube cssload-c1"></div>
        <div class="cssload-cube cssload-c2"></div>
        <div class="cssload-cube cssload-c4"></div>
        <div class="cssload-cube cssload-c3"></div>
      </div>
      
<div class="panel" ng-hide="content">
        <header class="panel-heading">
          <h3 class="panel-title">Media List</h3>
        </header>
        <div class="panel-body">
          <p class="total-records"><b>Total:</b> <% getTotal() %></p>
          <table class="table table-bordered table-hover table-striped" id="recordTable">
            <thead>
              <tr>
                <th><a href="javascript:;" ng-click="sortRowByName()"><i class="fa fa-arrows-v"></i></a> Media Title</th>
                <th> Author</th>
                <th> Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr class="gradeA" dir-paginate="news in model | filter:q | itemsPerPage: pageSize | orderBy: order" current-page="currentPage" ng-dblclick="modify(news)">
                <td><% news.title %></td>
                <td><% news.name  %></td>
                <td class="actions">  
                  <a href="javascript:;" ng-click="editNews(news)" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-toggle="modal" data-target="#editNews" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>
                </td>
              </tr>
            </tbody>
          </table>
          <div align="center" class="no-records well well-lg" ng-show="!model.length">No media yet.</div>

          <div ng-controller="OtherController" class="other-controller">
              <div class="text-center">
              <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="{{asset('/custom/js/modules/angularPaginate')}}/dirPagination.tpl.html"></dir-pagination-controls>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade modal-fill-in" id="addnewNews" aria-hidden="true" aria-labelledby="addnewNews" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg">
 <form class="pages-form" name="newsForm" ng-submit="submitForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Add Media Photo</h4>
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <label>Title</label>
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="Title" ng-model="texttitle" required />
      </div>
      <label>Publish</label>
      <div class="form-group form-material">
          <label>Media URL</label>
          <input id="inputFocus" class="form-control mediaUrl" type="text" value="" placeholder="Media URL" ng-model="mediaUrl" readonly/>
      </div>
      <label>Featured Image</label>
      <div class="form-group form-material form-control" align="center" style="height:100%">
            <input type="hidden" class="featured-photo-image" />
            <input type="file" file-model="myFile" value=""/><img src="{{asset('img//media/news.png')}}" class="featured-photo-image-url" width="150" /><span class="featured-photo">Choose Photo</span>
      </div>
       <button type="button" ng-click="uploadNewsFile()">set featured photo</button>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="newsForm.$invalid">Save Media Photo</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>

<div class="modal fade modal-fill-in" id="editNews" aria-hidden="true" aria-labelledby="editNews" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg">
 <form class="pages-form" name="editNewsForm" ng-submit="submitUpdateForm()" novalidate>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close triggerEditClose" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title">Edit Media Photo</h4>
    </div>
    <div class="modal-body">
      <div class="form-group form-material">
          <label>Title</label>
          <input id="inputFocus" class="form-control" type="text" value="" placeholder="Title" ng-model="textedittitle" required />
      </div>
      <div class="form-group form-material">
          <label>Media URL</label>
          <input id="inputFocus" class="form-control mediaUrl" type="text" value="" placeholder="Media URL" ng-model="mediaUrl" readonly/>
      </div>
      <label>Featured Image</label>
      <div class="form-group form-material form-control" align="center" style="height:100%">
            <input type="hidden" class="featured-photo-image" name="" />
            <input type="file" file-model="myFile" value=""/><img src="" class="featured-photo-image-url" width="150" />
            <span class="featured-photo">Choose Photo</span>
      </div>
       <button type="button" ng-click="uploadNewsFile()">set featured photo</button>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block" ng-disabled="editNewsForm.$invalid">Edit Media Photo</button>
      <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
    </div>
  </div>
 </form>
</div>
</div>
