<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>Login | DemoBlog</title>
</head>

 <body class="page-login-v2 layout-full page-dark">
  <div class="page animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content">
      <div class="page-brand-info">
        <div class="brand">
          <img class="brand-img" src="{{asset('/img/main/logo.png')}}" alt="...">
          <h2 class="brand-text font-size-40">Back Office</h2>
        </div>
      </div>

      <div class="page-login-main">
        <div class="brand visible-xs">
          <img class="brand-img" src="{{asset('/remark/images/logo.png')}}" alt="...">
          <h3 class="brand-text font-size-40">Back Office</h3>
        </div>
        <h3 class="font-size-24">Sign In</h3>
        <form method="POST" action="{{action('Backoffice\UsersController@authenticate')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! csrf_field() !!}
        <?php
        $message = '';
        if(Session::get('message')){
            $sessionMessage = Session::get('message');
            $message = 'messages'.'.'.$sessionMessage;
        }
        ?>
       <p style="color:red;">{{ $errors->first('email') }}</p>
            <p style="color:red;">{{ $errors->first('password') }}</p>
            <p style="color:black"><?php echo Session::get('message'); ?></p>
          <div class="form-group">
            <label class="sr-only" for="inputEmail">Email</label>
            <input type="text" class="form-control" name="email" placeholder="Username or Email">
          </div>
          <div class="form-group">
            <label class="sr-only" for="inputPassword">Password</label>
            <input type="password" class="form-control" name="password"
            placeholder="Password">
          </div>
          <button type="submit" class="btn btn-primary btn-block">Sign in</button>
        </form>

        <footer class="page-copyright">
          <p>© {{ date('Y') }}. Demoblog.</p>
          <p>All RIGHTS RESERVED.</p>
        </footer>
      </div>

    </div>
  </div>
</body>
</html>