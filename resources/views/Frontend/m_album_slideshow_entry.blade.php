@extends('layout.frontend_layout.master')
@section('content')

<div id="mid" class="to-move  layout-full no-sidebars">
    <ul id="album-media-collection" class="screen-reader-text">

      <?php
      foreach($galleries as $key){
         ?>
         <li class="album-item type-image subtype-jpeg" data-ratio_x="1" data-bg_color="" data-product_id="882" data-thumb="{{ asset('/img/uploads/small/'.$key['image_name'] ) }}" data-brick_image="{{ asset('/img/uploads/medium/'.$key['image_name'] ) }}" data-main-image="{{ asset('/img/uploads/large/'.$key['image_name'] ) }}">
            <a href="#"></a>
            <div id="album-desc-1" class="album-desc">
                <?=$key['caption']?>
            </div>
            <div class="a2a_kit a2a_kit_size_18 addtoany_list a2a_target" id="wpa2a_1">
                <a class="a2a_button_facebook" href="#" title="Facebook" rel="nofollow" target="_blank"></a>
                <a class="a2a_button_pinterest" href="#" title="Pinterest" rel="nofollow" target="_blank"></a>
                <a class="a2a_button_twitter" href="#" title="Twitter" rel="nofollow" target="_blank"></a>
                <a class="a2a_dd addtoany_share_save" href="https://www.addtoany.com/share"></a>
            </div>
        </li>
        <?php
    } 
    ?>
</ul>
<div class="in-post-slider" id="album-slider" data-autoplay="1" data-transition="3" data-fit_variant="4" data-pattern="1" data-gradient="1" data-ken_burns_scale="120" data-texts="1" data-title_color="#d21b0a" data-transition_time="600" data-slide_interval="7000" data-thumbs="on" data-thumbs_on_load="on"></div>
</div> 

@stop()