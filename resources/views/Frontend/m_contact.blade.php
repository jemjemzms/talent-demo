 @extends('layout.frontend_layout.master')
@section('content')

<div id="mid" class="to-move  layout-right layout-parted layout-edge no-sidebars">
        <article id="content" class="clearfix">
            <div class="content-limiter">
                <div id="col-mask">

                    <div id="post-722" class="content-box post-722 page type-page status-publish has-post-thumbnail hentry">
                        <div class="item-image post-media"><img width="740" src="{{asset('/img/media/large/'.$contactHeaderPhoto->featured_image)}}" class="attachment-740x0x1 size-740x0x1 wp-post-image" alt="wrapper" />
                        </div>
                        <div class="formatter">
                            <header class="title-bar subtitle inside">
                                <div class="in">
                                    <h1 class="page-title">Contact Me</h1>
                                    <h2>Need Assistance? Get in touch!</h2> </div>
                            </header>
                            <div class="real-content">
                                <div>
                                    <p style="color:green;font-weight:bold;"><?php echo Session::get('message'); ?></p>
                                    <p style="color:red;font-weight:bold;"><?php echo Session::get('error'); ?></p>
                                    {!! Form::open(array('url'=>'mail','method'=>'POST', 'id'=>'contact_form', 'name'=>'contact_form', 'class'=>'')) !!} 

                                        <p style="font-size:12px;">Name: <span style="color:red;">*</span>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-name">
                                            {!! Form::text('name','',array('id'=>'name','name'=>'name','required'=>'required','placeholder' => '')) !!}</span>
                                        </p>
                                        <p style="font-size:12px;">Email: <span style="color:red;">*</span>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                            {!! Form::email('email','',array('id'=>'email','name'=>'email','required'=>'required','placeholder' => '','pattern' => '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')) !!}</span>
                                        </p>
                                        <p style="font-size:12px;">Message:
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-message">
                                            {!! Form::textarea('message','',array('id'=>'message','name'=>'message','required'=>'required','placeholder' => '')) !!}</span>
                                        </p>
                                        <p>
                                            <input type="submit" value="Send the Message" class="wpcf7-form-control wpcf7-submit" />
                                        </p>
                                    {!! Form::close() !!}
                                </div>
                                <div class="clear"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>

    </div>
@stop()