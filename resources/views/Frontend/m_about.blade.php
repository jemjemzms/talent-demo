 @extends('layout.frontend_layout.master')
@section('content')

<div id="mid" class="to-move  layout-right layout-parted layout-edge no-sidebars">
        <article id="content" class="clearfix">
            <div class="content-limiter">
                <div id="col-mask">

                    <div id="post-225" class="content-box post-225 page type-page status-publish has-post-thumbnail hentry">
                        <div class="item-image post-media"><img width="740" src="{{asset('/img/media/large/'.$aboutheaderPhoto->featured_image)}}" class="attachment-740x0x1 size-740x0x1 wp-post-image" alt="wrapper" />
                        </div>
                        <div class="formatter">
                            <header class="title-bar inside">
                                <div class="in">
                                    <h1 class="page-title"><?php echo $data->title ?></h1> </div>
                            </header>
                            <div class="real-content">
                                <p><?php echo $data->description ?></p>
                                <div class="clear"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>

    </div>
    <!-- #mid -->

<style type='text/css'>

        .page-background{
            background-color:;
            background-image: url('<?=asset('/img/media/large/'.$aboutBgPhoto->featured_image)?>') !important;
            background-size: cover; background-repeat: no-repeat; background-position: 0 0 !important;
        }

</style>
@stop()