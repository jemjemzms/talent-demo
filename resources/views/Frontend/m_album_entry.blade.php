@extends('layout.frontend_layout.master')
@section('content')

<div id="mid" class="to-move layout-full no-sidebars">
        <header class="title-bar outside">
            <div class="in">
                <span style="color:Red;"><?=$categories?></span>
                <h1 class="page-title"><?=$albumDetail->model_name?></h1> 
                <?=$albumDetail->notes?>
            </div>
        </header>
        <article id="content" class="clearfix">
            <div class="content-limiter">
                <div id="col-mask">
                    <div class="content-box">
                        <div class="bricks-frame bricks-columns-3">
                            <div id="only-albums-here">
                                <div class="grid-master"></div>

                                <?php
                                foreach($galleries as $key){
                                ?>
                                <figure class="archive-item default-eff w1" data-genre-0="1" id="album-904">
                                    <img src="{{ asset('/img/uploads/medium/'.$key['image_name'] ) }}" alt="" />
                                    <figcaption>
                                        <div class="center_group">
                                            <div class="album-categories"><a href="{{ asset('/albums-slideshow/'.$albumId.'/'.$key['id'] ) }}"><?=$key['caption']?></a>
                                            </div>
                                            <div class="excerpt">
                                            </div>
                                        </div>
                                        <a href="{{ asset('/albums-slideshow/'.$albumId.'/'.$key['id'] ) }}"></a>
                                    </figcaption>
                                </figure>
                                <?php
                                }
                                ?>
                                
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </article>

    </div>

@stop()