 @extends('layout.frontend_layout.master')
@section('content')

<div id="mid" class="to-move  layout-center layout-parted no-sidebars">
        <article id="content" class="clearfix">
            <div class="content-limiter">
                <div id="col-mask">

                    <div id="post-78" class="content-box post-78 post type-post status-publish format-video hentry category-beauty category-photography tag-fashion tag-image tag-link tag-news tag-wordpress post_format-post-format-video">
                        <div class="item-video post-media">
                            <img width="728" src="{{ asset('/img/news/large/'.$data->featured_image ) }}" class="attachment-728x0x1 size-728x0x1 wp-post-image" alt="blog2" />
                        </div>
                        <div class="formatter">
                            <div class="post-meta">
                                <time class="entry-date" datetime="2015-03-03T07:56:05+00:00"><i class="fa fa-clock-o"></i> {{ $data->date }}</time> <span class="cats">
                            </div>
                            <h2 class="post-title">{{ $data->title }}</h2>
                            <div class="real-content">

                                <p><?=$data->description?></p>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </article>
    </div>

@stop()