@extends('layout.frontend_layout.master')
@section('content')

<div id="mid" class="to-move layout-full no-sidebars">
        <header class="title-bar outside">
            <div class="in">
                <h1 class="page-title">Portfolio</h1> </div>
        </header>
        <article id="content" class="clearfix">
            <div class="content-limiter">
                <div id="col-mask">
                    <div class="content-box">
                        <ul class="genre-filter clearfix">
                            <li class="label"><i class="fa fa-bars"></i>Filter</li>
                            <li class="selected" data-filter="__all"><a href="index.html">All</a>
                            </li>
                            <?php
                            foreach($categories as $key){
                            ?>
                            <li data-filter="<?=$key['id']?>"><a href=""><?=$key['name']?></a></li>
                            <?php
                            }
                            ?>
                        </ul>
                        <div class="bricks-frame bricks-columns-3">
                            <div id="only-albums-here">
                                <div class="grid-master"></div>

                                <?php
                                foreach($albums as $key){
                                ?>
                                <figure class="archive-item default-eff w1" data-genre-<?=$key['categories']?>="1" id="album-904">
                                    <img src="{{ asset('/img/profiles/medium/'.$key['primary_photo'] ) }}" alt="" />
                                    <figcaption>
                                        <div class="center_group">
                                            <div class="album-categories"><a href="{{ asset('/albums/'.$key['id'].'/'.$key['slug'] ) }}"><?=$key['model_name']?></a>
                                            </div>
                                            <h2 class="post-title"><?=$key['notes']?></h2>
                                            <div class="excerpt">
                                            </div>
                                        </div>
                                        <a href="{{ asset('/albums/'.$key['id'].'/'.$key['slug'] ) }}"></a>
                                    </figcaption>
                                </figure>
                                <?php
                                }
                                ?>
                                
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </article>

    </div>

@stop()