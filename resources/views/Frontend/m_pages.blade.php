@extends('layout.frontend_layout.master')
@section('content')

<div id="mid" class="to-move  layout-full_fixed no-sidebars">
        <article id="content" class="clearfix">
            <div class="content-limiter">
                <div id="col-mask">

                    <div id="post-204" class="content-box post-204 page type-page status-publish hentry">
                        <div class="formatter">
                            <header class="title-bar inside">
                                <div class="in">
                                    <h1 class="page-title">{{ $data->title }}</h1> </div>
                            </header>
                            <div class="real-content">
                                <p style="text-align: justify;"><?=$data->description?></p>
                                <div class="clear"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>

    </div>

@stop()