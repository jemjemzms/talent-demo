 @extends('layout.frontend_layout.master')
@section('content')

<div id="mid" class="to-move  layout-full_padding layout-padding no-sidebars">
        <header class="title-bar subtitle outside">
            <div class="in">
                <h1 class="page-title">The Blog</h1>
            </div>
        </header>

        <article id="content" class="clearfix">
            <div class="content-limiter">
                <div id="col-mask">
                    <div class="content-box">



                        <div class="bricks-frame bricks-columns-3">
                            <div id="only-posts-here">
                                <div class="grid-master"></div>

                                <?php
                                foreach($data as $key){
                                ?>
                                <div id="post-90" class="archive-item w1 post-90 post type-post status-publish format-standard has-post-thumbnail hentry category-beauty category-people category-photography tag-fashion tag-news tag-wordpress">

                                    <div class="item-image post-media">
                                        <a href="{{ asset('/blogs/'.$key['blog_id'].'/'.$key['slug'] ) }}"><img width="728" src="{{ asset('/img/news/small/'.$key['featured_image'] ) }}" class="attachment-728x0x1 size-728x0x1 wp-post-image" alt="blog2" />
                                        </a>
                                    </div>
                                    <div class="formatter">
                                        <div class="post-meta">
                                            <time class="entry-date" datetime="2015-04-15T08:23:09+00:00"><i class="fa fa-clock-o"></i> <?=$key['date']?></time>
                                        </div>
                                        <h2 class="post-title"><a href="{{ asset('/blogs/'.$key['blog_id'].'/'.$key['slug'] ) }}"><?=$key['title']?></a></h2>
                                        <div class="real-content">

                                            <p><?=$key['summary']?> <a href="{{ asset('/blogs/'.$key['blog_id'].'/'.$key['slug'] ) }}" class="more-link">Read more &#8230;</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </article>
    </div>
@stop()