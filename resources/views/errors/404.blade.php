@extends('layout.frontend_layout.master')
@section('content')
  
<div id="c">
       <!-- start slideshow-->
       <div class="container page-template  default-template" id="container">
          <div class="top-page">
              <div class="container">
                  <div id="crumbs"><a href="<?=URL::to('/')?>">Home</a> <span class="brn_arrow">»</span> <span class="current">Feedback</span>
                  </div>
              </div>
          </div>
          <div role="main" id="content">
              <div id="main">
                  <h1 class="heading-title page-title">Feedback</h1>
                  <div class="col-sm-24" id="container-main">
                      <div class="main-content">
                          <article class="post-33 page type-page status-publish hentry" id="post-33">
                              <div class="entry-content-post">
                                  <div class="woocommerce">
                                      <div id="customer_login" class="col2-set">
                                          <div class="col-1">
                                              <h2>Thank you for your feedback!</h2>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </article>
                      </div>
                  </div>
              </div>
          </div>
      </div>
        <!-- end -->
        <div id="footer" role="contentinfo">
                <div class="footer-container">
@stop()